/// <reference types="cypress" />

import {Search} from "../../src/actions/Actions";


describe("SearchChecking", () => {

	beforeEach(() => {
		cy.visit("http://localhost:3000");
	})

	it("has a title", () => {
		cy.get('.result-container').contains('Loading')
	});


	it('Search', () => {
		cy.title().should('eq','React App')
		cy.get('#search-input').type('Test-article')
		cy.get('.search-label').contains('Search').should('be.visible').click()
		cy.get('.result-container').contains('Test-article')
		cy.get('.result-container').contains('Comments')
		cy.get('.add-comment-btn', {timeout:10000}).contains('Add comment').should('be.visible')
	})

	it('Search suggestions test', () => {
		cy.title().should('eq','React App')
		cy.get('#search-input').type('love')
		cy.get(".recommendations", {timeout:10000}).contains('love').should('be.visible')
		cy.get(".recommendations").contains('loved').should('be.visible')
		cy.get('.suggestion').contains("LitIpsum: I would've loved to make it work").click()
		cy.get('.one-article').contains("LitIpsum: I would've loved to make it work")

	})

	it('Sort', () => {
		cy.get('.sort_item', {timeout:10000}).contains('Sort articles alphabetically').click()
		cy.get('.one-article').contains('A')
	})

	it('Check 4 articles in the page', () => {
		cy.get('.one-article').contains('Test-article').should('be.visible')
		cy.get('.one-article').contains('Test-article2').should('be.visible')
		cy.get('.one-article').contains('Test-article3').should('be.visible')
		cy.get('.one-article').contains('Test-article4').should('be.visible')
	})

	it('Add comment', () => {
		cy.title().should('eq','React App')
		cy.get('#search-input').type("LitIpsum: I would've loved to make it work")
		cy.get('.search-label').contains('Search').should('be.visible').click()
		cy.get('#comment-input').type('Cypress Test comment 2')
		cy.get('.add-comment-btn').contains('Add comment').should('be.visible').click()
		cy.get('.search-label').contains('Search').should('be.visible').click()
		cy.get('.result-container').contains('Comments')
		cy.get('.comments').contains('Cypress Test comment 2')
		cy.get('.add-comment-btn', {timeout:10000}).contains('Add comment').should('be.visible')
	})

});
