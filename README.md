# Project 4

## Run the application

To run the application you must first run ``npm install`` in the root directory of the project.
You must also run ``cd backend`` and then ``npm install`` there as well. While you are in the
**backend** directory run ``npm start ``, as the backend must run before the app can run.
You must then open a new terminal and run ``npm start `` at the root directory of the project.
Backend will work on ``localhost:8080``.

Then everything should work to check the project out, and it will open the app on ``localhost:3000``.


## Project development after project 3
I have chosen the alternative B of develop other functionalities on project 3 and make tests, because project
3 wasn't 100% complete because of time. If you want to read about work processen we had in project 3, you can read
the file README_from_prosjekt3. I have added some functionalities to this version. The application is made by
using Typescript. And Redux is used as third party component. Login doesn't work yet.


## Functionality
- The list of filtering on author or tag was one hardcoded list, so I have updated it to two separated lists with
fetched values from database. The fetching is in the file **components/Navbar/FilterSortTypes**. I have used async
fetching and put them in a list to be used in dropdown part. Then I have updated the redux reducers and actions
to match this change.
- I have added a new functionality for searching, which make suggestions if one start to write in the search place.
When one starts to write one letter in the search, a list with recommended articles will appear, and then it
will be possible to choose a specific article. Then just this article will be shown.
This work is in the **SearchInput** and **Suggestions** components in the Search folder. In **SearchInput**
I have added a function getInfo which is called from onChangeHandler. This function fetches the 10 article titles
from database based on the search input. Then send the results to **Suggestions** to be displayed on screen.
It still possible to search on writen input in search place.
- The filter alternatives work on all articles, not on the search results.
- I have updated sort to work on search result, not on all articles.
- When one adds a comment, the comment will be visible on the screen for the chosen article. It wasn't possible to
see the new comment directly in the same page. This was by save the last query in reducers and use it on update.
- Updated som design and css.

## Testing
- There wasn't a lot of use of the testing library. I have created a __test__ folder in src, with three tests.
**App.test** **navbar.test** **SearchInput.test** with two snapshots.
To run all tests: ``npm run test``.
###App.test
Tests the AppWrapper
###navbar.test
Tests the presintation of header menu to match snapshots, and tests the filter menu by mouse enter action
and check the dropdown menu.
###SearchInput.test
Tests the search input and search lable and matches the snapshots on loading. I have tried to make a test for search
and article displayed but it tok very short time so fetching was not finished. Therfor I choosed to test search
in end to end testing with Cypress. I have tried to use @teting-library/user-event here.

###End-to-End testing
Has been done by Cypress. You have to call ``npm start`` to both backend and frontend parts before calling the e2e test.
E2E testing is called by ``npm run e2e`` from the prosjekt-4 folder.
I have added more testes:
- Test the suggestions and choose one article and make sure that the article has been displayed.
- Adding comment after search
- Check that 4 articles are displayed at the start.
- Updated other tests.

