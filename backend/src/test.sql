USE `Backend`;
DROP procedure IF EXISTS `test_procedure_for_Article`;

DELIMITER $$
USE `Backend`$$
CREATE PROCEDURE test_procedure_for_Article ()
BEGIN
	DECLARE nCounter INT default 1;
	DECLARE mCounter INT default 1;
    DECLARE nValue INT default 0;
	DECLARE mValue INT default 0;
    select count(*) from Backend.articles into nValue;
	select count(*) from Backend.users into mValue;
	WHILE @nCounter <= nValue DO
    INSERT into Backend.comments (userID, articleID, text) VALUES (mCounter, nCounter, 'Counting? ');
		set @nCounter = nCounter + 1;
		set @mCounter = mCounter + 1;
		if  (@mValue = @mCounter)  THEN
			SET @mCounter = 1;
		end if;
    END WHILE;
END$$

DELIMITER ;

Use Backend;
CALL test_procedure_for_Article
