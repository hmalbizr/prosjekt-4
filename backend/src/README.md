# Installation

To make sure you have all the necessary node modules, make 
sure you are in the directory *backend*, by running ``cd backend``,
then run ```npm install```. That should ensure all the required 
files are downloaded.

## Start the server

You can run the server by the command ``npm start`` in the backend directory.

### Background on the server

If you're running the server with ``npm start`` the following is not useful to
you.

The server is started by running server.js, which must be compiled 
from the TypeScript files in *src*, and they should end
up in *dist*, which is a local folder that should not
be committed to git.
To start the server with node, you should first make sure you've 
compiled the source TypeScript by running ``npm run compile``, which 
also creates the dist folder if it does not yet exist.

## Data in the server

If there isn't any data in the server, run first 
[database_setup.sql](./database_setup.sql) and then
[populate_test_data_old.sql](./populate_test_data_old.sql) and
 [populate_test_data_new.sql](./populate_test_data_new.sql). The file 
 **populate_test_data.sql** is deprecated because we tried too much with 
 it and it mucked up the application.
 
 Oh, they should go to the Schema Backend, using the user ``Master`` with
 with password ``MtoE773#F!``

Do not use this unless it's an emergency.

## Connecting

Running it locally should let you connect to it from 
the React app with GraphQL connecting at ``localhost:8080/graphql``

## Queries

The following code block may be used to show all the 
data currently in the database.
```
{
	user
		{
		id
		username
		}
	article
	{
		id
		timestamp
		title
		text
		}
	author
	{
		id
		firstName
		lastName
	}
	tag
	{
		id
		tag
	}
	comment
	{
		id
		userID
		articleID
		timestamp
		text
	}
	articleTags
	{
		id
		articleID
		tagID
	}
	articleAuthors
	{
		id
		articleID
		authorID
	}
	savedArticles
	{
		id
		userID
		articleID
	}
}
```
