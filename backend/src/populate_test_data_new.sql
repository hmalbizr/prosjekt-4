insert into Backend.articles (title, text)
values ('LitIpsum: Test-article1', '"As a rule," said Holmes, "the more bizarre a thing is the less mysterious it proves to be. It is your commonplace, featureless crimes which are really puzzling, just as a commonplace face is the most difficult to identify. But I must be prompt over this matter."

"What are you going to do, then?" I asked.

"To smoke," he answered. "It is quite a three pipe problem, and I beg that you won''t speak to me for fifty minutes." He curled himself up in his chair, with his thin knees drawn up to his hawk-like nose, and there he sat with his eyes closed and his black clay pipe thrusting out like the bill of some strange bird. I had come to the conclusion that he had dropped asleep, and indeed was nodding myself, when he suddenly sprang out of his chair with the gesture of a man who has made up his mind and put his pipe down upon the mantelpiece.

"Sarasate plays at the St. James''s Hall this afternoon," he remarked. "What do you think, Watson? Could your patients spare you for a few hours?"

"I have nothing to do to-day. My practice is never very absorbing."

"Then put on your hat and come. I am going through the City first, and we can have some lunch on the way. I observe that there is a good deal of German music on the programme, which is rather more to my taste than Italian or French. It is introspective, and I want to introspect. Come along!"

We travelled by the Underground as far as Aldersgate; and a short walk took us to Saxe-Coburg Square, the scene of the singular story which we had listened to in the morning. It was a poky, little, shabby-genteel place, where four lines of dingy two-storied brick houses looked out into a small railed-in enclosure, where a lawn of weedy grass and a few clumps of faded laurel-bushes made a hard fight against a smoke-laden and uncongenial atmosphere. Three gilt balls and a brown board with "JABEZ WILSON" in white letters, upon a corner house, announced the place where our red-headed client carried on his business. Sherlock Holmes stopped in front of it with his head on one side and looked it all over, with his eyes shining brightly between puckered lids. Then he walked slowly up the street, and then down again to the corner, still looking keenly at the houses. Finally he returned to the pawnbroker''s, and, having thumped vigorously upon the pavement with his stick two or three times, he went up to the door and knocked. It was instantly opened by a bright-looking, clean-shaven young fellow, who asked him to step in.

"Thank you," said Holmes, "I only wished to ask you how you would go from here to the Strand."

"Third right, fourth left," answered the assistant promptly, closing the door.

"Smart fellow, that," observed Holmes as we walked away. "He is, in my judgment, the fourth smartest man in London, and for daring I am not sure that he has not a claim to be third. I have known something of him before."

"Evidently," said I, "Mr. Wilson''s assistant counts for a good deal in this mystery of the Red-headed League. I am sure that you inquired your way merely in order that you might see him."

"Not him."

"What then?"

"The knees of his trousers."

"And what did you see?"

"What I expected to see."

"Why did you beat the pavement?"

"My dear doctor, this is a time for observation, not for talk. We are spies in an enemy''s country. We know something of Saxe-Coburg Square. Let us now explore the parts which lie behind it."

The road in which we found ourselves as we turned round the corner from the retired Saxe-Coburg Square presented as great a contrast to it as the front of a picture does to the back. It was one of the main arteries which conveyed the traffic of the City to the north and west. The roadway was blocked with the immense stream of commerce flowing in a double tide inward and outward, while the footpaths were black with the hurrying swarm of pedestrians. It was difficult to realise as we looked at the line of fine shops and stately business premises that they really abutted on the other side upon the faded and stagnant square which we had just quitted.

"Let me see," said Holmes, standing at the corner and glancing along the line, "I should like just to remember the order of the houses here. It is a hobby of mine to have an exact knowledge of London. There is Mortimer''s, the tobacconist, the little newspaper shop, the Coburg branch of the City and Suburban Bank, the Vegetarian Restaurant, and McFarlane''s carriage-building depot. That carries us right on to the other block. And now, Doctor, we''ve done our work, so it''s time we had some play. A sandwich and a cup of coffee, and then off to violin-land, where all is sweetness and delicacy and harmony, and there are no red-headed clients to vex us with their conundrums."

My friend was an enthusiastic musician, being himself not only a very capable performer but a composer of no ordinary merit. All the afternoon he sat in the stalls wrapped in the most perfect happiness, gently waving his long, thin fingers in time to the music, while his gently smiling face and his languid, dreamy eyes were as unlike those of Holmes the sleuth-hound, Holmes the relentless, keen-witted, ready-handed criminal agent, as it was possible to conceive. In his singular character the dual nature alternately asserted itself, and his extreme exactness and astuteness represented, as I have often thought, the reaction against the poetic and contemplative mood which occasionally predominated in him. The swing of his nature took him from extreme languor to devouring energy; and, as I knew well, he was never so truly formidable as when, for days on end, he had been lounging in his armchair amid his improvisations and his black-letter editions. Then it was that the lust of the chase would suddenly come upon him, and that his brilliant reasoning power would rise to the level of intuition, until those who were unacquainted with his methods would look askance at him as on a man whose knowledge was not that of other mortals. When I saw him that afternoon so enwrapped in the music at St. James''s Hall I felt that an evil time might be coming upon those whom he had set himself to hunt down.

"You want to go home, no doubt, Doctor," he remarked as we emerged.

"Yes, it would be as well."

"And I have some business to do which will take some hours. This business at Coburg Square is serious."

"Why serious?"

"A considerable crime is in contemplation. I have every reason to believe that we shall be in time to stop it. But to-day being Saturday rather complicates matters. I shall want your help to-night."

"At what time?"

"Ten will be early enough."

"I shall be at Baker Street at ten."

"Very well. And, I say, Doctor, there may be some little danger, so kindly put your army revolver in your pocket." He waved his hand, turned on his heel, and disappeared in an instant among the crowd.');

insert into Backend.articles (title, text)
values ('LitIpsum: Test-article2', 'His behaviour to her sister was such, during dinner time, as showed an admiration of her, which, though more guarded than formerly, persuaded Elizabeth, that if left wholly to himself, Jane’s happiness, and his own, would be speedily secured. Though she dared not depend upon the consequence, she yet received pleasure from observing his behaviour. It gave her all the animation that her spirits could boast; for she was in no cheerful humour. Mr. Darcy was almost as far from her as the table could divide them. He was on one side of her mother. She knew how little such a situation would give pleasure to either, or make either appear to advantage. She was not near enough to hear any of their discourse, but she could see how seldom they spoke to each other, and how formal and cold was their manner whenever they did. Her mother’s ungraciousness, made the sense of what they owed him more painful to Elizabeth’s mind; and she would, at times, have given anything to be privileged to tell him that his kindness was neither unknown nor unfelt by the whole of the family.

She was in hopes that the evening would afford some opportunity of bringing them together; that the whole of the visit would not pass away without enabling them to enter into something more of conversation than the mere ceremonious salutation attending his entrance. Anxious and uneasy, the period which passed in the drawing-room, before the gentlemen came, was wearisome and dull to a degree that almost made her uncivil. She looked forward to their entrance as the point on which all her chance of pleasure for the evening must depend.

“If he does not come to me, then,” said she, “I shall give him up for ever.”

The gentlemen came; and she thought he looked as if he would have answered her hopes; but, alas! the ladies had crowded round the table, where Miss Bennet was making tea, and Elizabeth pouring out the coffee, in so close a confederacy that there was not a single vacancy near her which would admit of a chair. And on the gentlemen’s approaching, one of the girls moved closer to her than ever, and said, in a whisper:

“The men shan’t come and part us, I am determined. We want none of them; do we?”

Darcy had walked away to another part of the room. She followed him with her eyes, envied everyone to whom he spoke, had scarcely patience enough to help anybody to coffee; and then was enraged against herself for being so silly!

“A man who has once been refused! How could I ever be foolish enough to expect a renewal of his love? Is there one among the sex, who would not protest against such a weakness as a second proposal to the same woman? There is no indignity so abhorrent to their feelings!”

She was a little revived, however, by his bringing back his coffee cup himself; and she seized the opportunity of saying:

“Is your sister at Pemberley still?”

“Yes, she will remain there till Christmas.”

“And quite alone? Have all her friends left her?”

“Mrs. Annesley is with her. The others have been gone on to Scarborough, these three weeks.”

She could think of nothing more to say; but if he wished to converse with her, he might have better success. He stood by her, however, for some minutes, in silence; and, at last, on the young lady’s whispering to Elizabeth again, he walked away.

When the tea-things were removed, and the card-tables placed, the ladies all rose, and Elizabeth was then hoping to be soon joined by him, when all her views were overthrown by seeing him fall a victim to her mother’s rapacity for whist players, and in a few moments after seated with the rest of the party. She now lost every expectation of pleasure. They were confined for the evening at different tables, and she had nothing to hope, but that his eyes were so often turned towards her side of the room, as to make him play as unsuccessfully as herself.

Mrs. Bennet had designed to keep the two Netherfield gentlemen to supper; but their carriage was unluckily ordered before any of the others, and she had no opportunity of detaining them.

“Well girls,” said she, as soon as they were left to themselves, “What say you to the day? I think every thing has passed off uncommonly well, I assure you. The dinner was as well dressed as any I ever saw. The venison was roasted to a turn--and everybody said they never saw so fat a haunch. The soup was fifty times better than what we had at the Lucases’ last week; and even Mr. Darcy acknowledged, that the partridges were remarkably well done; and I suppose he has two or three French cooks at least. And, my dear Jane, I never saw you look in greater beauty. Mrs. Long said so too, for I asked her whether you did not. And what do you think she said besides? ‘Ah! Mrs. Bennet, we shall have her at Netherfield at last.’ She did indeed. I do think Mrs. Long is as good a creature as ever lived--and her nieces are very pretty behaved girls, and not at all handsome: I like them prodigiously.”

Mrs. Bennet, in short, was in very great spirits; she had seen enough of Bingley’s behaviour to Jane, to be convinced that she would get him at last; and her expectations of advantage to her family, when in a happy humour, were so far beyond reason, that she was quite disappointed at not seeing him there again the next day, to make his proposals.

“It has been a very agreeable day,” said Miss Bennet to Elizabeth. “The party seemed so well selected, so suitable one with the other. I hope we may often meet again.”

Elizabeth smiled.

“Lizzy, you must not do so. You must not suspect me. It mortifies me. I assure you that I have now learnt to enjoy his conversation as an agreeable and sensible young man, without having a wish beyond it. I am perfectly satisfied, from what his manners now are, that he never had any design of engaging my affection. It is only that he is blessed with greater sweetness of address, and a stronger desire of generally pleasing, than any other man.”

“You are very cruel,” said her sister, “you will not let me smile, and are provoking me to it every moment.”

“How hard it is in some cases to be believed!”

“And how impossible in others!”

“But why should you wish to persuade me that I feel more than I acknowledge?”

“That is a question which I hardly know how to answer. We all love to instruct, though we can teach only what is not worth knowing. Forgive me; and if you persist in indifference, do not make me your confidante.”

A few days after this visit, Mr. Bingley called again, and alone. His friend had left him that morning for London, but was to return home in ten days time. He sat with them above an hour, and was in remarkably good spirits. Mrs. Bennet invited him to dine with them; but, with many expressions of concern, he confessed himself engaged elsewhere.

“Next time you call,” said she, “I hope we shall be more lucky.”');

insert into Backend.articles (title, text)
values ('LitIpsum: Test-article3', 'As Bradshaw left, the lawyer looked at his watch. “And now, Poole, let us get to ours,” he said; and taking the poker under his arm, led the way into the yard. The scud had banked over the moon, and it was now quite dark. The wind, which only broke in puffs and draughts into that deep well of building, tossed the light of the candle to and fro about their steps, until they came into the shelter of the theatre, where they sat down silently to wait. London hummed solemnly all around; but nearer at hand, the stillness was only broken by the sounds of a footfall moving to and fro along the cabinet floor.

“So it will walk all day, sir,” whispered Poole; “ay, and the better part of the night. Only when a new sample comes from the chemist, there’s a bit of a break. Ah, it’s an ill conscience that’s such an enemy to rest! Ah, sir, there’s blood foully shed in every step of it! But hark again, a little closer--put your heart in your ears, Mr. Utterson, and tell me, is that the doctor’s foot?”

The steps fell lightly and oddly, with a certain swing, for all they went so slowly; it was different indeed from the heavy creaking tread of Henry Jekyll. Utterson sighed. “Is there never anything else?” he asked.

Poole nodded. “Once,” he said. “Once I heard it weeping!”

“Weeping? how that?” said the lawyer, conscious of a sudden chill of horror.

“Weeping like a woman or a lost soul,” said the butler. “I came away with that upon my heart, that I could have wept too.”

But now the ten minutes drew to an end. Poole disinterred the axe from under a stack of packing straw; the candle was set upon the nearest table to light them to the attack; and they drew near with bated breath to where that patient foot was still going up and down, up and down, in the quiet of the night. “Jekyll,” cried Utterson, with a loud voice, “I demand to see you.” He paused a moment, but there came no reply. “I give you fair warning, our suspicions are aroused, and I must and shall see you,” he resumed; “if not by fair means, then by foul--if not of your consent, then by brute force!”

“Utterson,” said the voice, “for God’s sake, have mercy!”

“Ah, that’s not Jekyll’s voice--it’s Hyde’s!” cried Utterson. “Down with the door, Poole!”

Poole swung the axe over his shoulder; the blow shook the building, and the red baize door leaped against the lock and hinges. A dismal screech, as of mere animal terror, rang from the cabinet. Up went the axe again, and again the panels crashed and the frame bounded; four times the blow fell; but the wood was tough and the fittings were of excellent workmanship; and it was not until the fifth, that the lock burst and the wreck of the door fell inwards on the carpet.

The besiegers, appalled by their own riot and the stillness that had succeeded, stood back a little and peered in. There lay the cabinet before their eyes in the quiet lamplight, a good fire glowing and chattering on the hearth, the kettle singing its thin strain, a drawer or two open, papers neatly set forth on the business table, and nearer the fire, the things laid out for tea; the quietest room, you would have said, and, but for the glazed presses full of chemicals, the most commonplace that night in London.

Right in the middle there lay the body of a man sorely contorted and still twitching. They drew near on tiptoe, turned it on its back and beheld the face of Edward Hyde. He was dressed in clothes far too large for him, clothes of the doctor’s bigness; the cords of his face still moved with a semblance of life, but life was quite gone: and by the crushed phial in the hand and the strong smell of kernels that hung upon the air, Utterson knew that he was looking on the body of a self-destroyer.

“We have come too late,” he said sternly, “whether to save or punish. Hyde is gone to his account; and it only remains for us to find the body of your master.”

The far greater proportion of the building was occupied by the theatre, which filled almost the whole ground storey and was lighted from above, and by the cabinet, which formed an upper story at one end and looked upon the court. A corridor joined the theatre to the door on the by-street; and with this the cabinet communicated separately by a second flight of stairs. There were besides a few dark closets and a spacious cellar. All these they now thoroughly examined. Each closet needed but a glance, for all were empty, and all, by the dust that fell from their doors, had stood long unopened. The cellar, indeed, was filled with crazy lumber, mostly dating from the times of the surgeon who was Jekyll’s predecessor; but even as they opened the door they were advertised of the uselessness of further search, by the fall of a perfect mat of cobweb which had for years sealed up the entrance. No where was there any trace of Henry Jekyll dead or alive.

Poole stamped on the flags of the corridor. “He must be buried here,” he said, hearkening to the sound.

“Or he may have fled,” said Utterson, and he turned to examine the door in the by-street. It was locked; and lying near by on the flags, they found the key, already stained with rust.

“This does not look like use,” observed the lawyer.

“Use!” echoed Poole. “Do you not see, sir, it is broken? much as if a man had stamped on it.”

“Ay,” continued Utterson, “and the fractures, too, are rusty.” The two men looked at each other with a scare. “This is beyond me, Poole,” said the lawyer. “Let us go back to the cabinet.”

They mounted the stair in silence, and still with an occasional awestruck glance at the dead body, proceeded more thoroughly to examine the contents of the cabinet. At one table, there were traces of chemical work, various measured heaps of some white salt being laid on glass saucers, as though for an experiment in which the unhappy man had been prevented.

“That is the same drug that I was always bringing him,” said Poole; and even as he spoke, the kettle with a startling noise boiled over.

This brought them to the fireside, where the easy-chair was drawn cosily up, and the tea things stood ready to the sitter’s elbow, the very sugar in the cup. There were several books on a shelf; one lay beside the tea things open, and Utterson was amazed to find it a copy of a pious work, for which Jekyll had several times expressed a great esteem, annotated, in his own hand with startling blasphemies.

Next, in the course of their review of the chamber, the searchers came to the cheval-glass, into whose depths they looked with an involuntary horror. But it was so turned as to show them nothing but the rosy glow playing on the roof, the fire sparkling in a hundred repetitions along the glazed front of the presses, and their own pale and fearful countenances stooping to look in.

“This glass has seen some strange things, sir,” whispered Poole.');

insert into Backend.articles (title, text)
values ('LitIpsum: Test-article4', '"Whoever she is, I wish she would mind her own affairs: I don''t know what the devil a woman lives for after thirty: she is only in other folk''s way. Shall you be at the assembly?"

"I believe not, my Lord."

"No!-why then, how in the world can you contrive to pass your time?"

"In a manner which your Lordship will think very extraordinary," cried Mrs.  Selwyn, "for the young lady reads."

"Ha, ha, ha! Egad, my Lord," cried the facetious companion, "you are got into bad hands."

"You had better, Ma''am," answered he, "attack Jack Coverley here, for you will make nothing of me."

"Of you, my Lord," cried she, "Heaven forbid I should ever entertain so idle an expectation! I only talk, like a silly woman, for the sake of talking; but I have by no means so low an opinion of your Lordship, as to suppose you vulnerable to censure."

"Do, pray, Ma''am," cried he, "turn to Jack Coverley; he''s the very man for you;-he''d be a wit himself if he was not too modest."

"Prithee, my Lord, be quiet," returned the other; "if the lady is contented to bestow all her favours upon you, why should you make such a point of my going snacks?"

"Don''t be apprehensive, gentlemen," said Mrs. Selwyn, drily, "I am not romantic;-I have not the least design of doing good to either of you."

"Have not you been ill since I saw you?" said his Lordship, again addressing himself to me.

"Yes, my Lord."

"I thought so; you are paler than you was, and I suppose that''s the reason I did not recollect you sooner."

"Has not your Lordship too much gallantry," cried Mrs. Selwyn, "to discover a young lady''s illness by her looks?"

"The devil a word can I speak for that woman," said he, in a low voice; "do, prithee, Jack, take her in hand."

"Excuse me, my Lord," answered Mr. Coverley.

"When shall I see you again?" continued his Lordship; "do you go to the pump-room every morning?"

"No, my Lord."

"Do you ride out?"

"No, my Lord."

Just then we arrived at the pump-room, and an end was put to our conversation, if it is not an abuse of words to give such a term to a string of rude questions and free compliments.

He had not opportunity to say much more to me, as Mrs. Selwyn joined a large party, and I walked home between two ladies. He had, however, the curiosity to see us to the door.

Mrs. Selwyn was very eager to know how I had made acquaintance with this nobleman, whose manners so evidently announced the character of a confirmed libertine. I could give her very little satisfaction, as I was ignorant even of his name: but, in the afternoon, Mr. Ridgeway, the apothecary, gave us very ample information.

As his person was easily described, for he is remarkably tall, Mr. Ridgeway told us he was Lord Merton, a nobleman who is but lately come to his title, though he has already dissipated more than half his fortune; a professed admirer of beauty, but a man of most licentious character; that among men, his companions consisted chiefly of gamblers and jockeys, and among women he was rarely admitted.

"Well, Miss Anville," said Mrs. Selwyn, "I am glad I was not more civil to him. You may depend upon me for keeping him at a distance."

"O, Madam," said Mr. Ridgeway, "he may now be admitted any where, for he is going to reform."

"Has he, under that notion, persuaded any fool to marry him?"

"Not yet, Madam, but a marriage is expected to take place shortly: it has been some time in agitation; but the friends of the lady have obliged her to wait till she is of age: however, her brother, who has chiefly opposed the match, now that she is near being at her own disposal, is tolerably quiet. She is very pretty, and will have a large fortune. We expect her at the Wells every day."

"What is her name?" said Mrs. Selwyn.

"Larpent," answered he: "Lady Louisa Larpent, sister of Lord Orville."

"Lord Orville!" repeated I, all amazement.

"Yes, Ma''am; his Lordship is coming with her. I have had certain information.  They are to be at the Honourable Mrs. Beaumont''s. She is a relation of my Lord''s, and has a very fine house upon Clifton Hill."

His Lordship is coming with her! -Good God, what an emotion did those words give me! How strange, my dear Sir, that, just at this time, he should visit Bristol! It will be impossible for me to avoid seeing him, as Mrs. Selwyn is very well acquainted with Mrs. Beaumont. Indeed, I have had an escape in not being under the same roof with him, for Mrs. Beaumont invited us to her house immediately upon our arrival; but the inconvenience of being so distant from the pump-room made Mrs. Selwyn decline her civility.

Oh that the first meeting were over!-or that I could quit Bristol without seeing him!-inexpressibly do I dread an interview! Should the same impertinent freedom be expressed by his looks, which dictated this cruel letter, I shall not know how to endure either him or myself. Had I but returned it, I should be easier, because my sentiments of it would then be known to him; but now, he can only gather them from my behaviour; and I tremble lest he should mistake my indignation for confusion!-lest he should misconstrue my reserve into embarrassment!-for how, my dearest Sir, how shall I be able totally to divest myself of the respect with which I have been used to think of him?-the pleasure with which I have been used to see him?

Surely he, as well as I, must recollect the  the moment of our meeting; and he will, probably, mean to gather my thoughts of it from my looks;-oh that they could but convey to him my real detestation of impertinence and vanity! then would he see how much he had mistaken my disposition when he imagined them my due.

There was a time when the very idea that such a man as Lord Merton should ever be connected with Lord Orville would have both surprised and shocked me; and even yet I am pleased to hear of his repugnance to the marriage.

But how strange, that a man of so abandoned a character should be the choice of a sister of Lord Orville! and how strange, that, almost at the moment of the union, he should be so importunate in gallantry to another woman! What a world is this we live in! how corrupt! how degenerate! well might I be contented to see no more of it! If I find that the eyes of Lord Orville agree with his pen,-I shall then think, that of all mankind, the only virtuous individual resides at Berry Hill.

.

EVELINA IN CONTINUATION.  Bristol Hotwells, Sept. 16th.');


insert into Backend.articles (title, text)
VALUES ('LitIpsum: An article', '"Let me write this all out now. We must be ready for Dr. Van Helsing when he comes. I have sent a telegram to Jonathan to come on here when he arrives in London from Whitby. In this matter dates are everything, and I think that if we get all our material ready, and have every item put in chronological order, we shall have done much. You tell me that Lord Godalming and Mr. Morris are coming too. Let us be able to tell him when they come." He accordingly set the phonograph at a slow pace, and I began to typewrite from the beginning of the seventh cylinder. I used manifold, and so took three copies of the diary, just as I had done with all the rest. It was late when I got through, but Dr. Seward went about his work of going his round of the patients; when he had finished he came back and sat near me, reading, so that I did not feel too lonely whilst I worked. How good and thoughtful he is; the world seems full of good men--even if there are monsters in it. Before I left him I remembered what Jonathan put in his diary of the Professor''s perturbation at reading something in an evening paper at the station at Exeter; so, seeing that Dr. Seward keeps his newspapers, I borrowed the files of "The Westminster Gazette" and "The Pall Mall Gazette," and took them to my room. I remember how much "The Dailygraph" and "The Whitby Gazette," of which I had made cuttings, helped us to understand the terrible events at Whitby when Count Dracula landed, so I shall look through the evening papers since then, and perhaps I shall get some new light. I am not sleepy, and the work will help to keep me quiet. 30 September.--Mr. Harker arrived at nine o''clock. He had got his wife''s wire just before starting. He is uncommonly clever, if one can judge from his face, and full of energy. If this journal be true--and judging by one''s own wonderful experiences, it must be--he is also a man of great nerve. That going down to the vault a second time was a remarkable piece of daring. After reading his account of it I was prepared to meet a good specimen of manhood, but hardly the quiet, business-like gentleman who came here to-day. Later.--After lunch Harker and his wife went back to their own room, and as I passed a while ago I heard the click of the typewriter. They are hard at it. Mrs. Harker says that they are knitting together in chronological order every scrap of evidence they have. Harker has got the letters between the consignee of the boxes at Whitby and the carriers in London who took charge of them. He is now reading his wife''s typescript of my diary. I wonder what they make out of it. Here it is....

Strange that it never struck me that the very next house might be the Count''s hiding-place! Goodness knows that we had enough clues from the conduct of the patient Renfield! The bundle of letters relating to the purchase of the house were with the typescript. Oh, if we had only had them earlier we might have saved poor Lucy! Stop; that way madness lies! Harker has gone back, and is again collating his material. He says that by dinner-time they will be able to show a whole connected narrative. He thinks that in the meantime I should see Renfield, as hitherto he has been a sort of index to the coming and going of the Count. I hardly see this yet, but when I get at the dates I suppose I shall. What a good thing that Mrs. Harker put my cylinders into type! We never could have found the dates otherwise....');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Some other article', '“The disagreement subsisting between yourself and my late honoured father always gave me much uneasiness, and since I have had the misfortune to lose him, I have frequently wished to heal the breach; but for some time I was kept back by my own doubts, fearing lest it might seem disrespectful to his memory for me to be on good terms with anyone with whom it had always pleased him to be at variance.--‘There, Mrs. Bennet.’--My mind, however, is now made up on the subject, for having received ordination at Easter, I have been so fortunate as to be distinguished by the patronage of the Right Honourable Lady Catherine de Bourgh, widow of Sir Lewis de Bourgh, whose bounty and beneficence has preferred me to the valuable rectory of this parish, where it shall be my earnest endeavour to demean myself with grateful respect towards her ladyship, and be ever ready to perform those rites and ceremonies which are instituted by the Church of England. As a clergyman, moreover, I feel it my duty to promote and establish the blessing of peace in all families within the reach of my influence; and on these grounds I flatter myself that my present overtures are highly commendable, and that the circumstance of my being next in the entail of Longbourn estate will be kindly overlooked on your side, and not lead you to reject the offered olive-branch. I cannot be otherwise than concerned at being the means of injuring your amiable daughters, and beg leave to apologise for it, as well as to assure you of my readiness to make them every possible amends--but of this hereafter. If you should have no objection to receive me into your house, I propose myself the satisfaction of waiting on you and your family, Monday, November 18th, by four o’clock, and shall probably trespass on your hospitality till the Saturday se’ennight following, which I can do without any inconvenience, as Lady Catherine is far from objecting to my occasional absence on a Sunday, provided that some other clergyman is engaged to do the duty of the day.--I remain, dear sir, with respectful compliments to your lady and daughters, your well-wisher and friend,

“WILLIAM COLLINS”

“At four o’clock, therefore, we may expect this peace-making gentleman,”  said Mr. Bennet, as he folded up the letter. “He seems to be a most conscientious and polite young man, upon my word, and I doubt not will prove a valuable acquaintance, especially if Lady Catherine should be so indulgent as to let him come to us again.”

“There is some sense in what he says about the girls, however, and if he is disposed to make them any amends, I shall not be the person to discourage him.”

“Though it is difficult,” said Jane, “to guess in what way he can mean to make us the atonement he thinks our due, the wish is certainly to his credit.”

Elizabeth was chiefly struck by his extraordinary deference for Lady Catherine, and his kind intention of christening, marrying, and burying his parishioners whenever it were required.

“He must be an oddity, I think,” said she. “I cannot make him out.--There is something very pompous in his style.--And what can he mean by apologising for being next in the entail?--We cannot suppose he would help it if he could.--Could he be a sensible man, sir?”

“No, my dear, I think not. I have great hopes of finding him quite the reverse. There is a mixture of servility and self-importance in his letter, which promises well. I am impatient to see him.”

“In point of composition,” said Mary, “the letter does not seem defective. The idea of the olive-branch perhaps is not wholly new, yet I think it is well expressed.”

To Catherine and Lydia, neither the letter nor its writer were in any degree interesting. It was next to impossible that their cousin should come in a scarlet coat, and it was now some weeks since they had received pleasure from the society of a man in any other colour. As for their mother, Mr. Collins’s letter had done away much of her ill-will, and she was preparing to see him with a degree of composure which astonished her husband and daughters.

Mr. Collins was punctual to his time, and was received with great politeness by the whole family. Mr. Bennet indeed said little; but the ladies were ready enough to talk, and Mr. Collins seemed neither in need of encouragement, nor inclined to be silent himself. He was a tall, heavy-looking young man of five-and-twenty. His air was grave and stately, and his manners were very formal. He had not been long seated before he complimented Mrs. Bennet on having so fine a family of daughters; said he had heard much of their beauty, but that in this instance fame had fallen short of the truth; and added, that he did not doubt her seeing them all in due time disposed of in marriage. This gallantry was not much to the taste of some of his hearers; but Mrs. Bennet, who quarreled with no compliments, answered most readily.

“You are very kind, I am sure; and I wish with all my heart it may prove so, for else they will be destitute enough. Things are settled so oddly.”

“You allude, perhaps, to the entail of this estate.”

“Ah! sir, I do indeed. It is a grievous affair to my poor girls, you must confess. Not that I mean to find fault with you, for such things I know are all chance in this world. There is no knowing how estates will go when once they come to be entailed.”

“I am very sensible, madam, of the hardship to my fair cousins, and could say much on the subject, but that I am cautious of appearing forward and precipitate. But I can assure the young ladies that I come prepared to admire them. At present I will not say more; but, perhaps, when we are better acquainted--”

He was interrupted by a summons to dinner; and the girls smiled on each other. They were not the only objects of Mr. Collins’s admiration. The hall, the dining-room, and all its furniture, were examined and praised; and his commendation of everything would have touched Mrs. Bennet’s heart, but for the mortifying supposition of his viewing it all as his own future property. The dinner too in its turn was highly admired; and he begged to know to which of his fair cousins the excellency of its cooking was owing. But he was set right there by Mrs. Bennet, who assured him with some asperity that they were very well able to keep a good cook, and that her daughters had nothing to do in the kitchen. He begged pardon for having displeased her. In a softened tone she declared herself not at all offended; but he continued to apologise for about a quarter of an hour.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: The dairy of Jenz', '“I am sure there is not on his. I will answer for it, he never cared three straws about her--who could about such a nasty little freckled thing?”

Elizabeth was shocked to think that, however incapable of such coarseness of expression herself, the coarseness of the sentiment was little other than her own breast had harboured and fancied liberal!

As soon as all had ate, and the elder ones paid, the carriage was ordered; and after some contrivance, the whole party, with all their boxes, work-bags, and parcels, and the unwelcome addition of Kitty’s and Lydia’s purchases, were seated in it.

“How nicely we are all crammed in,” cried Lydia. “I am glad I bought my bonnet, if it is only for the fun of having another bandbox! Well, now let us be quite comfortable and snug, and talk and laugh all the way home. And in the first place, let us hear what has happened to you all since you went away. Have you seen any pleasant men? Have you had any flirting? I was in great hopes that one of you would have got a husband before you came back. Jane will be quite an old maid soon, I declare. She is almost three-and-twenty! Lord, how ashamed I should be of not being married before three-and-twenty! My aunt Phillips wants you so to get husbands, you can’t think. She says Lizzy had better have taken Mr. Collins; but I do not think there would have been any fun in it. Lord! how I should like to be married before any of you; and then I would chaperon you about to all the balls. Dear me! we had such a good piece of fun the other day at Colonel Forster’s. Kitty and me were to spend the day there, and Mrs. Forster promised to have a little dance in the evening; (by the bye, Mrs. Forster and me are such friends!) and so she asked the two Harringtons to come, but Harriet was ill, and so Pen was forced to come by herself; and then, what do you think we did? We dressed up Chamberlayne in woman’s clothes on purpose to pass for a lady, only think what fun! Not a soul knew of it, but Colonel and Mrs. Forster, and Kitty and me, except my aunt, for we were forced to borrow one of her gowns; and you cannot imagine how well he looked! When Denny, and Wickham, and Pratt, and two or three more of the men came in, they did not know him in the least. Lord! how I laughed! and so did Mrs. Forster. I thought I should have died. And that made the men suspect something, and then they soon found out what was the matter.”

With such kinds of histories of their parties and good jokes, did Lydia, assisted by Kitty’s hints and additions, endeavour to amuse her companions all the way to Longbourn. Elizabeth listened as little as she could, but there was no escaping the frequent mention of Wickham’s name.

Their reception at home was most kind. Mrs. Bennet rejoiced to see Jane in undiminished beauty; and more than once during dinner did Mr. Bennet say voluntarily to Elizabeth:

“I am glad you are come back, Lizzy.”

Their party in the dining-room was large, for almost all the Lucases came to meet Maria and hear the news; and various were the subjects that occupied them: Lady Lucas was inquiring of Maria, after the welfare and poultry of her eldest daughter; Mrs. Bennet was doubly engaged, on one hand collecting an account of the present fashions from Jane, who sat some way below her, and, on the other, retailing them all to the younger Lucases; and Lydia, in a voice rather louder than any other person’s, was enumerating the various pleasures of the morning to anybody who would hear her.

“Oh! Mary,” said she, “I wish you had gone with us, for we had such fun! As we went along, Kitty and I drew up the blinds, and pretended there was nobody in the coach; and I should have gone so all the way, if Kitty had not been sick; and when we got to the George, I do think we behaved very handsomely, for we treated the other three with the nicest cold luncheon in the world, and if you would have gone, we would have treated you too. And then when we came away it was such fun! I thought we never should have got into the coach. I was ready to die of laughter. And then we were so merry all the way home! we talked and laughed so loud, that anybody might have heard us ten miles off!”

To this Mary very gravely replied, “Far be it from me, my dear sister, to depreciate such pleasures! They would doubtless be congenial with the generality of female minds. But I confess they would have no charms for me--I should infinitely prefer a book.”

But of this answer Lydia heard not a word. She seldom listened to anybody for more than half a minute, and never attended to Mary at all.

In the afternoon Lydia was urgent with the rest of the girls to walk to Meryton, and to see how everybody went on; but Elizabeth steadily opposed the scheme. It should not be said that the Miss Bennets could not be at home half a day before they were in pursuit of the officers. There was another reason too for her opposition. She dreaded seeing Mr. Wickham again, and was resolved to avoid it as long as possible. The comfort to her of the regiment’s approaching removal was indeed beyond expression. In a fortnight they were to go--and once gone, she hoped there could be nothing more to plague her on his account.

She had not been many hours at home before she found that the Brighton scheme, of which Lydia had given them a hint at the inn, was under frequent discussion between her parents. Elizabeth saw directly that her father had not the smallest intention of yielding; but his answers were at the same time so vague and equivocal, that her mother, though often disheartened, had never yet despaired of succeeding at last.

Elizabeth’s impatience to acquaint Jane with what had happened could no longer be overcome; and at length, resolving to suppress every particular in which her sister was concerned, and preparing her to be surprised, she related to her the next morning the chief of the scene between Mr. Darcy and herself.

Miss Bennet’s astonishment was soon lessened by the strong sisterly partiality which made any admiration of Elizabeth appear perfectly natural; and all surprise was shortly lost in other feelings. She was sorry that Mr. Darcy should have delivered his sentiments in a manner so little suited to recommend them; but still more was she grieved for the unhappiness which her sister’s refusal must have given him.

“His being so sure of succeeding was wrong,” said she, “and certainly ought not to have appeared; but consider how much it must increase his disappointment!”

“Indeed,” replied Elizabeth, “I am heartily sorry for him; but he has other feelings, which will probably soon drive away his regard for me. You do not blame me, however, for refusing him?”

“Blame you! Oh, no.”

“But you blame me for having spoken so warmly of Wickham?”

“No--I do not know that you were wrong in saying what you did.”');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Four Horsemen met at a crossroads', 'We then sought for Skinsky, but were unable to find him. One of his neighbours, who did not seem to bear him any affection, said that he had gone away two days before, no one knew whither. This was corroborated by his landlord, who had received by messenger the key of the house together with the rent due, in English money. This had been between ten and eleven o''clock last night. We were at a standstill again.

Whilst we were talking one came running and breathlessly gasped out that the body of Skinsky had been found inside the wall of the churchyard of St. Peter, and that the throat had been torn open as if by some wild animal. Those we had been speaking with ran off to see the horror, the women crying out "This is the work of a Slovak!" We hurried away lest we should have been in some way drawn into the affair, and so detained.

As we came home we could arrive at no definite conclusion. We were all convinced that the box was on its way, by water, to somewhere; but where that might be we would have to discover. With heavy hearts we came home to the hotel to Mina.

When we met together, the first thing was to consult as to taking Mina again into our confidence. Things are getting desperate, and it is at least a chance, though a hazardous one. As a preliminary step, I was released from my promise to her. 30 October, evening.--They were so tired and worn out and dispirited that there was nothing to be done till they had some rest; so I asked them all to lie down for half an hour whilst I should enter everything up to the moment. I feel so grateful to the man who invented the "Traveller''s" typewriter, and to Mr. Morris for getting this one for me. I should have felt quite; astray doing the work if I had to write with a pen....

It is all done; poor dear, dear Jonathan, what he must have suffered, what must he be suffering now. He lies on the sofa hardly seeming to breathe, and his whole body appears in collapse. His brows are knit; his face is drawn with pain. Poor fellow, maybe he is thinking, and I can see his face all wrinkled up with the concentration of his thoughts. Oh! if I could only help at all.... I shall do what I can.

I have asked Dr. Van Helsing, and he has got me all the papers that I have not yet seen.... Whilst they are resting, I shall go over all carefully, and perhaps I may arrive at some conclusion. I shall try to follow the Professor''s example, and think without prejudice on the facts before me....

I do believe that under God''s providence I have made a discovery. I shall get the maps and look over them....

I am more than ever sure that I am right. My new conclusion is ready, so I shall get our party together and read it. They can judge it; it is well to be accurate, and every minute is precious. Ground of inquiry.--Count Dracula''s problem is to get back to his own place.

(a) He must be brought back by some one. This is evident; for had he power to move himself as he wished he could go either as man, or wolf, or bat, or in some other way. He evidently fears discovery or interference, in the state of helplessness in which he must be--confined as he is between dawn and sunset in his wooden box.

(b) How is he to be taken?--Here a process of exclusions may help us. By road, by rail, by water?

1. By Road.--There are endless difficulties, especially in leaving the city.

(x) There are people; and people are curious, and investigate. A hint, a surmise, a doubt as to what might be in the box, would destroy him.

(y) There are, or there may be, customs and octroi officers to pass.

(z) His pursuers might follow. This is his highest fear; and in order to prevent his being betrayed he has repelled, so far as he can, even his victim--me!

2. By Rail.--There is no one in charge of the box. It would have to take its chance of being delayed; and delay would be fatal, with enemies on the track. True, he might escape at night; but what would he be, if left in a strange place with no refuge that he could fly to? This is not what he intends; and he does not mean to risk it.

3. By Water.--Here is the safest way, in one respect, but with most danger in another. On the water he is powerless except at night; even then he can only summon fog and storm and snow and his wolves. But were he wrecked, the living water would engulf him, helpless; and he would indeed be lost. He could have the vessel drive to land; but if it were unfriendly land, wherein he was not free to move, his position would still be desperate.

We know from the record that he was on the water; so what we have to do is to ascertain what water.

The first thing is to realise exactly what he has done as yet; we may, then, get a light on what his later task is to be.

Firstly.--We must differentiate between what he did in London as part of his general plan of action, when he was pressed for moments and had to arrange as best he could.

Secondly we must see, as well as we can surmise it from the facts we know of, what he has done here.

As to the first, he evidently intended to arrive at Galatz, and sent invoice to Varna to deceive us lest we should ascertain his means of exit from England; his immediate and sole purpose then was to escape. The proof of this, is the letter of instructions sent to Immanuel Hildesheim to clear and take away the box before sunrise. There is also the instruction to Petrof Skinsky. These we must only guess at; but there must have been some letter or message, since Skinsky came to Hildesheim.

That, so far, his plans were successful we know. The Czarina Catherine made a phenomenally quick journey--so much so that Captain Donelson''s suspicions were aroused; but his superstition united with his canniness played the Count''s game for him, and he ran with his favouring wind through fogs and all till he brought up blindfold at Galatz. That the Count''s arrangements were well made, has been proved. Hildesheim cleared the box, took it off, and gave it to Skinsky. Skinsky took it--and here we lose the trail. We only know that the box is somewhere on the water, moving along. The customs and the octroi, if there be any, have been avoided.

Now we come to what the Count must have done after his arrival--on land, at Galatz.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: If you can get it to work', '"You may imagine, Mr. Holmes, that to me, destitute as I was, such an offer seemed almost too good to be true. The gentleman, however, seeing perhaps the look of incredulity upon my face, opened a pocket-book and took out a note.

"''It is also my custom,'' said he, smiling in the most pleasant fashion until his eyes were just two little shining slits amid the white creases of his face, ''to advance to my young ladies half their salary beforehand, so that they may meet any little expenses of their journey and their wardrobe.''

"It seemed to me that I had never met so fascinating and so thoughtful a man. As I was already in debt to my tradesmen, the advance was a great convenience, and yet there was something unnatural about the whole transaction which made me wish to know a little more before I quite committed myself.

"''May I ask where you live, sir?'' said I.

"''Hampshire. Charming rural place. The Copper Beeches, five miles on the far side of Winchester. It is the most lovely country, my dear young lady, and the dearest old country-house.''

"''And my duties, sir? I should be glad to know what they would be.''

"''One child--one dear little romper just six years old. Oh, if you could see him killing cockroaches with a slipper! Smack! smack! smack! Three gone before you could wink!'' He leaned back in his chair and laughed his eyes into his head again.

"I was a little startled at the nature of the child''s amusement, but the father''s laughter made me think that perhaps he was joking.

"''My sole duties, then,'' I asked, ''are to take charge of a single child?''

"''No, no, not the sole, not the sole, my dear young lady,'' he cried. ''Your duty would be, as I am sure your good sense would suggest, to obey any little commands my wife might give, provided always that they were such commands as a lady might with propriety obey. You see no difficulty, heh?''

"''I should be happy to make myself useful.''

"''Quite so. In dress now, for example. We are faddy people, you know--faddy but kind-hearted. If you were asked to wear any dress which we might give you, you would not object to our little whim. Heh?''

"''No,'' said I, considerably astonished at his words.

"''Or to sit here, or sit there, that would not be offensive to you?''

"''Oh, no.''

"''Or to cut your hair quite short before you come to us?''

"I could hardly believe my ears. As you may observe, Mr. Holmes, my hair is somewhat luxuriant, and of a rather peculiar tint of chestnut. It has been considered artistic. I could not dream of sacrificing it in this offhand fashion.

"''I am afraid that that is quite impossible,'' said I. He had been watching me eagerly out of his small eyes, and I could see a shadow pass over his face as I spoke.

"''I am afraid that it is quite essential,'' said he. ''It is a little fancy of my wife''s, and ladies'' fancies, you know, madam, ladies'' fancies must be consulted. And so you won''t cut your hair?''

"''No, sir, I really could not,'' I answered firmly.

"''Ah, very well; then that quite settles the matter. It is a pity, because in other respects you would really have done very nicely. In that case, Miss Stoper, I had best inspect a few more of your young ladies.''

"The manageress had sat all this while busy with her papers without a word to either of us, but she glanced at me now with so much annoyance upon her face that I could not help suspecting that she had lost a handsome commission through my refusal.

"''Do you desire your name to be kept upon the books?'' she asked.

"''If you please, Miss Stoper.''

"''Well, really, it seems rather useless, since you refuse the most excellent offers in this fashion,'' said she sharply. ''You can hardly expect us to exert ourselves to find another such opening for you. Good-day to you, Miss Hunter.'' She struck a gong upon the table, and I was shown out by the page.

"Well, Mr. Holmes, when I got back to my lodgings and found little enough in the cupboard, and two or three bills upon the table, I began to ask myself whether I had not done a very foolish thing. After all, if these people had strange fads and expected obedience on the most extraordinary matters, they were at least ready to pay for their eccentricity. Very few governesses in England are getting 100 pounds a year. Besides, what use was my hair to me? Many people are improved by wearing it short and perhaps I should be among the number. Next day I was inclined to think that I had made a mistake, and by the day after I was sure of it. I had almost overcome my pride so far as to go back to the agency and inquire whether the place was still open when I received this letter from the gentleman himself. I have it here and I will read it to you:

"''The Copper Beeches, near Winchester. "''DEAR MISS HUNTER:--Miss Stoper has very kindly given me your address, and I write from here to ask you whether you have reconsidered your decision. My wife is very anxious that you should come, for she has been much attracted by my description of you. We are willing to give 30 pounds a quarter, or 120 pounds a year, so as to recompense you for any little inconvenience which our fads may cause you. They are not very exacting, after all. My wife is fond of a particular shade of electric blue and would like you to wear such a dress indoors in the morning. You need not, however, go to the expense of purchasing one, as we have one belonging to my dear daughter Alice (now in Philadelphia), which would, I should think, fit you very well. Then, as to sitting here or there, or amusing yourself in any manner indicated, that need cause you no inconvenience. As regards your hair, it is no doubt a pity, especially as I could not help remarking its beauty during our short interview, but I am afraid that I must remain firm upon this point, and I only hope that the increased salary may recompense you for the loss. Your duties, as far as the child is concerned, are very light. Now do try to come, and I shall meet you with the dog-cart at Winchester. Let me know your train. Yours faithfully, JEPHRO RUCASTLE.''

"That is the letter which I have just received, Mr. Holmes, and my mind is made up that I will accept it. I thought, however, that before taking the final step I should like to submit the whole matter to your consideration."

"Well, Miss Hunter, if your mind is made up, that settles the question," said Holmes, smiling.

"But you would not advise me to refuse?"

"I confess that it is not the situation which I should like to see a sister of mine apply for."

"What is the meaning of it all, Mr. Holmes?"

"Ah, I have no data. I cannot tell. Perhaps you have yourself formed some opinion?"');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Somehow this is just not as fun', '"When I went down there I found him talking with his son, so I smoked a cigar and waited behind a tree until he should be alone. But as I listened to his talk all that was black and bitter in me seemed to come uppermost. He was urging his son to marry my daughter with as little regard for what she might think as if she were a slut from off the streets. It drove me mad to think that I and all that I held most dear should be in the power of such a man as this. Could I not snap the bond? I was already a dying and a desperate man. Though clear of mind and fairly strong of limb, I knew that my own fate was sealed. But my memory and my girl! Both could be saved if I could but silence that foul tongue. I did it, Mr. Holmes. I would do it again. Deeply as I have sinned, I have led a life of martyrdom to atone for it. But that my girl should be entangled in the same meshes which held me was more than I could suffer. I struck him down with no more compunction than if he had been some foul and venomous beast. His cry brought back his son; but I had gained the cover of the wood, though I was forced to go back to fetch the cloak which I had dropped in my flight. That is the true story, gentlemen, of all that occurred."

"Well, it is not for me to judge you," said Holmes as the old man signed the statement which had been drawn out. "I pray that we may never be exposed to such a temptation."

"I pray not, sir. And what do you intend to do?"

"In view of your health, nothing. You are yourself aware that you will soon have to answer for your deed at a higher court than the Assizes. I will keep your confession, and if McCarthy is condemned I shall be forced to use it. If not, it shall never be seen by mortal eye; and your secret, whether you be alive or dead, shall be safe with us."

"Farewell, then," said the old man solemnly. "Your own deathbeds, when they come, will be the easier for the thought of the peace which you have given to mine." Tottering and shaking in all his giant frame, he stumbled slowly from the room.

"God help us!" said Holmes after a long silence. "Why does fate play such tricks with poor, helpless worms? I never hear of such a case as this that I do not think of Baxter''s words, and say, ''There, but for the grace of God, goes Sherlock Holmes.''"

James McCarthy was acquitted at the Assizes on the strength of a number of objections which had been drawn out by Holmes and submitted to the defending counsel. Old Turner lived for seven months after our interview, but he is now dead; and there is every prospect that the son and daughter may come to live happily together in ignorance of the black cloud which rests upon their past.

When I glance over my notes and records of the Sherlock Holmes cases between the years ''82 and ''90, I am faced by so many which present strange and interesting features that it is no easy matter to know which to choose and which to leave. Some, however, have already gained publicity through the papers, and others have not offered a field for those peculiar qualities which my friend possessed in so high a degree, and which it is the object of these papers to illustrate. Some, too, have baffled his analytical skill, and would be, as narratives, beginnings without an ending, while others have been but partially cleared up, and have their explanations founded rather upon conjecture and surmise than on that absolute logical proof which was so dear to him. There is, however, one of these last which was so remarkable in its details and so startling in its results that I am tempted to give some account of it in spite of the fact that there are points in connection with it which never have been, and probably never will be, entirely cleared up.

The year ''87 furnished us with a long series of cases of greater or less interest, of which I retain the records. Among my headings under this one twelve months I find an account of the adventure of the Paradol Chamber, of the Amateur Mendicant Society, who held a luxurious club in the lower vault of a furniture warehouse, of the facts connected with the loss of the British barque "Sophy Anderson", of the singular adventures of the Grice Patersons in the island of Uffa, and finally of the Camberwell poisoning case. In the latter, as may be remembered, Sherlock Holmes was able, by winding up the dead man''s watch, to prove that it had been wound up two hours before, and that therefore the deceased had gone to bed within that time--a deduction which was of the greatest importance in clearing up the case. All these I may sketch out at some future date, but none of them present such singular features as the strange train of circumstances which I have now taken up my pen to describe.

It was in the latter days of September, and the equinoctial gales had set in with exceptional violence. All day the wind had screamed and the rain had beaten against the windows, so that even here in the heart of great, hand-made London we were forced to raise our minds for the instant from the routine of life and to recognise the presence of those great elemental forces which shriek at mankind through the bars of his civilisation, like untamed beasts in a cage. As evening drew in, the storm grew higher and louder, and the wind cried and sobbed like a child in the chimney. Sherlock Holmes sat moodily at one side of the fireplace cross-indexing his records of crime, while I at the other was deep in one of Clark Russell''s fine sea-stories until the howl of the gale from without seemed to blend with the text, and the splash of the rain to lengthen out into the long swash of the sea waves. My wife was on a visit to her mother''s, and for a few days I was a dweller once more in my old quarters at Baker Street.

"Why," said I, glancing up at my companion, "that was surely the bell. Who could come to-night? Some friend of yours, perhaps?"

"Except yourself I have none," he answered. "I do not encourage visitors."

"A client, then?"

"If so, it is a serious case. Nothing less would bring a man out on such a day and at such an hour. But I take it that it is more likely to be some crony of the landlady''s."

Sherlock Holmes was wrong in his conjecture, however, for there came a step in the passage and a tapping at the door. He stretched out his long arm to turn the lamp away from himself and towards the vacant chair upon which a newcomer must sit.

"Come in!" said he.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: As getting a script to programmatically', 'Elizabeth listened in silence, but was not convinced; their behaviour at the assembly had not been calculated to please in general; and with more quickness of observation and less pliancy of temper than her sister, and with a judgement too unassailed by any attention to herself, she was very little disposed to approve them. They were in fact very fine ladies; not deficient in good humour when they were pleased, nor in the power of making themselves agreeable when they chose it, but proud and conceited. They were rather handsome, had been educated in one of the first private seminaries in town, had a fortune of twenty thousand pounds, were in the habit of spending more than they ought, and of associating with people of rank, and were therefore in every respect entitled to think well of themselves, and meanly of others. They were of a respectable family in the north of England; a circumstance more deeply impressed on their memories than that their brother’s fortune and their own had been acquired by trade.

Mr. Bingley inherited property to the amount of nearly a hundred thousand pounds from his father, who had intended to purchase an estate, but did not live to do it. Mr. Bingley intended it likewise, and sometimes made choice of his county; but as he was now provided with a good house and the liberty of a manor, it was doubtful to many of those who best knew the easiness of his temper, whether he might not spend the remainder of his days at Netherfield, and leave the next generation to purchase.

His sisters were anxious for his having an estate of his own; but, though he was now only established as a tenant, Miss Bingley was by no means unwilling to preside at his table--nor was Mrs. Hurst, who had married a man of more fashion than fortune, less disposed to consider his house as her home when it suited her. Mr. Bingley had not been of age two years, when he was tempted by an accidental recommendation to look at Netherfield House. He did look at it, and into it for half-an-hour--was pleased with the situation and the principal rooms, satisfied with what the owner said in its praise, and took it immediately.

Between him and Darcy there was a very steady friendship, in spite of great opposition of character. Bingley was endeared to Darcy by the easiness, openness, and ductility of his temper, though no disposition could offer a greater contrast to his own, and though with his own he never appeared dissatisfied. On the strength of Darcy’s regard, Bingley had the firmest reliance, and of his judgement the highest opinion. In understanding, Darcy was the superior. Bingley was by no means deficient, but Darcy was clever. He was at the same time haughty, reserved, and fastidious, and his manners, though well-bred, were not inviting. In that respect his friend had greatly the advantage. Bingley was sure of being liked wherever he appeared, Darcy was continually giving offense.

The manner in which they spoke of the Meryton assembly was sufficiently characteristic. Bingley had never met with more pleasant people or prettier girls in his life; everybody had been most kind and attentive to him; there had been no formality, no stiffness; he had soon felt acquainted with all the room; and, as to Miss Bennet, he could not conceive an angel more beautiful. Darcy, on the contrary, had seen a collection of people in whom there was little beauty and no fashion, for none of whom he had felt the smallest interest, and from none received either attention or pleasure. Miss Bennet he acknowledged to be pretty, but she smiled too much.

Mrs. Hurst and her sister allowed it to be so--but still they admired her and liked her, and pronounced her to be a sweet girl, and one whom they would not object to know more of. Miss Bennet was therefore established as a sweet girl, and their brother felt authorized by such commendation to think of her as he chose.



Within a short walk of Longbourn lived a family with whom the Bennets were particularly intimate. Sir William Lucas had been formerly in trade in Meryton, where he had made a tolerable fortune, and risen to the honour of knighthood by an address to the king during his mayoralty. The distinction had perhaps been felt too strongly. It had given him a disgust to his business, and to his residence in a small market town; and, in quitting them both, he had removed with his family to a house about a mile from Meryton, denominated from that period Lucas Lodge, where he could think with pleasure of his own importance, and, unshackled by business, occupy himself solely in being civil to all the world. For, though elated by his rank, it did not render him supercilious; on the contrary, he was all attention to everybody. By nature inoffensive, friendly, and obliging, his presentation at St. James’s had made him courteous.

Lady Lucas was a very good kind of woman, not too clever to be a valuable neighbour to Mrs. Bennet. They had several children. The eldest of them, a sensible, intelligent young woman, about twenty-seven, was Elizabeth’s intimate friend.

That the Miss Lucases and the Miss Bennets should meet to talk over a ball was absolutely necessary; and the morning after the assembly brought the former to Longbourn to hear and to communicate.

“You began the evening well, Charlotte,” said Mrs. Bennet with civil self-command to Miss Lucas. “You were Mr. Bingley’s first choice.”

“Yes; but he seemed to like his second better.”

“Oh! you mean Jane, I suppose, because he danced with her twice. To be sure that did seem as if he admired her--indeed I rather believe he did--I heard something about it--but I hardly know what--something about Mr. Robinson.”

“Perhaps you mean what I overheard between him and Mr. Robinson; did not I mention it to you? Mr. Robinson’s asking him how he liked our Meryton assemblies, and whether he did not think there were a great many pretty women in the room, and which he thought the prettiest? and his answering immediately to the last question: ‘Oh! the eldest Miss Bennet, beyond a doubt; there cannot be two opinions on that point.’”

“Upon my word! Well, that is very decided indeed--that does seem as if--but, however, it may all come to nothing, you know.”

“My overhearings were more to the purpose than yours, Eliza,” said Charlotte. “Mr. Darcy is not so well worth listening to as his friend, is he?--poor Eliza!--to be only just tolerable.”

“I beg you would not put it into Lizzy’s head to be vexed by his ill-treatment, for he is such a disagreeable man, that it would be quite a misfortune to be liked by him. Mrs. Long told me last night that he sat close to her for half-an-hour without once opening his lips.”

“Are you quite sure, ma’am?--is not there a little mistake?” said Jane. “I certainly saw Mr. Darcy speaking to her.”

“Aye--because she asked him at last how he liked Netherfield, and he could not help answering her; but she said he seemed quite angry at being spoke to.”

“Miss Bingley told me,” said Jane, “that he never speaks much, unless among his intimate acquaintances. With them he is remarkably agreeable.”');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Create your testdata', '“You are too sensible a girl, Lizzy, to fall in love merely because you are warned against it; and, therefore, I am not afraid of speaking openly. Seriously, I would have you be on your guard. Do not involve yourself or endeavour to involve him in an affection which the want of fortune would make so very imprudent. I have nothing to say against him; he is a most interesting young man; and if he had the fortune he ought to have, I should think you could not do better. But as it is, you must not let your fancy run away with you. You have sense, and we all expect you to use it. Your father would depend on your resolution and good conduct, I am sure. You must not disappoint your father.”

“My dear aunt, this is being serious indeed.”

“Yes, and I hope to engage you to be serious likewise.”

“Well, then, you need not be under any alarm. I will take care of myself, and of Mr. Wickham too. He shall not be in love with me, if I can prevent it.”

“Elizabeth, you are not serious now.”

“I beg your pardon, I will try again. At present I am not in love with Mr. Wickham; no, I certainly am not. But he is, beyond all comparison, the most agreeable man I ever saw--and if he becomes really attached to me--I believe it will be better that he should not. I see the imprudence of it. Oh! that abominable Mr. Darcy! My father’s opinion of me does me the greatest honour, and I should be miserable to forfeit it. My father, however, is partial to Mr. Wickham. In short, my dear aunt, I should be very sorry to be the means of making any of you unhappy; but since we see every day that where there is affection, young people are seldom withheld by immediate want of fortune from entering into engagements with each other, how can I promise to be wiser than so many of my fellow-creatures if I am tempted, or how am I even to know that it would be wisdom to resist? All that I can promise you, therefore, is not to be in a hurry. I will not be in a hurry to believe myself his first object. When I am in company with him, I will not be wishing. In short, I will do my best.”

“Perhaps it will be as well if you discourage his coming here so very often. At least, you should not remind your mother of inviting him.”

“As I did the other day,” said Elizabeth with a conscious smile: “very true, it will be wise in me to refrain from that. But do not imagine that he is always here so often. It is on your account that he has been so frequently invited this week. You know my mother’s ideas as to the necessity of constant company for her friends. But really, and upon my honour, I will try to do what I think to be the wisest; and now I hope you are satisfied.”

Her aunt assured her that she was, and Elizabeth having thanked her for the kindness of her hints, they parted; a wonderful instance of advice being given on such a point, without being resented.

Mr. Collins returned into Hertfordshire soon after it had been quitted by the Gardiners and Jane; but as he took up his abode with the Lucases, his arrival was no great inconvenience to Mrs. Bennet. His marriage was now fast approaching, and she was at length so far resigned as to think it inevitable, and even repeatedly to say, in an ill-natured tone, that she “wished they might be happy.” Thursday was to be the wedding day, and on Wednesday Miss Lucas paid her farewell visit; and when she rose to take leave, Elizabeth, ashamed of her mother’s ungracious and reluctant good wishes, and sincerely affected herself, accompanied her out of the room. As they went downstairs together, Charlotte said:

“I shall depend on hearing from you very often, Eliza.”

“That you certainly shall.”

“And I have another favour to ask you. Will you come and see me?”

“We shall often meet, I hope, in Hertfordshire.”

“I am not likely to leave Kent for some time. Promise me, therefore, to come to Hunsford.”

Elizabeth could not refuse, though she foresaw little pleasure in the visit.

“My father and Maria are coming to me in March,” added Charlotte, “and I hope you will consent to be of the party. Indeed, Eliza, you will be as welcome as either of them.”

The wedding took place; the bride and bridegroom set off for Kent from the church door, and everybody had as much to say, or to hear, on the subject as usual. Elizabeth soon heard from her friend; and their correspondence was as regular and frequent as it had ever been; that it should be equally unreserved was impossible. Elizabeth could never address her without feeling that all the comfort of intimacy was over, and though determined not to slacken as a correspondent, it was for the sake of what had been, rather than what was. Charlotte’s first letters were received with a good deal of eagerness; there could not but be curiosity to know how she would speak of her new home, how she would like Lady Catherine, and how happy she would dare pronounce herself to be; though, when the letters were read, Elizabeth felt that Charlotte expressed herself on every point exactly as she might have foreseen. She wrote cheerfully, seemed surrounded with comforts, and mentioned nothing which she could not praise. The house, furniture, neighbourhood, and roads, were all to her taste, and Lady Catherine’s behaviour was most friendly and obliging. It was Mr. Collins’s picture of Hunsford and Rosings rationally softened; and Elizabeth perceived that she must wait for her own visit there to know the rest.

Jane had already written a few lines to her sister to announce their safe arrival in London; and when she wrote again, Elizabeth hoped it would be in her power to say something of the Bingleys.

Her impatience for this second letter was as well rewarded as impatience generally is. Jane had been a week in town without either seeing or hearing from Caroline. She accounted for it, however, by supposing that her last letter to her friend from Longbourn had by some accident been lost.

“My aunt,” she continued, “is going to-morrow into that part of the town, and I shall take the opportunity of calling in Grosvenor Street.”

She wrote again when the visit was paid, and she had seen Miss Bingley. “I did not think Caroline in spirits,” were her words, “but she was very glad to see me, and reproached me for giving her no notice of my coming to London. I was right, therefore, my last letter had never reached her. I inquired after their brother, of course. He was well, but so much engaged with Mr. Darcy that they scarcely ever saw him. I found that Miss Darcy was expected to dinner. I wish I could see her. My visit was not long, as Caroline and Mrs. Hurst were going out. I dare say I shall see them soon here.”

Elizabeth shook her head over this letter. It convinced her that accident only could discover to Mr. Bingley her sister’s being in town.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: I would\'ve loved to make it work', 'He was not, however, blind to what he thought the defects of his own College; and I have, from the information of Dr. Taylor, a very strong instance of that rigid honesty which he ever inflexibly preserved. Taylor had obtained his father''s consent to be entered of Pembroke, that he might be with his schoolfellow Johnson, with whom, though some years older than himself, he was very intimate. This would have been a great comfort to Johnson. But he fairly told Taylor that he could not, in conscience, suffer him to enter where he knew he could not have an able tutor. He then made inquiry all round the University, and having found that Mr. Bateman, of Christ Church, was the tutor of highest reputation, Taylor was entered of that College. Mr. Bateman''s lectures were so excellent, that Johnson used to come and get them at second-hand from Taylor, till his poverty being so extreme that his shoes were worn out, and his feet appeared through them, he saw that this humiliating circumstance was perceived by the Christ Church men, and he came no more. He was too proud to accept of money, and somebody having set a pair of new shoes at his door, he threw them away with indignation. How must we feel when we read such an anecdote of Samuel Johnson!

The res angusta domi prevented him from having the advantage of a complete academical education. The friend to whom he had trusted for support had deceived him. His debts in College, though not great, were increasing; and his scanty remittances from Lichfield, which had all along been made with great difficulty, could be supplied no longer, his father having fallen into a state of insolvency. Compelled, therefore, by irresistible necessity, he left the College in autumn, 1731, without a degree, having been a member of it little more than three years.

And now (I had almost said POOR) Samuel Johnson returned to his native city, destitute, and not knowing how he should gain even a decent livelihood. His father''s misfortunes in trade rendered him unable to support his son; and for some time there appeared no means by which he could maintain himself. In the December of this year his father died.

Johnson was so far fortunate, that the respectable character of his parents, and his own merit, had, from his earliest years, secured him a kind reception in the best families at Lichfield. Among these I can mention Mr. Howard, Dr. Swinfen, Mr. Simpson, Mr. Levett, Captain Garrick, father of the great ornament of the British stage; but above all, Mr. Gilbert Walmsley, Register of the Prerogative Court of Lichfield, whose character, long after his decease, Dr. Johnson has, in his Life of Edmund Smith, thus drawn in the glowing colours of gratitude:

''Of Gilbert Walmsley, thus presented to my mind, let me indulge myself in the remembrance. I knew him very early; he was one of the first friends that literature procured me, and I hope that, at least, my gratitude made me worthy of his notice.

''He was of an advanced age, and I was only not a boy, yet he never received my notions with contempt. He was a whig, with all the virulence and malevolence of his party; yet difference of opinion did not keep us apart. I honoured him and he endured me.

''At this man''s table I enjoyed many cheerful and instructive hours, with companions, such as are not often found--with one who has lengthened, and one who has gladdened life; with Dr. James, whose skill in physick will be long remembered; and with David Garrick, whom I hoped to have gratified with this character of our common friend. But what are the hopes of man! I am disappointed by that stroke of death, which has eclipsed the gaiety of nations, and impoverished the publick stock of harmless pleasure.''

In these families he passed much time in his early years. In most of them, he was in the company of ladies, particularly at Mr. Walmsley''s, whose wife and sisters-in-law, of the name of Aston, and daughters of a Baronet, were remarkable for good breeding; so that the notion which has been industriously circulated and believed, that he never was in good company till late in life, and, consequently had been confirmed in coarse and ferocious manners by long habits, is wholly without foundation. Some of the ladies have assured me, they recollected him well when a young man, as distinguished for his complaisance.

In the forlorn state of his circumstances, he accepted of an offer to be employed as usher in the school of Market-Bosworth, in Leicestershire, to which it appears, from one of his little fragments of a diary, that he went on foot, on the 16th of July.

This employment was very irksome to him in every respect, and he complained grievously of it in his letters to his friend Mr. Hector, who was now settled as a surgeon at Birmingham. The letters are lost; but Mr. Hector recollects his writing ''that the poet had described the dull sameness of his existence in these words, "Vitam continet una dies" (one day contains the whole of my life); that it was unvaried as the note of the cuckow; and that he did not know whether it was more disagreeable for him to teach, or the boys to learn, the grammar rules.'' His general aversion to this painful drudgery was greatly enhanced by a disagreement between him and Sir Wolstan Dixey, the patron of the school, in whose house, I have been told, he officiated as a kind of domestick chaplain, so far, at least, as to say grace at table, but was treated with what he represented as intolerable harshness; and, after suffering for a few months such complicated misery, he relinquished a situation which all his life afterwards he recollected with the strongest aversion, and even a degree of horrour. But it is probable that at this period, whatever uneasiness he may have endured, he laid the foundation of much future eminence by application to his studies.

Being now again totally unoccupied, he was invited by Mr. Hector to pass some time with him at Birmingham, as his guest, at the house of Mr. Warren, with whom Mr. Hector lodged and boarded. Mr. Warren was the first established bookseller in Birmingham, and was very attentive to Johnson, who he soon found could be of much service to him in his trade, by his knowledge of literature; and he even obtained the assistance of his pen in furnishing some numbers of a periodical Essay printed in the newspaper, of which Warren was proprietor. After very diligent inquiry, I have not been able to recover those early specimens of that particular mode of writing by which Johnson afterwards so greatly distinguished himself.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: But it appear that is not going to happen', 'He went to his room and dressed.  There was a wild recklessness of gaiety in his manner as he sat at table, but now and then a thrill of terror ran through him when he remembered that, pressed against the window of the conservatory, like a white handkerchief, he had seen the face of James Vane watching him. The next day he did not leave the house, and, indeed, spent most of the time in his own room, sick with a wild terror of dying, and yet indifferent to life itself.  The consciousness of being hunted, snared, tracked down, had begun to dominate him.  If the tapestry did but tremble in the wind, he shook.  The dead leaves that were blown against the leaded panes seemed to him like his own wasted resolutions and wild regrets.  When he closed his eyes, he saw again the sailor''s face peering through the mist-stained glass, and horror seemed once more to lay its hand upon his heart.

But perhaps it had been only his fancy that had called vengeance out of the night and set the hideous shapes of punishment before him.  Actual life was chaos, but there was something terribly logical in the imagination.  It was the imagination that set remorse to dog the feet of sin.  It was the imagination that made each crime bear its misshapen brood.  In the common world of fact the wicked were not punished, nor the good rewarded.  Success was given to the strong, failure thrust upon the weak.  That was all.  Besides, had any stranger been prowling round the house, he would have been seen by the servants or the keepers.  Had any foot-marks been found on the flower-beds, the gardeners would have reported it.  Yes, it had been merely fancy. Sibyl Vane''s brother had not come back to kill him.  He had sailed away in his ship to founder in some winter sea.  From him, at any rate, he was safe.  Why, the man did not know who he was, could not know who he was.  The mask of youth had saved him.

And yet if it had been merely an illusion, how terrible it was to think that conscience could raise such fearful phantoms, and give them visible form, and make them move before one!  What sort of life would his be if, day and night, shadows of his crime were to peer at him from silent corners, to mock him from secret places, to whisper in his ear as he sat at the feast, to wake him with icy fingers as he lay asleep! As the thought crept through his brain, he grew pale with terror, and the air seemed to him to have become suddenly colder.  Oh! in what a wild hour of madness he had killed his friend!  How ghastly the mere memory of the scene!  He saw it all again.  Each hideous detail came back to him with added horror.  Out of the black cave of time, terrible and swathed in scarlet, rose the image of his sin.  When Lord Henry came in at six o''clock, he found him crying as one whose heart will break.

It was not till the third day that he ventured to go out.  There was something in the clear, pine-scented air of that winter morning that seemed to bring him back his joyousness and his ardour for life.  But it was not merely the physical conditions of environment that had caused the change.  His own nature had revolted against the excess of anguish that had sought to maim and mar the perfection of its calm. With subtle and finely wrought temperaments it is always so.  Their strong passions must either bruise or bend.  They either slay the man, or themselves die.  Shallow sorrows and shallow loves live on.  The loves and sorrows that are great are destroyed by their own plenitude. Besides, he had convinced himself that he had been the victim of a terror-stricken imagination, and looked back now on his fears with something of pity and not a little of contempt.

After breakfast, he walked with the duchess for an hour in the garden and then drove across the park to join the shooting-party. The crisp frost lay like salt upon the grass.  The sky was an inverted cup of blue metal.  A thin film of ice bordered the flat, reed-grown lake.

At the corner of the pine-wood he caught sight of Sir Geoffrey Clouston, the duchess''s brother, jerking two spent cartridges out of his gun.  He jumped from the cart, and having told the groom to take the mare home, made his way towards his guest through the withered bracken and rough undergrowth.

"Have you had good sport, Geoffrey?" he asked.

"Not very good, Dorian.  I think most of the birds have gone to the open.  I dare say it will be better after lunch, when we get to new ground."

Dorian strolled along by his side.  The keen aromatic air, the brown and red lights that glimmered in the wood, the hoarse cries of the beaters ringing out from time to time, and the sharp snaps of the guns that followed, fascinated him and filled him with a sense of delightful freedom.  He was dominated by the carelessness of happiness, by the high indifference of joy.

Suddenly from a lumpy tussock of old grass some twenty yards in front of them, with black-tipped ears erect and long hinder limbs throwing it forward, started a hare.  It bolted for a thicket of alders.  Sir Geoffrey put his gun to his shoulder, but there was something in the animal''s grace of movement that strangely charmed Dorian Gray, and he cried out at once, "Don''t shoot it, Geoffrey.  Let it live."

"What nonsense, Dorian!" laughed his companion, and as the hare bounded into the thicket, he fired.  There were two cries heard, the cry of a hare in pain, which is dreadful, the cry of a man in agony, which is worse.

"Good heavens!  I have hit a beater!" exclaimed Sir Geoffrey.  "What an ass the man was to get in front of the guns!  Stop shooting there!" he called out at the top of his voice.  "A man is hurt."

The head-keeper came running up with a stick in his hand.

"Where, sir?  Where is he?" he shouted.  At the same time, the firing ceased along the line.

"Here," answered Sir Geoffrey angrily, hurrying towards the thicket. "Why on earth don''t you keep your men back?  Spoiled my shooting for the day."

Dorian watched them as they plunged into the alder-clump, brushing the lithe swinging branches aside.  In a few moments they emerged, dragging a body after them into the sunlight.  He turned away in horror.  It seemed to him that misfortune followed wherever he went.  He heard Sir Geoffrey ask if the man was really dead, and the affirmative answer of the keeper.  The wood seemed to him to have become suddenly alive with faces.  There was the trampling of myriad feet and the low buzz of voices.  A great copper-breasted pheasant came beating through the boughs overhead.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Oh well, I\'ll just have to add this', '"Days in summer, Basil, are apt to linger," murmured Lord Henry. "Perhaps you will tire sooner than he will.  It is a sad thing to think of, but there is no doubt that genius lasts longer than beauty.  That accounts for the fact that we all take such pains to over-educate ourselves.  In the wild struggle for existence, we want to have something that endures, and so we fill our minds with rubbish and facts, in the silly hope of keeping our place.  The thoroughly well-informed man--that is the modern ideal.  And the mind of the thoroughly well-informed man is a dreadful thing.  It is like a bric-a-brac shop, all monsters and dust, with everything priced above its proper value.  I think you will tire first, all the same.  Some day you will look at your friend, and he will seem to you to be a little out of drawing, or you won''t like his tone of colour, or something. You will bitterly reproach him in your own heart, and seriously think that he has behaved very badly to you.  The next time he calls, you will be perfectly cold and indifferent.  It will be a great pity, for it will alter you.  What you have told me is quite a romance, a romance of art one might call it, and the worst of having a romance of any kind is that it leaves one so unromantic."

"Harry, don''t talk like that.  As long as I live, the personality of Dorian Gray will dominate me.  You can''t feel what I feel.  You change too often."

"Ah, my dear Basil, that is exactly why I can feel it.  Those who are faithful know only the trivial side of love: it is the faithless who know love''s tragedies."  And Lord Henry struck a light on a dainty silver case and began to smoke a cigarette with a self-conscious and satisfied air, as if he had summed up the world in a phrase.  There was a rustle of chirruping sparrows in the green lacquer leaves of the ivy, and the blue cloud-shadows chased themselves across the grass like swallows.  How pleasant it was in the garden!  And how delightful other people''s emotions were!--much more delightful than their ideas, it seemed to him.  One''s own soul, and the passions of one''s friends--those were the fascinating things in life.  He pictured to himself with silent amusement the tedious luncheon that he had missed by staying so long with Basil Hallward.  Had he gone to his aunt''s, he would have been sure to have met Lord Goodbody there, and the whole conversation would have been about the feeding of the poor and the necessity for model lodging-houses. Each class would have preached the importance of those virtues, for whose exercise there was no necessity in their own lives.  The rich would have spoken on the value of thrift, and the idle grown eloquent over the dignity of labour.  It was charming to have escaped all that!  As he thought of his aunt, an idea seemed to strike him.  He turned to Hallward and said, "My dear fellow, I have just remembered."

"Remembered what, Harry?"

"Where I heard the name of Dorian Gray."

"Where was it?" asked Hallward, with a slight frown.

"Don''t look so angry, Basil.  It was at my aunt, Lady Agatha''s.  She told me she had discovered a wonderful young man who was going to help her in the East End, and that his name was Dorian Gray.  I am bound to state that she never told me he was good-looking. Women have no appreciation of good looks; at least, good women have not.  She said that he was very earnest and had a beautiful nature.  I at once pictured to myself a creature with spectacles and lank hair, horribly freckled, and tramping about on huge feet.  I wish I had known it was your friend."

"I am very glad you didn''t, Harry."

"Why?"

"I don''t want you to meet him."

"You don''t want me to meet him?"

"No."

"Mr. Dorian Gray is in the studio, sir," said the butler, coming into the garden.

"You must introduce me now," cried Lord Henry, laughing.

The painter turned to his servant, who stood blinking in the sunlight. "Ask Mr. Gray to wait, Parker:  I shall be in in a few moments." The man bowed and went up the walk.

Then he looked at Lord Henry.  "Dorian Gray is my dearest friend," he said.  "He has a simple and a beautiful nature.  Your aunt was quite right in what she said of him.  Don''t spoil him.  Don''t try to influence him.  Your influence would be bad.  The world is wide, and has many marvellous people in it.  Don''t take away from me the one person who gives to my art whatever charm it possesses:  my life as an artist depends on him.  Mind, Harry, I trust you."  He spoke very slowly, and the words seemed wrung out of him almost against his will.

"What nonsense you talk!" said Lord Henry, smiling, and taking Hallward by the arm, he almost led him into the house. As they entered they saw Dorian Gray.  He was seated at the piano, with his back to them, turning over the pages of a volume of Schumann''s "Forest Scenes."  "You must lend me these, Basil," he cried.  "I want to learn them.  They are perfectly charming."

"That entirely depends on how you sit to-day, Dorian."

"Oh, I am tired of sitting, and I don''t want a life-sized portrait of myself," answered the lad, swinging round on the music-stool in a wilful, petulant manner.  When he caught sight of Lord Henry, a faint blush coloured his cheeks for a moment, and he started up.  "I beg your pardon, Basil, but I didn''t know you had any one with you."

"This is Lord Henry Wotton, Dorian, an old Oxford friend of mine.  I have just been telling him what a capital sitter you were, and now you have spoiled everything."

"You have not spoiled my pleasure in meeting you, Mr. Gray," said Lord Henry, stepping forward and extending his hand.  "My aunt has often spoken to me about you.  You are one of her favourites, and, I am afraid, one of her victims also."

"I am in Lady Agatha''s black books at present," answered Dorian with a funny look of penitence.  "I promised to go to a club in Whitechapel with her last Tuesday, and I really forgot all about it.  We were to have played a duet together--three duets, I believe.  I don''t know what she will say to me.  I am far too frightened to call."

"Oh, I will make your peace with my aunt.  She is quite devoted to you. And I don''t think it really matters about your not being there.  The audience probably thought it was a duet.  When Aunt Agatha sits down to the piano, she makes quite enough noise for two people."

"That is very horrid to her, and not very nice to me," answered Dorian, laughing.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Manually to the SQL file', '“I wouldn’t speak of this note, you know,” said the master.

“No, sir,” said the clerk. “I understand.”

But no sooner was Mr. Utterson alone that night, than he locked the note into his safe, where it reposed from that time forward. “What!” he thought. “Henry Jekyll forge for a murderer!” And his blood ran cold in his veins.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: This is a fucking travesty', 'Mr. Thomas Sheridan and Mr. Murphy, who then lived a good deal both with him and Mr. Wedderburne, told me, that they previously talked with Johnson upon this matter, and that it was perfectly understood by all parties that the pension was merely honorary. Sir Joshua Reynolds told me, that Johnson called on him after his majesty''s intention had been notified to him, and said he wished to consult his friends as to the propriety of his accepting this mark of the royal favour, after the definitions which he had given in his Dictionary of pension and pensioners. He said he would not have Sir Joshua''s answer till next day, when he would call again, and desired he might think of it. Sir Joshua answered that he was clear to give his opinion then, that there could be no objection to his receiving from the King a reward for literary merit; and that certainly the definitions in his Dictionary were not applicable to him. Johnson, it should seem, was satisfied, for he did not call again till he had accepted the pension, and had waited on Lord Bute to thank him. He then told Sir Joshua that Lord Bute said to him expressly, ''It is not given you for anything you are to do, but for what you have done.'' His Lordship, he said, behaved in the handsomest manner, he repeated the words twice, that he might be sure Johnson heard them, and thus set his mind perfectly at ease. This nobleman, who has been so virulently abused, acted with great honour in this instance and displayed a mind truly liberal. A minister of a more narrow and selfish disposition would have availed himself of such an opportunity to fix an implied obligation on a man of Johnson''s powerful talents to give him his support.

Mr. Murphy and the late Mr. Sheridan severally contended for the distinction of having been the first who mentioned to Mr. Wedderburne that Johnson ought to have a pension. When I spoke of this to Lord Loughborough, wishing to know if he recollected the prime mover in the business, he said, ''All his friends assisted:'' and when I told him that Mr. Sheridan strenuously asserted his claim to it, his Lordship said, ''He rang the bell.'' And it is but just to add, that Mr. Sheridan told me, that when he communicated to Dr. Johnson that a pension was to be granted him, he replied in a fervour of gratitude, ''The English language does not afford me terms adequate to my feelings on this occasion. I must have recourse to the French. I am penetre with his Majesty''s goodness.'' When I repeated this to Dr. Johnson, he did not contradict it.

This year his friend Sir Joshua Reynolds paid a visit of some weeks to his native country, Devonshire, in which he was accompanied by Johnson, who was much pleased with this jaunt, and declared he had derived from it a great accession of new ideas. He was entertained at the seats of several noblemen and gentlemen in the West of England; but the greatest part of the time was passed at Plymouth, where the magnificence of the navy, the ship-building and all its circumstances, afforded him a grand subject of contemplation. The Commissioner of the Dock-yard paid him the compliment of ordering the yacht to convey him and his friend to the Eddystone, to which they accordingly sailed. But the weather was so tempestuous that they could not land.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: But since there\'s not much time left', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: We\'ll have to just make it work', '"Ah! then it must be an illusion.  The things one feels absolutely certain about are never true.  That is the fatality of faith, and the lesson of romance.  How grave you are!  Don''t be so serious.  What have you or I to do with the superstitions of our age?  No:  we have given up our belief in the soul.  Play me something.  Play me a nocturne, Dorian, and, as you play, tell me, in a low voice, how you have kept your youth.  You must have some secret.  I am only ten years older than you are, and I am wrinkled, and worn, and yellow.  You are really wonderful, Dorian.  You have never looked more charming than you do to-night. You remind me of the day I saw you first.  You were rather cheeky, very shy, and absolutely extraordinary.  You have changed, of course, but not in appearance.  I wish you would tell me your secret. To get back my youth I would do anything in the world, except take exercise, get up early, or be respectable.  Youth!  There is nothing like it.  It''s absurd to talk of the ignorance of youth.  The only people to whose opinions I listen now with any respect are people much younger than myself.  They seem in front of me.  Life has revealed to them her latest wonder.  As for the aged, I always contradict the aged. I do it on principle.  If you ask them their opinion on something that happened yesterday, they solemnly give you the opinions current in 1820, when people wore high stocks, believed in everything, and knew absolutely nothing.  How lovely that thing you are playing is!  I wonder, did Chopin write it at Majorca, with the sea weeping round the villa and the salt spray dashing against the panes?  It is marvellously romantic.  What a blessing it is that there is one art left to us that is not imitative!  Don''t stop.  I want music to-night. It seems to me that you are the young Apollo and that I am Marsyas listening to you. I have sorrows, Dorian, of my own, that even you know nothing of.  The tragedy of old age is not that one is old, but that one is young.  I am amazed sometimes at my own sincerity.  Ah, Dorian, how happy you are! What an exquisite life you have had!  You have drunk deeply of everything.  You have crushed the grapes against your palate.  Nothing has been hidden from you.  And it has all been to you no more than the sound of music.  It has not marred you.  You are still the same."

"I am not the same, Harry."

"Yes, you are the same.  I wonder what the rest of your life will be. Don''t spoil it by renunciations.  At present you are a perfect type. Don''t make yourself incomplete.  You are quite flawless now.  You need not shake your head:  you know you are.  Besides, Dorian, don''t deceive yourself.  Life is not governed by will or intention.  Life is a question of nerves, and fibres, and slowly built-up cells in which thought hides itself and passion has its dreams.  You may fancy yourself safe and think yourself strong.  But a chance tone of colour in a room or a morning sky, a particular perfume that you had once loved and that brings subtle memories with it, a line from a forgotten poem that you had come across again, a cadence from a piece of music that you had ceased to play--I tell you, Dorian, that it is on things like these that our lives depend.  Browning writes about that somewhere; but our own senses will imagine them for us.  There are moments when the odour of lilas blanc passes suddenly across me, and I have to live the strangest month of my life over again.  I wish I could change places with you, Dorian.  The world has cried out against us both, but it has always worshipped you.  It always will worship you. You are the type of what the age is searching for, and what it is afraid it has found.  I am so glad that you have never done anything, never carved a statue, or painted a picture, or produced anything outside of yourself!  Life has been your art.  You have set yourself to music.  Your days are your sonnets."');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: And that involves me writing all these manually', 'Dorian shrugged his shoulders.  "I believe he married Lady Radley''s maid, and has established her in Paris as an English dressmaker. Anglomania is very fashionable over there now, I hear.  It seems silly of the French, doesn''t it?  But--do you know?--he was not at all a bad servant.  I never liked him, but I had nothing to complain about.  One often imagines things that are quite absurd.  He was really very devoted to me and seemed quite sorry when he went away.  Have another brandy-and-soda? Or would you like hock-and-seltzer? I always take hock-and-seltzer myself.  There is sure to be some in the next room."

"Thanks, I won''t have anything more," said the painter, taking his cap and coat off and throwing them on the bag that he had placed in the corner.  "And now, my dear fellow, I want to speak to you seriously. Don''t frown like that.  You make it so much more difficult for me."

"What is it all about?" cried Dorian in his petulant way, flinging himself down on the sofa.  "I hope it is not about myself.  I am tired of myself to-night. I should like to be somebody else."');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: I\'ve now lost count of how many of these I\'m making', '“I begin to be sorry that he comes at all,” said Jane to her sister. “It would be nothing; I could see him with perfect indifference, but I can hardly bear to hear it thus perpetually talked of. My mother means well; but she does not know, no one can know, how much I suffer from what she says. Happy shall I be, when his stay at Netherfield is over!”

“I wish I could say anything to comfort you,” replied Elizabeth; “but it is wholly out of my power. You must feel it; and the usual satisfaction of preaching patience to a sufferer is denied me, because you have always so much.”

Mr. Bingley arrived. Mrs. Bennet, through the assistance of servants, contrived to have the earliest tidings of it, that the period of anxiety and fretfulness on her side might be as long as it could. She counted the days that must intervene before their invitation could be sent; hopeless of seeing him before. But on the third morning after his arrival in Hertfordshire, she saw him, from her dressing-room window, enter the paddock and ride towards the house.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: I guess it works out in the end, maybe', 'The circle of his friends, indeed, at this time was extensive and various, far beyond what has been generally imagined. To trace his acquaintance with each particular person, if it could be done, would be a task, of which the labour would not be repaid by the advantage. But exceptions are to be made; one of which must be a friend so eminent as Sir Joshua Reynolds, who was truly his dulce decus, and with whom he maintained an uninterrupted intimacy to the last hour of his life. When Johnson lived in Castle-street, Cavendish-square, he used frequently to visit two ladies, who lived opposite to him, Miss Cotterells, daughters of Admiral Cotterell. Reynolds used also to visit there, and thus they met. Mr. Reynolds, as I have observed above, had, from the first reading of his Life of Savage, conceived a very high admiration of Johnson''s powers of writing. His conversation no less delighted him; and he cultivated his acquaintance with the laudable zeal of one who was ambitious of general improvement. Sir Joshua, indeed, was lucky enough at their very first meeting to make a remark, which was so much above the common-place style of conversation, that Johnson at once perceived that Reynolds had the habit of thinking for himself. The ladies were regretting the death of a friend, to whom they owed great obligations; upon which Reynolds observed, ''You have, however, the comfort of being relieved from a burthen of gratitude.'' They were shocked a little at this alleviating suggestion, as too selfish; but Johnson defended it in his clear and forcible manner, and was much pleased with the MIND, the fair view of human nature, which it exhibited, like some of the reflections of Rochefaucault. The consequence was, that he went home with Reynolds, and supped with him.

Sir Joshua told me a pleasant characteristical anecdote of Johnson about the time of their first acquaintance. When they were one evening together at the Miss Cotterells'', the then Duchess of Argyle and another lady of high rank came in. Johnson thinking that the Miss Cotterells were too much engrossed by them, and that he and his friend were neglected, as low company of whom they were somewhat ashamed, grew angry; and resolving to shock their supposed pride, by making their great visitors imagine that his friend and he were low indeed, he addressed himself in a loud tone to Mr. Reynolds, saying, ''How much do you think you and I could get in a week, if we were to WORK AS HARD as we could?''--as if they had been common mechanicks.

His acquaintance with Bennet Langton, Esq. of Langton, in Lincolnshire, another much valued friend, commenced soon after the conclusion of his Rambler; which that gentleman, then a youth, had read with so much admiration, that he came to London chiefly with the view of endeavouring to be introduced to its authour. By a fortunate chance he happened to take lodgings in a house where Mr. Levet frequently visited; and having mentioned his wish to his landlady, she introduced him to Mr. Levet, who readily obtained Johnson''s permission to bring Mr. Langton to him; as, indeed, Johnson, during the whole course of his life, had no shyness, real or affected, but was easy of access to all who were properly recommended, and even wished to see numbers at his levee, as his morning circle of company might, with strict propriety, be called. Mr. Langton was exceedingly surprised when the sage first appeared. He had not received the smallest intimation of his figure, dress, or manner. From perusing his writings, he fancied he should see a decent, well-drest, in short, remarkably decorous philosopher. Instead of which, down from his bed-chamber, about noon, came, as newly risen, a huge uncouth figure, with a little dark wig which scarcely covered his head, and his clothes hanging loose about him. But his conversation was so rich, so animated, and so forcible, and his religious and political notions so congenial with those in which Langton had been educated, that he conceived for him that veneration and attachment which he ever preserved. Johnson was not the less ready to love Mr. Langton, for his being of a very ancient family; for I have heard him say, with pleasure, ''Langton, Sir, has a grant of free warren from Henry the Second; and Cardinal Stephen Langton, in King John''s reign, was of this family.''');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: There\'s just so many', 'Midnight.--I have had a long talk with the Count. I asked him a few questions on Transylvania history, and he warmed up to the subject wonderfully. In his speaking of things and people, and especially of battles, he spoke as if he had been present at them all. This he afterwards explained by saying that to a boyar the pride of his house and name is his own pride, that their glory is his glory, that their fate is his fate. Whenever he spoke of his house he always said "we," and spoke almost in the plural, like a king speaking. I wish I could put down all he said exactly as he said it, for to me it was most fascinating. It seemed to have in it a whole history of the country. He grew excited as he spoke, and walked about the room pulling his great white moustache and grasping anything on which he laid his hands as though he would crush it by main strength. One thing he said which I shall put down as nearly as I can; for it tells in its way the story of his race:--

"We Szekelys have a right to be proud, for in our veins flows the blood of many brave races who fought as the lion fights, for lordship. Here, in the whirlpool of European races, the Ugric tribe bore down from Iceland the fighting spirit which Thor and Wodin gave them, which their Berserkers displayed to such fell intent on the seaboards of Europe, ay, and of Asia and Africa too, till the peoples thought that the were-wolves themselves had come. Here, too, when they came, they found the Huns, whose warlike fury had swept the earth like a living flame, till the dying peoples held that in their veins ran the blood of those old witches, who, expelled from Scythia had mated with the devils in the desert. Fools, fools! What devil or what witch was ever so great as Attila, whose blood is in these veins?" He held up his arms. "Is it a wonder that we were a conquering race; that we were proud; that when the Magyar, the Lombard, the Avar, the Bulgar, or the Turk poured his thousands on our frontiers, we drove them back? Is it strange that when Arpad and his legions swept through the Hungarian fatherland he found us here when he reached the frontier; that the Honfoglalas was completed there? And when the Hungarian flood swept eastward, the Szekelys were claimed as kindred by the victorious Magyars, and to us for centuries was trusted the guarding of the frontier of Turkey-land; ay, and more than that, endless duty of the frontier guard, for, as the Turks say, ''water sleeps, and enemy is sleepless.'' Who more gladly than we throughout the Four Nations received the ''bloody sword,'' or at its warlike call flocked quicker to the standard of the King? When was redeemed that great shame of my nation, the shame of Cassova, when the flags of the Wallach and the Magyar went down beneath the Crescent? Who was it but one of my own race who as Voivode crossed the Danube and beat the Turk on his own ground? This was a Dracula indeed! Woe was it that his own unworthy brother, when he had fallen, sold his people to the Turk and brought the shame of slavery on them! Was it not this Dracula, indeed, who inspired that other of his race who in a later age again and again brought his forces over the great river into Turkey-land; who, when he was beaten back, came again, and again, and again, though he had to come alone from the bloody field where his troops were being slaughtered, since he knew that he alone could ultimately triumph! They said that he thought only of himself. Bah! what good are peasants without a leader? Where ends the war without a brain and heart to conduct it? Again, when, after the battle of Mohács, we threw off the Hungarian yoke, we of the Dracula blood were amongst their leaders, for our spirit would not brook that we were not free. Ah, young sir, the Szekelys--and the Dracula as their heart''s blood, their brains, and their swords--can boast a record that mushroom growths like the Hapsburgs and the Romanoffs can never reach. The warlike days are over. Blood is too precious a thing in these days of dishonourable peace; and the glories of the great races are as a tale that is told."

It was by this time close on morning, and we went to bed. (Mem., this diary seems horribly like the beginning of the "Arabian Nights," for everything has to break off at cockcrow--or like the ghost of Hamlet''s father.)');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: And I should make them unique', '"Come!" she said, "come away from this awful place! Let us go to meet my husband who is, I know, coming towards us." She was looking thin and pale and weak; but her eyes were pure and glowed with fervour. I was glad to see her paleness and her illness, for my mind was full of the fresh horror of that ruddy vampire sleep.

And so with trust and hope, and yet full of fear, we go eastward to meet our friends--and him--whom Madam Mina tell me that she know are coming to meet us. 6 November.--It was late in the afternoon when the Professor and I took our way towards the east whence I knew Jonathan was coming. We did not go fast, though the way was steeply downhill, for we had to take heavy rugs and wraps with us; we dared not face the possibility of being left without warmth in the cold and the snow. We had to take some of our provisions, too, for we were in a perfect desolation, and, so far as we could see through the snowfall, there was not even the sign of habitation. When we had gone about a mile, I was tired with the heavy walking and sat down to rest. Then we looked back and saw where the clear line of Dracula''s castle cut the sky; for we were so deep under the hill whereon it was set that the angle of perspective of the Carpathian mountains was far below it. We saw it in all its grandeur, perched a thousand feet on the summit of a sheer precipice, and with seemingly a great gap between it and the steep of the adjacent mountain on any side. There was something wild and uncanny about the place. We could hear the distant howling of wolves. They were far off, but the sound, even though coming muffled through the deadening snowfall, was full of terror. I knew from the way Dr. Van Helsing was searching about that he was trying to seek some strategic point, where we would be less exposed in case of attack. The rough roadway still led downwards; we could trace it through the drifted snow.

In a little while the Professor signalled to me, so I got up and joined him. He had found a wonderful spot, a sort of natural hollow in a rock, with an entrance like a doorway between two boulders. He took me by the hand and drew me in: "See!" he said, "here you will be in shelter; and if the wolves do come I can meet them one by one." He brought in our furs, and made a snug nest for me, and got out some provisions and forced them upon me. But I could not eat; to even try to do so was repulsive to me, and, much as I would have liked to please him, I could not bring myself to the attempt. He looked very sad, but did not reproach me. Taking his field-glasses from the case, he stood on the top of the rock, and began to search the horizon. Suddenly he called out:--');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Making them unique is hard work', 'The folly and unreasonableness of this speech would admit of no answer. But what a scene was this for Sir Clement! his surprise was evident; and I must acknowledge my confusion was equally great.

We had now to wait for young Branghton, who did not appear for some time; and during this interval it was with difficulty that I avoided Sir Clement, who was on the rack of curiosity, and dying to speak to me.

When, at last, the hopeful youth returned, a long and frightful quarrel ensued between him and his father, in which his sisters occasionally joined, concerning his neglect; and he defended himself only by a brutal mirth, which he indulged at their expense.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: And it also precludes me', '"And now I have a very strange experience to tell you. I had, as you know, cut off my hair in London, and I had placed it in a great coil at the bottom of my trunk. One evening, after the child was in bed, I began to amuse myself by examining the furniture of my room and by rearranging my own little things. There was an old chest of drawers in the room, the two upper ones empty and open, the lower one locked. I had filled the first two with my linen, and as I had still much to pack away I was naturally annoyed at not having the use of the third drawer. It struck me that it might have been fastened by a mere oversight, so I took out my bunch of keys and tried to open it. The very first key fitted to perfection, and I drew the drawer open. There was only one thing in it, but I am sure that you would never guess what it was. It was my coil of hair.

"I took it up and examined it. It was of the same peculiar tint, and the same thickness. But then the impossibility of the thing obtruded itself upon me. How could my hair have been locked in the drawer? With trembling hands I undid my trunk, turned out the contents, and drew from the bottom my own hair. I laid the two tresses together, and I assure you that they were identical. Was it not extraordinary? Puzzle as I would, I could make nothing at all of what it meant. I returned the strange hair to the drawer, and I said nothing of the matter to the Rucastles as I felt that I had put myself in the wrong by opening a drawer which they had locked.

"I am naturally observant, as you may have remarked, Mr. Holmes, and I soon had a pretty good plan of the whole house in my head. There was one wing, however, which appeared not to be inhabited at all. A door which faced that which led into the quarters of the Tollers opened into this suite, but it was invariably locked. One day, however, as I ascended the stair, I met Mr. Rucastle coming out through this door, his keys in his hand, and a look on his face which made him a very different person to the round, jovial man to whom I was accustomed. His cheeks were red, his brow was all crinkled with anger, and the veins stood out at his temples with passion. He locked the door and hurried past me without a word or a look.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: From making sure that the', '“You are considering how insupportable it would be to pass many evenings in this manner--in such society; and indeed I am quite of your opinion. I was never more annoyed! The insipidity, and yet the noise--the nothingness, and yet the self-importance of all those people! What would I give to hear your strictures on them!”

“Your conjecture is totally wrong, I assure you. My mind was more agreeably engaged. I have been meditating on the very great pleasure which a pair of fine eyes in the face of a pretty woman can bestow.”

Miss Bingley immediately fixed her eyes on his face, and desired he would tell her what lady had the credit of inspiring such reflections. Mr. Darcy replied with great intrepidity:');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Dates are better created', '"Ah! then it must be an illusion.  The things one feels absolutely certain about are never true.  That is the fatality of faith, and the lesson of romance.  How grave you are!  Don''t be so serious.  What have you or I to do with the superstitions of our age?  No:  we have given up our belief in the soul.  Play me something.  Play me a nocturne, Dorian, and, as you play, tell me, in a low voice, how you have kept your youth.  You must have some secret.  I am only ten years older than you are, and I am wrinkled, and worn, and yellow.  You are really wonderful, Dorian.  You have never looked more charming than you do to-night. You remind me of the day I saw you first.  You were rather cheeky, very shy, and absolutely extraordinary.  You have changed, of course, but not in appearance.  I wish you would tell me your secret. To get back my youth I would do anything in the world, except take exercise, get up early, or be respectable.  Youth!  There is nothing like it.  It''s absurd to talk of the ignorance of youth.  The only people to whose opinions I listen now with any respect are people much younger than myself.  They seem in front of me.  Life has revealed to them her latest wonder.  As for the aged, I always contradict the aged. I do it on principle.  If you ask them their opinion on something that happened yesterday, they solemnly give you the opinions current in 1820, when people wore high stocks, believed in everything, and knew absolutely nothing.  How lovely that thing you are playing is!  I wonder, did Chopin write it at Majorca, with the sea weeping round the villa and the salt spray dashing against the panes?  It is marvellously romantic.  What a blessing it is that there is one art left to us that is not imitative!  Don''t stop.  I want music to-night. It seems to me that you are the young Apollo and that I am Marsyas listening to you. I have sorrows, Dorian, of my own, that even you know nothing of.  The tragedy of old age is not that one is old, but that one is young.  I am amazed sometimes at my own sincerity.  Ah, Dorian, how happy you are! What an exquisite life you have had!  You have drunk deeply of everything.  You have crushed the grapes against your palate.  Nothing has been hidden from you.  And it has all been to you no more than the sound of music.  It has not marred you.  You are still the same."

"I am not the same, Harry."

"Yes, you are the same.  I wonder what the rest of your life will be. Don''t spoil it by renunciations.  At present you are a perfect type. Don''t make yourself incomplete.  You are quite flawless now.  You need not shake your head:  you know you are.  Besides, Dorian, don''t deceive yourself.  Life is not governed by will or intention.  Life is a question of nerves, and fibres, and slowly built-up cells in which thought hides itself and passion has its dreams.  You may fancy yourself safe and think yourself strong.  But a chance tone of colour in a room or a morning sky, a particular perfume that you had once loved and that brings subtle memories with it, a line from a forgotten poem that you had come across again, a cadence from a piece of music that you had ceased to play--I tell you, Dorian, that it is on things like these that our lives depend.  Browning writes about that somewhere; but our own senses will imagine them for us.  There are moments when the odour of lilas blanc passes suddenly across me, and I have to live the strangest month of my life over again.  I wish I could change places with you, Dorian.  The world has cried out against us both, but it has always worshipped you.  It always will worship you. You are the type of what the age is searching for, and what it is afraid it has found.  I am so glad that you have never done anything, never carved a statue, or painted a picture, or produced anything outside of yourself!  Life has been your art.  You have set yourself to music.  Your days are your sonnets."');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Would\'ve loved to use some JS', '“I beg your pardon, Dr. Lanyon,” he replied civilly enough. “What you say is very well founded; and my impatience has shown its heels to my politeness. I come here at the instance of your colleague, Dr. Henry Jekyll, on a piece of business of some moment; and I understood...” He paused and put his hand to his throat, and I could see, in spite of his collected manner, that he was wrestling against the approaches of the hysteria--“I understood, a drawer...”

But here I took pity on my visitor’s suspense, and some perhaps on my own growing curiosity.

“There it is, sir,” said I, pointing to the drawer, where it lay on the floor behind a table and still covered with the sheet.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Scripting to make it all', 'They now walked on in silence, each of them deep in thought. Elizabeth was not comfortable; that was impossible; but she was flattered and pleased. His wish of introducing his sister to her was a compliment of the highest kind. They soon outstripped the others, and when they had reached the carriage, Mr. and Mrs. Gardiner were half a quarter of a mile behind.

He then asked her to walk into the house--but she declared herself not tired, and they stood together on the lawn. At such a time much might have been said, and silence was very awkward. She wanted to talk, but there seemed to be an embargo on every subject. At last she recollected that she had been travelling, and they talked of Matlock and Dove Dale with great perseverance. Yet time and her aunt moved slowly--and her patience and her ideas were nearly worn out before the tete-a-tete was over. On Mr. and Mrs. Gardiner’s coming up they were all pressed to go into the house and take some refreshment; but this was declined, and they parted on each side with utmost politeness. Mr. Darcy handed the ladies into the carriage; and when it drove off, Elizabeth saw him walking slowly towards the house.

The observations of her uncle and aunt now began; and each of them pronounced him to be infinitely superior to anything they had expected. “He is perfectly well behaved, polite, and unassuming,” said her uncle.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Get added for up to 1000', '"There you''re out, Madame Fury," returned he; "for you must know, I never suffer anybody to be in a passion in my house, but myself."

"But you shall," cried she, in a great rage; "for I''ll be in as great a passion as ever I please, without asking your leave: so don''t give yourself no more airs about it. And as for you Miss," again advancing to me, "I order you to follow me this moment, or else I''ll make you repent it all your life."  And, with these words, she flung out of the room.

I was in such extreme terror, at being addressed and threatened in a manner to which I am so wholly unused, that I almost thought I should have fainted.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Articles, but that just didn\'t happen', 'The worship of the senses has often, and with much justice, been decried, men feeling a natural instinct of terror about passions and sensations that seem stronger than themselves, and that they are conscious of sharing with the less highly organized forms of existence. But it appeared to Dorian Gray that the true nature of the senses had never been understood, and that they had remained savage and animal merely because the world had sought to starve them into submission or to kill them by pain, instead of aiming at making them elements of a new spirituality, of which a fine instinct for beauty was to be the dominant characteristic.  As he looked back upon man moving through history, he was haunted by a feeling of loss.  So much had been surrendered! and to such little purpose!  There had been mad wilful rejections, monstrous forms of self-torture and self-denial, whose origin was fear and whose result was a degradation infinitely more terrible than that fancied degradation from which, in their ignorance, they had sought to escape; Nature, in her wonderful irony, driving out the anchorite to feed with the wild animals of the desert and giving to the hermit the beasts of the field as his companions.

Yes:  there was to be, as Lord Henry had prophesied, a new Hedonism that was to recreate life and to save it from that harsh uncomely puritanism that is having, in our own day, its curious revival.  It was to have its service of the intellect, certainly, yet it was never to accept any theory or system that would involve the sacrifice of any mode of passionate experience.  Its aim, indeed, was to be experience itself, and not the fruits of experience, sweet or bitter as they might be.  Of the asceticism that deadens the senses, as of the vulgar profligacy that dulls them, it was to know nothing.  But it was to teach man to concentrate himself upon the moments of a life that is itself but a moment.

There are few of us who have not sometimes wakened before dawn, either after one of those dreamless nights that make us almost enamoured of death, or one of those nights of horror and misshapen joy, when through the chambers of the brain sweep phantoms more terrible than reality itself, and instinct with that vivid life that lurks in all grotesques, and that lends to Gothic art its enduring vitality, this art being, one might fancy, especially the art of those whose minds have been troubled with the malady of reverie.  Gradually white fingers creep through the curtains, and they appear to tremble.  In black fantastic shapes, dumb shadows crawl into the corners of the room and crouch there.  Outside, there is the stirring of birds among the leaves, or the sound of men going forth to their work, or the sigh and sob of the wind coming down from the hills and wandering round the silent house, as though it feared to wake the sleepers and yet must needs call forth sleep from her purple cave.  Veil after veil of thin dusky gauze is lifted, and by degrees the forms and colours of things are restored to them, and we watch the dawn remaking the world in its antique pattern.  The wan mirrors get back their mimic life.  The flameless tapers stand where we had left them, and beside them lies the half-cut book that we had been studying, or the wired flower that we had worn at the ball, or the letter that we had been afraid to read, or that we had read too often. Nothing seems to us changed.  Out of the unreal shadows of the night comes back the real life that we had known.  We have to resume it where we had left off, and there steals over us a terrible sense of the necessity for the continuance of energy in the same wearisome round of stereotyped habits, or a wild longing, it may be, that our eyelids might open some morning upon a world that had been refashioned anew in the darkness for our pleasure, a world in which things would have fresh shapes and colours, and be changed, or have other secrets, a world in which the past would have little or no place, or survive, at any rate, in no conscious form of obligation or regret, the remembrance even of joy having its bitterness and the memories of pleasure their pain.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: I\'m coming up on the end of how many I\'ve', '"Ah! then it must be an illusion.  The things one feels absolutely certain about are never true.  That is the fatality of faith, and the lesson of romance.  How grave you are!  Don''t be so serious.  What have you or I to do with the superstitions of our age?  No:  we have given up our belief in the soul.  Play me something.  Play me a nocturne, Dorian, and, as you play, tell me, in a low voice, how you have kept your youth.  You must have some secret.  I am only ten years older than you are, and I am wrinkled, and worn, and yellow.  You are really wonderful, Dorian.  You have never looked more charming than you do to-night. You remind me of the day I saw you first.  You were rather cheeky, very shy, and absolutely extraordinary.  You have changed, of course, but not in appearance.  I wish you would tell me your secret. To get back my youth I would do anything in the world, except take exercise, get up early, or be respectable.  Youth!  There is nothing like it.  It''s absurd to talk of the ignorance of youth.  The only people to whose opinions I listen now with any respect are people much younger than myself.  They seem in front of me.  Life has revealed to them her latest wonder.  As for the aged, I always contradict the aged. I do it on principle.  If you ask them their opinion on something that happened yesterday, they solemnly give you the opinions current in 1820, when people wore high stocks, believed in everything, and knew absolutely nothing.  How lovely that thing you are playing is!  I wonder, did Chopin write it at Majorca, with the sea weeping round the villa and the salt spray dashing against the panes?  It is marvellously romantic.  What a blessing it is that there is one art left to us that is not imitative!  Don''t stop.  I want music to-night. It seems to me that you are the young Apollo and that I am Marsyas listening to you. I have sorrows, Dorian, of my own, that even you know nothing of.  The tragedy of old age is not that one is old, but that one is young.  I am amazed sometimes at my own sincerity.  Ah, Dorian, how happy you are! What an exquisite life you have had!  You have drunk deeply of everything.  You have crushed the grapes against your palate.  Nothing has been hidden from you.  And it has all been to you no more than the sound of music.  It has not marred you.  You are still the same."

"I am not the same, Harry."

"Yes, you are the same.  I wonder what the rest of your life will be. Don''t spoil it by renunciations.  At present you are a perfect type. Don''t make yourself incomplete.  You are quite flawless now.  You need not shake your head:  you know you are.  Besides, Dorian, don''t deceive yourself.  Life is not governed by will or intention.  Life is a question of nerves, and fibres, and slowly built-up cells in which thought hides itself and passion has its dreams.  You may fancy yourself safe and think yourself strong.  But a chance tone of colour in a room or a morning sky, a particular perfume that you had once loved and that brings subtle memories with it, a line from a forgotten poem that you had come across again, a cadence from a piece of music that you had ceased to play--I tell you, Dorian, that it is on things like these that our lives depend.  Browning writes about that somewhere; but our own senses will imagine them for us.  There are moments when the odour of lilas blanc passes suddenly across me, and I have to live the strangest month of my life over again.  I wish I could change places with you, Dorian.  The world has cried out against us both, but it has always worshipped you.  It always will worship you. You are the type of what the age is searching for, and what it is afraid it has found.  I am so glad that you have never done anything, never carved a statue, or painted a picture, or produced anything outside of yourself!  Life has been your art.  You have set yourself to music.  Your days are your sonnets."');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: copied the SQL inserting', '"I did as he ordered, and when the lawyer arrived I was asked to step up to the room. The fire was burning brightly, and in the grate there was a mass of black, fluffy ashes, as of burned paper, while the brass box stood open and empty beside it. As I glanced at the box I noticed, with a start, that upon the lid was printed the treble K which I had read in the morning upon the envelope.

"''I wish you, John,'' said my uncle, ''to witness my will. I leave my estate, with all its advantages and all its disadvantages, to my brother, your father, whence it will, no doubt, descend to you. If you can enjoy it in peace, well and good! If you find you cannot, take my advice, my boy, and leave it to your deadliest enemy. I am sorry to give you such a two-edged thing, but I can''t say what turn things are going to take. Kindly sign the paper where Mr. Fordham shows you.''

"I signed the paper as directed, and the lawyer took it away with him. The singular incident made, as you may think, the deepest impression upon me, and I pondered over it and turned it every way in my mind without being able to make anything of it. Yet I could not shake off the vague feeling of dread which it left behind, though the sensation grew less keen as the weeks passed and nothing happened to disturb the usual routine of our lives. I could see a change in my uncle, however. He drank more than ever, and he was less inclined for any sort of society. Most of his time he would spend in his room, with the door locked upon the inside, but sometimes he would emerge in a sort of drunken frenzy and would burst out of the house and tear about the garden with a revolver in his hand, screaming out that he was afraid of no man, and that he was not to be cooped up, like a sheep in a pen, by man or devil. When these hot fits were over, however, he would rush tumultuously in at the door and lock and bar it behind him, like a man who can brazen it out no longer against the terror which lies at the roots of his soul. At such times I have seen his face, even on a cold day, glisten with moisture, as though it were new raised from a basin.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: For. Let\'s hope it all works', 'The Dictionary, with a Grammar and History of the English Language, being now at length published, in two volumes folio, the world contemplated with wonder so stupendous a work achieved by one man, while other countries had thought such undertakings fit only for whole academies. Vast as his powers were, I cannot but think that his imagination deceived him, when he supposed that by constant application he might have performed the task in three years.

The extensive reading which was absolutely necessary for the accumulation of authorities, and which alone may account for Johnson''s retentive mind being enriched with a very large and various store of knowledge and imagery, must have occupied several years. The Preface furnishes an eminent instance of a double talent, of which Johnson was fully conscious. Sir Joshua Reynolds heard him say, ''There are two things which I am confident I can do very well: one is an introduction to any literary work, stating what it is to contain, and how it should be executed in the most perfect manner; the other is a conclusion, shewing from various causes why the execution has not been equal to what the authour promised to himself and to the publick.''

A few of his definitions must be admitted to be erroneous. Thus, Windward and Leeward, though directly of opposite meaning, are defined identically the same way; as to which inconsiderable specks it is enough to observe, that his Preface announces that he was aware there might be many such in so immense a work; nor was he at all disconcerted when an instance was pointed out to him. A lady once asked him how he came to define Pastern the KNEE of a horse: instead of making an elaborate defence, as she expected, he at once answered, ''Ignorance, madam, pure ignorance.'' His definition of Network has been often quoted with sportive malignity, as obscuring a thing in itself very plain. But to these frivolous censures no other answer is necessary than that with which we are furnished by his own Preface.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Otherwise I\'m Fucked', '"Oh, my friend, it is because I would save Madam Mina from that awful place that I would go. God forbid that I should take her into that place. There is work--wild work--to be done there, that her eyes may not see. We men here, all save Jonathan, have seen with their own eyes what is to be done before that place can be purify. Remember that we are in terrible straits. If the Count escape us this time--and he is strong and subtle and cunning--he may choose to sleep him for a century, and then in time our dear one"--he took my hand--"would come to him to keep him company, and would be as those others that you, Jonathan, saw. You have told us of their gloating lips; you heard their ribald laugh as they clutched the moving bag that the Count threw to them. You shudder; and well may it be. Forgive me that I make you so much pain, but it is necessary. My friend, is it not a dire need for the which I am giving, possibly my life? If it were that any one went into that place to stay, it is I who would have to go to keep them company."

"Do as you will," said Jonathan, with a sob that shook him all over, "we are in the hands of God!"

Later.--Oh, it did me good to see the way that these brave men worked. How can women help loving men when they are so earnest, and so true, and so brave! And, too, it made me think of the wonderful power of money! What can it not do when it is properly applied; and what might it do when basely used. I felt so thankful that Lord Godalming is rich, and that both he and Mr. Morris, who also has plenty of money, are willing to spend it so freely. For if they did not, our little expedition could not start, either so promptly or so well equipped, as it will within another hour. It is not three hours since it was arranged what part each of us was to do; and now Lord Godalming and Jonathan have a lovely steam launch, with steam up ready to start at a moment''s notice. Dr. Seward and Mr. Morris have half a dozen good horses, well appointed. We have all the maps and appliances of various kinds that can be had. Professor Van Helsing and I are to leave by the 11:40 train to-night for Veresti, where we are to get a carriage to drive to the Borgo Pass. We are bringing a good deal of ready money, as we are to buy a carriage and horses. We shall drive ourselves, for we have no one whom we can trust in the matter. The Professor knows something of a great many languages, so we shall get on all right. We have all got arms, even for me a large-bore revolver; Jonathan would not be happy unless I was armed like the rest. Alas! I cannot carry one arm that the rest do; the scar on my forehead forbids that. Dear Dr. Van Helsing comforts me by telling me that I am fully armed as there may be wolves; the weather is getting colder every hour, and there are snow-flurries which come and go as warnings.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: And none of us wants that to happen', 'The pleasures which I made haste to seek in my disguise were, as I have said, undignified; I would scarce use a harder term. But in the hands of Edward Hyde, they soon began to turn toward the monstrous. When I would come back from these excursions, I was often plunged into a kind of wonder at my vicarious depravity. This familiar that I called out of my own soul, and sent forth alone to do his good pleasure, was a being inherently malign and villainous; his every act and thought centered on self; drinking pleasure with bestial avidity from any degree of torture to another; relentless like a man of stone. Henry Jekyll stood at times aghast before the acts of Edward Hyde; but the situation was apart from ordinary laws, and insidiously relaxed the grasp of conscience. It was Hyde, after all, and Hyde alone, that was guilty. Jekyll was no worse; he woke again to his good qualities seemingly unimpaired; he would even make haste, where it was possible, to undo the evil done by Hyde. And thus his conscience slumbered.

Into the details of the infamy at which I thus connived (for even now I can scarce grant that I committed it) I have no design of entering; I mean but to point out the warnings and the successive steps with which my chastisement approached. I met with one accident which, as it brought on no consequence, I shall no more than mention. An act of cruelty to a child aroused against me the anger of a passer-by, whom I recognised the other day in the person of your kinsman; the doctor and the child’s family joined him; there were moments when I feared for my life; and at last, in order to pacify their too just resentment, Edward Hyde had to bring them to the door, and pay them in a cheque drawn in the name of Henry Jekyll. But this danger was easily eliminated from the future, by opening an account at another bank in the name of Edward Hyde himself; and when, by sloping my own hand backward, I had supplied my double with a signature, I thought I sat beyond the reach of fate.

Some two months before the murder of Sir Danvers, I had been out for one of my adventures, had returned at a late hour, and woke the next day in bed with somewhat odd sensations. It was in vain I looked about me; in vain I saw the decent furniture and tall proportions of my room in the square; in vain that I recognised the pattern of the bed curtains and the design of the mahogany frame; something still kept insisting that I was not where I was, that I had not wakened where I seemed to be, but in the little room in Soho where I was accustomed to sleep in the body of Edward Hyde. I smiled to myself, and in my psychological way, began lazily to inquire into the elements of this illusion, occasionally, even as I did so, dropping back into a comfortable morning doze. I was still so engaged when, in one of my more wakeful moments, my eyes fell upon my hand. Now the hand of Henry Jekyll (as you have often remarked) was professional in shape and size: it was large, firm, white and comely. But the hand which I now saw, clearly enough, in the yellow light of a mid-London morning, lying half shut on the bedclothes, was lean, corder, knuckly, of a dusky pallor and thickly shaded with a swart growth of hair. It was the hand of Edward Hyde.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: This is the penultimate article I\'m writing', 'The King of Ceilan rode through his city with a large ruby in his hand, as the ceremony of his coronation.  The gates of the palace of John the Priest were "made of sardius, with the horn of the horned snake inwrought, so that no man might bring poison within." Over the gable were "two golden apples, in which were two carbuncles," so that the gold might shine by day and the carbuncles by night.  In Lodge''s strange romance ''A Margarite of America'', it was stated that in the chamber of the queen one could behold "all the chaste ladies of the world, inchased out of silver, looking through fair mirrours of chrysolites, carbuncles, sapphires, and greene emeraults." Marco Polo had seen the inhabitants of Zipangu place rose-coloured pearls in the mouths of the dead.  A sea-monster had been enamoured of the pearl that the diver brought to King Perozes, and had slain the thief, and mourned for seven moons over its loss.  When the Huns lured the king into the great pit, he flung it away--Procopius tells the story--nor was it ever found again, though the Emperor Anastasius offered five hundred-weight of gold pieces for it.  The King of Malabar had shown to a certain Venetian a rosary of three hundred and four pearls, one for every god that he worshipped.

When the Duke de Valentinois, son of Alexander VI, visited Louis XII of France, his horse was loaded with gold leaves, according to Brantome, and his cap had double rows of rubies that threw out a great light. Charles of England had ridden in stirrups hung with four hundred and twenty-one diamonds.  Richard II had a coat, valued at thirty thousand marks, which was covered with balas rubies.  Hall described Henry VIII, on his way to the Tower previous to his coronation, as wearing "a jacket of raised gold, the placard embroidered with diamonds and other rich stones, and a great bauderike about his neck of large balasses." The favourites of James I wore ear-rings of emeralds set in gold filigrane.  Edward II gave to Piers Gaveston a suit of red-gold armour studded with jacinths, a collar of gold roses set with turquoise-stones, and a skull-cap parseme with pearls.  Henry II wore jewelled gloves reaching to the elbow, and had a hawk-glove sewn with twelve rubies and fifty-two great orients.  The ducal hat of Charles the Rash, the last Duke of Burgundy of his race, was hung with pear-shaped pearls and studded with sapphires.

How exquisite life had once been!  How gorgeous in its pomp and decoration!  Even to read of the luxury of the dead was wonderful.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: This is the ultimate article', '"Ah! then it must be an illusion.  The things one feels absolutely certain about are never true.  That is the fatality of faith, and the lesson of romance.  How grave you are!  Don''t be so serious.  What have you or I to do with the superstitions of our age?  No:  we have given up our belief in the soul.  Play me something.  Play me a nocturne, Dorian, and, as you play, tell me, in a low voice, how you have kept your youth.  You must have some secret.  I am only ten years older than you are, and I am wrinkled, and worn, and yellow.  You are really wonderful, Dorian.  You have never looked more charming than you do to-night. You remind me of the day I saw you first.  You were rather cheeky, very shy, and absolutely extraordinary.  You have changed, of course, but not in appearance.  I wish you would tell me your secret. To get back my youth I would do anything in the world, except take exercise, get up early, or be respectable.  Youth!  There is nothing like it.  It''s absurd to talk of the ignorance of youth.  The only people to whose opinions I listen now with any respect are people much younger than myself.  They seem in front of me.  Life has revealed to them her latest wonder.  As for the aged, I always contradict the aged. I do it on principle.  If you ask them their opinion on something that happened yesterday, they solemnly give you the opinions current in 1820, when people wore high stocks, believed in everything, and knew absolutely nothing.  How lovely that thing you are playing is!  I wonder, did Chopin write it at Majorca, with the sea weeping round the villa and the salt spray dashing against the panes?  It is marvellously romantic.  What a blessing it is that there is one art left to us that is not imitative!  Don''t stop.  I want music to-night. It seems to me that you are the young Apollo and that I am Marsyas listening to you. I have sorrows, Dorian, of my own, that even you know nothing of.  The tragedy of old age is not that one is old, but that one is young.  I am amazed sometimes at my own sincerity.  Ah, Dorian, how happy you are! What an exquisite life you have had!  You have drunk deeply of everything.  You have crushed the grapes against your palate.  Nothing has been hidden from you.  And it has all been to you no more than the sound of music.  It has not marred you.  You are still the same."

"I am not the same, Harry."

"Yes, you are the same.  I wonder what the rest of your life will be. Don''t spoil it by renunciations.  At present you are a perfect type. Don''t make yourself incomplete.  You are quite flawless now.  You need not shake your head:  you know you are.  Besides, Dorian, don''t deceive yourself.  Life is not governed by will or intention.  Life is a question of nerves, and fibres, and slowly built-up cells in which thought hides itself and passion has its dreams.  You may fancy yourself safe and think yourself strong.  But a chance tone of colour in a room or a morning sky, a particular perfume that you had once loved and that brings subtle memories with it, a line from a forgotten poem that you had come across again, a cadence from a piece of music that you had ceased to play--I tell you, Dorian, that it is on things like these that our lives depend.  Browning writes about that somewhere; but our own senses will imagine them for us.  There are moments when the odour of lilas blanc passes suddenly across me, and I have to live the strangest month of my life over again.  I wish I could change places with you, Dorian.  The world has cried out against us both, but it has always worshipped you.  It always will worship you. You are the type of what the age is searching for, and what it is afraid it has found.  I am so glad that you have never done anything, never carved a statue, or painted a picture, or produced anything outside of yourself!  Life has been your art.  You have set yourself to music.  Your days are your sonnets."');

#
#  DONE WITH ARTICLES
#



#
# AUTHORS
#

insert into Backend.authors (firstName, lastName)
values ('testAuthorFirstName1', 'testAuthorLastName1');

insert into Backend.authors (firstName, lastName)
values ('testAuthorFirstName2', 'testAuthorLastName2');

insert into Backend.authors (firstName, lastName)
values ('testAuthorFirstName3', 'testAuthorLastName3');

insert into Backend.authors (firstName, lastName)
values ('Just', 'Have');


insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article1'),
		(select min(id) from Backend.authors where firstName = 'testAuthorFirstName1'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article2'),
		(select min(id) from Backend.authors where firstName = 'testAuthorFirstName2'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article2'),
		(select min(id) from Backend.authors where firstName = 'testAuthorFirstName3'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article3'),
		(select min(id) from Backend.authors where firstName = 'Just'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article4'),
		(select min(id) from Backend.authors where firstName = 'Just'));



insert into Backend.articleTags (articleID, tagID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article1'),
		(select min(id) from Backend.tags where tag = 'TestTag'));

insert into Backend.articleTags (articleID, tagID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article1'),
		(select min(id) from Backend.tags where tag = 'TestTag2'));

insert into Backend.articleTags (articleID, tagID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article2'),
		(select min(id) from Backend.tags where tag = 'TestTag'));
