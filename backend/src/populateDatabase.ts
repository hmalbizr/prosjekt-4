import Express from 'express';
import Schema from './schema';
import {graphqlHTTP} from "express-graphql";
import Axios from "axios";

const bodyParser = require("body-parser");
const request = require("request");


const APP_PORT = 8070;
const app = Express();

//app.use(bodyParser.urlencoded({extended: false}));
//app.use(bodyParser.json());

app.use('/graphql', graphqlHTTP({
	schema: Schema,
	pretty: true,
	graphiql: true,
}));

// start the server
app.listen(APP_PORT, () => {
	console.log(`App listening on port ${APP_PORT}`)
});

function populate() {
	for (let i: number = 0; i < 4; i++) {
		console.log("running: " + i);
		if (i === 50) {
			Axios.get('https://litipsum.com/api/json').then(function (result) {
				let data = result["data"];
				let title = data["title"];
				console.log(typeof data);
				console.log(data);
				console.log(typeof title);
				console.log(title);
			})
		}
		else
		{
			//Axios({url: "http://localhost:8070/graphql"})
			Axios.post("http://localhost:8070/graphql", {query: "{article{id}}"}).then(function (response) {
				console.log(JSON.stringify(response.data));
			}).catch(function (err) {
				console.log(err);
			})
		}
	}
}

populate();

