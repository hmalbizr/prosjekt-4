import Express from 'express';
import Schema from './schema';
import {graphqlHTTP} from "express-graphql";


const APP_PORT = 8080;
const app = Express();


app.use('/graphql', graphqlHTTP({
	schema: Schema,
	pretty: true,
	graphiql: true,
}));


app.listen(APP_PORT, () => {
	console.log(`App listening on port ${APP_PORT}`)
});
