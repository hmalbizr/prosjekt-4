insert into Backend.articles (title, text)
values ('Test-article', 'Lorem ipsum. Med store forventninger til alt av verdens barn');

insert into Backend.articles (title, text)
values ('Test-article2', 'Lorem ipsum. Med store forventninger til alt av verdens barn');

insert into Backend.articles (title, text)
values ('Test-article3', 'Lorem ipsum. Med store forventninger til alt av verdens barn');

insert into Backend.articles (title, text)
values ('Test-article4', 'Lorem ipsum. Med store forventninger til alt av verdens barn');

insert into Backend.users (username, password)
values ('test', 'test');

insert into Backend.users (username, password)
values ('test2', 'test2');

insert into Backend.authors (firstName, lastName)
values ('testFirstName', 'testLastName');

insert into Backend.authors (firstName, lastName)
values ('test2FirstName', 'test2LastName');

insert into Backend.authors (firstName, lastName)
values ('test3FirstName', 'test3LastName');

insert into Backend.tags (tag)
values ('TestTag');

insert into Backend.tags (tag)
values ('TestTag2');

insert into Backend.comments (userID, articleID, text)
values ((Select id from Backend.users where username = 'test'),
		(select min(id) from Backend.articles where title = 'Test-article'),
		'Test-comment');

insert into Backend.comments (userID, articleID, text)
values ((Select id from Backend.users where username = 'test2'),
		(select min(id) from Backend.articles where title = 'Test-article2'),
		'Test-comment2');

insert into Backend.comments (userID, articleID, text)
values ((Select id from Backend.users where username = 'test'),
		(select min(id) from Backend.articles where title = 'Test-article2'),
		'Test-comment3');

insert into Backend.comments (userID, articleID, text)
values ((Select id from Backend.users where username = 'test'),
		(select min(id) from Backend.articles where title = 'Test-article'),
		'Test-comment4');

insert into Backend.articleTags (articleID, tagID)
values ((select min(id) from Backend.articles where title = 'Test-article'),
		(select min(id) from Backend.tags where tag = 'TestTag'));

insert into Backend.articleTags (articleID, tagID)
values ((select min(id) from Backend.articles where title = 'Test-article'),
		(select min(id) from Backend.tags where tag = 'TestTag2'));

insert into Backend.articleTags (articleID, tagID)
values ((select min(id) from Backend.articles where title = 'Test-article2'),
		(select min(id) from Backend.tags where tag = 'TestTag'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'Test-article'),
		(select min(id) from Backend.authors where firstName = 'testFirstName'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'Test-article2'),
		(select min(id) from Backend.authors where firstName = 'testFirstName'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'Test-article3'),
		(select min(id) from Backend.authors where firstName = 'test2FirstName'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'Test-article2'),
		(select min(id) from Backend.authors where firstName = 'test3FirstName'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'Test-article4'),
		(select min(id) from Backend.authors where firstName = 'testFirstName'));

insert into Backend.savedArticles (userID, articleID)
values ((Select id from Backend.users where username = 'test'),
		(select min(id) from Backend.articles where title = 'Test-article'));

insert into Backend.savedArticles (userID, articleID)
values ((Select id from Backend.users where username = 'test2'),
		(select min(id) from Backend.articles where title = 'Test-article'));

insert into Backend.savedArticles (userID, articleID)
values ((Select id from Backend.users where username = 'test'),
		(select min(id) from Backend.articles where title = 'Test-article2'));
