const {Sequelize} = require('sequelize');

const Conn = new Sequelize(
	'Backend',
	'Inserter',
	'iN53r73R?',
	{
		dialect: 'mysql',
		host: 'it2810-25.idi.ntnu.no',
		define: {
			timestamps: false
		}
	}
);

// The data of the database must be added to a model here, otherwise it will not be found.

Conn.define('user',
	{
		username:
			{
				type: Sequelize.STRING,
				allowNull: false
			},
		password:
			{
				type: Sequelize.STRING,
				allowNull: false
			}
	});

Conn.define('article',
	{
		timestamp:
			{
				type: Sequelize.DATE,
				allowNull: false,
				defaultValue: Sequelize.NOW,
			},
		title:
			{
				type: Sequelize.STRING,
				allowNull: false,
			},
		text:
			{
				type: Sequelize.TEXT,
				allowNull: false,
			}
	}
);

Conn.define("author",
	{
		firstName:
			{
				type: Sequelize.STRING,
				allowNull: false,
			},
		lastName:
			{
				type: Sequelize.STRING,
				allowNull: false,
			}
	}
);

Conn.define("tag",
	{
		tag:
			{
				type: Sequelize.STRING,
				allowNull: false,
			},
	}
);

Conn.define("comment",
	{
		userID:
			{
				type: Sequelize.INTEGER,
				allowNull: false,
			},
		articleID:
			{
				type: Sequelize.INTEGER,
				allowNull: false,
			},
		timestamp:
			{
				type: Sequelize.DATE,
				allowNull: false,
				defaultValue: Sequelize.NOW
			},
		text:
			{
				type: Sequelize.TEXT,
				allowNull: false,
			},
	}
);

Conn.define("articleTags",
	{
		articleID:
			{
				type: Sequelize.INTEGER,
				allowNull: false,
			},
		tagID:
			{
				type: Sequelize.INTEGER,
				allowNull: false,
			},
	}
);

Conn.define("articleAuthors",
	{
		articleID:
			{
				type: Sequelize.INTEGER,
				allowNull: false,
			},
		authorID:
			{
				type: Sequelize.INTEGER,
				allowNull: false,
			},
	}
);

Conn.define("savedArticles",
	{
		userID:
			{
				type: Sequelize.INTEGER,
				allowNull: false,
			},
		articleID:
			{
				type: Sequelize.INTEGER,
				allowNull: false,
			},
	}
);


// Wish I could've made Sequelize work well with GraphQL in regards to quickly getting out proper answers.
// It is probably very much possible, but the tutorials I have seen didn't help me towards doing it with the setup
// we already have. Thus all the relationships that can be defined in Sequelize won't be of use.
/*
// Relationships
Conn.models.article.hasMany(Conn.models.comment, {foreignKey: {name: "articleID"}});
Conn.models.comment.belongsTo(Conn.models.article);
Conn.models.comment.belongsTo(Conn.models.user);
Conn.models.user.hasMany(Conn.models.comment, {foreignKey:{name: "userID"}});
Conn.models.article.hasMany(Conn.models.articleAuthors);
Conn.models.author.hasMany(Conn.models.articleAuthors);
Conn.models.articleAuthors.belongsTo(Conn.models.author);
//Conn.models.articleAuthors.hasOne(Conn.models.article);*/

/*Conn.models.article.belongsToMany(Conn.models.author, {through: Conn.models.articleAuthors, foreignKey: "articleID"});
Conn.models.author.belongsToMany(Conn.models.article, {through: Conn.models.articleAuthors, foreignKey: "authorID"});*/


/*Conn.define("",
	{}
);*/

export default Conn;
