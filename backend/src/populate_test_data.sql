insert into Backend.articles (title, text)
values ('LitIpsum: Test-article_1', '"As a rule," said Holmes, "the more bizarre a thing is the less mysterious it proves to be. It is your commonplace, featureless crimes which are really puzzling, just as a commonplace face is the most difficult to identify. But I must be prompt over this matter."

"What are you going to do, then?" I asked.

"To smoke," he answered. "It is quite a three pipe problem, and I beg that you won''t speak to me for fifty minutes." He curled himself up in his chair, with his thin knees drawn up to his hawk-like nose, and there he sat with his eyes closed and his black clay pipe thrusting out like the bill of some strange bird. I had come to the conclusion that he had dropped asleep, and indeed was nodding myself, when he suddenly sprang out of his chair with the gesture of a man who has made up his mind and put his pipe down upon the mantelpiece.

"Sarasate plays at the St. James''s Hall this afternoon," he remarked. "What do you think, Watson? Could your patients spare you for a few hours?"

"I have nothing to do to-day. My practice is never very absorbing."

"Then put on your hat and come. I am going through the City first, and we can have some lunch on the way. I observe that there is a good deal of German music on the programme, which is rather more to my taste than Italian or French. It is introspective, and I want to introspect. Come along!"

We travelled by the Underground as far as Aldersgate; and a short walk took us to Saxe-Coburg Square, the scene of the singular story which we had listened to in the morning. It was a poky, little, shabby-genteel place, where four lines of dingy two-storied brick houses looked out into a small railed-in enclosure, where a lawn of weedy grass and a few clumps of faded laurel-bushes made a hard fight against a smoke-laden and uncongenial atmosphere. Three gilt balls and a brown board with "JABEZ WILSON" in white letters, upon a corner house, announced the place where our red-headed client carried on his business. Sherlock Holmes stopped in front of it with his head on one side and looked it all over, with his eyes shining brightly between puckered lids. Then he walked slowly up the street, and then down again to the corner, still looking keenly at the houses. Finally he returned to the pawnbroker''s, and, having thumped vigorously upon the pavement with his stick two or three times, he went up to the door and knocked. It was instantly opened by a bright-looking, clean-shaven young fellow, who asked him to step in.

"Thank you," said Holmes, "I only wished to ask you how you would go from here to the Strand."

"Third right, fourth left," answered the assistant promptly, closing the door.

"Smart fellow, that," observed Holmes as we walked away. "He is, in my judgment, the fourth smartest man in London, and for daring I am not sure that he has not a claim to be third. I have known something of him before."

"Evidently," said I, "Mr. Wilson''s assistant counts for a good deal in this mystery of the Red-headed League. I am sure that you inquired your way merely in order that you might see him."

"Not him."

"What then?"

"The knees of his trousers."

"And what did you see?"

"What I expected to see."

"Why did you beat the pavement?"

"My dear doctor, this is a time for observation, not for talk. We are spies in an enemy''s country. We know something of Saxe-Coburg Square. Let us now explore the parts which lie behind it."

The road in which we found ourselves as we turned round the corner from the retired Saxe-Coburg Square presented as great a contrast to it as the front of a picture does to the back. It was one of the main arteries which conveyed the traffic of the City to the north and west. The roadway was blocked with the immense stream of commerce flowing in a double tide inward and outward, while the footpaths were black with the hurrying swarm of pedestrians. It was difficult to realise as we looked at the line of fine shops and stately business premises that they really abutted on the other side upon the faded and stagnant square which we had just quitted.

"Let me see," said Holmes, standing at the corner and glancing along the line, "I should like just to remember the order of the houses here. It is a hobby of mine to have an exact knowledge of London. There is Mortimer''s, the tobacconist, the little newspaper shop, the Coburg branch of the City and Suburban Bank, the Vegetarian Restaurant, and McFarlane''s carriage-building depot. That carries us right on to the other block. And now, Doctor, we''ve done our work, so it''s time we had some play. A sandwich and a cup of coffee, and then off to violin-land, where all is sweetness and delicacy and harmony, and there are no red-headed clients to vex us with their conundrums."

My friend was an enthusiastic musician, being himself not only a very capable performer but a composer of no ordinary merit. All the afternoon he sat in the stalls wrapped in the most perfect happiness, gently waving his long, thin fingers in time to the music, while his gently smiling face and his languid, dreamy eyes were as unlike those of Holmes the sleuth-hound, Holmes the relentless, keen-witted, ready-handed criminal agent, as it was possible to conceive. In his singular character the dual nature alternately asserted itself, and his extreme exactness and astuteness represented, as I have often thought, the reaction against the poetic and contemplative mood which occasionally predominated in him. The swing of his nature took him from extreme languor to devouring energy; and, as I knew well, he was never so truly formidable as when, for days on end, he had been lounging in his armchair amid his improvisations and his black-letter editions. Then it was that the lust of the chase would suddenly come upon him, and that his brilliant reasoning power would rise to the level of intuition, until those who were unacquainted with his methods would look askance at him as on a man whose knowledge was not that of other mortals. When I saw him that afternoon so enwrapped in the music at St. James''s Hall I felt that an evil time might be coming upon those whom he had set himself to hunt down.

"You want to go home, no doubt, Doctor," he remarked as we emerged.

"Yes, it would be as well."

"And I have some business to do which will take some hours. This business at Coburg Square is serious."

"Why serious?"

"A considerable crime is in contemplation. I have every reason to believe that we shall be in time to stop it. But to-day being Saturday rather complicates matters. I shall want your help to-night."

"At what time?"

"Ten will be early enough."

"I shall be at Baker Street at ten."

"Very well. And, I say, Doctor, there may be some little danger, so kindly put your army revolver in your pocket." He waved his hand, turned on his heel, and disappeared in an instant among the crowd.');

insert into Backend.articles (title, text)
values ('LitIpsum: Test-article2', 'His behaviour to her sister was such, during dinner time, as showed an admiration of her, which, though more guarded than formerly, persuaded Elizabeth, that if left wholly to himself, Jane’s happiness, and his own, would be speedily secured. Though she dared not depend upon the consequence, she yet received pleasure from observing his behaviour. It gave her all the animation that her spirits could boast; for she was in no cheerful humour. Mr. Darcy was almost as far from her as the table could divide them. He was on one side of her mother. She knew how little such a situation would give pleasure to either, or make either appear to advantage. She was not near enough to hear any of their discourse, but she could see how seldom they spoke to each other, and how formal and cold was their manner whenever they did. Her mother’s ungraciousness, made the sense of what they owed him more painful to Elizabeth’s mind; and she would, at times, have given anything to be privileged to tell him that his kindness was neither unknown nor unfelt by the whole of the family.

She was in hopes that the evening would afford some opportunity of bringing them together; that the whole of the visit would not pass away without enabling them to enter into something more of conversation than the mere ceremonious salutation attending his entrance. Anxious and uneasy, the period which passed in the drawing-room, before the gentlemen came, was wearisome and dull to a degree that almost made her uncivil. She looked forward to their entrance as the point on which all her chance of pleasure for the evening must depend.

“If he does not come to me, then,” said she, “I shall give him up for ever.”

The gentlemen came; and she thought he looked as if he would have answered her hopes; but, alas! the ladies had crowded round the table, where Miss Bennet was making tea, and Elizabeth pouring out the coffee, in so close a confederacy that there was not a single vacancy near her which would admit of a chair. And on the gentlemen’s approaching, one of the girls moved closer to her than ever, and said, in a whisper:

“The men shan’t come and part us, I am determined. We want none of them; do we?”

Darcy had walked away to another part of the room. She followed him with her eyes, envied everyone to whom he spoke, had scarcely patience enough to help anybody to coffee; and then was enraged against herself for being so silly!

“A man who has once been refused! How could I ever be foolish enough to expect a renewal of his love? Is there one among the sex, who would not protest against such a weakness as a second proposal to the same woman? There is no indignity so abhorrent to their feelings!”

She was a little revived, however, by his bringing back his coffee cup himself; and she seized the opportunity of saying:

“Is your sister at Pemberley still?”

“Yes, she will remain there till Christmas.”

“And quite alone? Have all her friends left her?”

“Mrs. Annesley is with her. The others have been gone on to Scarborough, these three weeks.”

She could think of nothing more to say; but if he wished to converse with her, he might have better success. He stood by her, however, for some minutes, in silence; and, at last, on the young lady’s whispering to Elizabeth again, he walked away.

When the tea-things were removed, and the card-tables placed, the ladies all rose, and Elizabeth was then hoping to be soon joined by him, when all her views were overthrown by seeing him fall a victim to her mother’s rapacity for whist players, and in a few moments after seated with the rest of the party. She now lost every expectation of pleasure. They were confined for the evening at different tables, and she had nothing to hope, but that his eyes were so often turned towards her side of the room, as to make him play as unsuccessfully as herself.

Mrs. Bennet had designed to keep the two Netherfield gentlemen to supper; but their carriage was unluckily ordered before any of the others, and she had no opportunity of detaining them.

“Well girls,” said she, as soon as they were left to themselves, “What say you to the day? I think every thing has passed off uncommonly well, I assure you. The dinner was as well dressed as any I ever saw. The venison was roasted to a turn--and everybody said they never saw so fat a haunch. The soup was fifty times better than what we had at the Lucases’ last week; and even Mr. Darcy acknowledged, that the partridges were remarkably well done; and I suppose he has two or three French cooks at least. And, my dear Jane, I never saw you look in greater beauty. Mrs. Long said so too, for I asked her whether you did not. And what do you think she said besides? ‘Ah! Mrs. Bennet, we shall have her at Netherfield at last.’ She did indeed. I do think Mrs. Long is as good a creature as ever lived--and her nieces are very pretty behaved girls, and not at all handsome: I like them prodigiously.”

Mrs. Bennet, in short, was in very great spirits; she had seen enough of Bingley’s behaviour to Jane, to be convinced that she would get him at last; and her expectations of advantage to her family, when in a happy humour, were so far beyond reason, that she was quite disappointed at not seeing him there again the next day, to make his proposals.

“It has been a very agreeable day,” said Miss Bennet to Elizabeth. “The party seemed so well selected, so suitable one with the other. I hope we may often meet again.”

Elizabeth smiled.

“Lizzy, you must not do so. You must not suspect me. It mortifies me. I assure you that I have now learnt to enjoy his conversation as an agreeable and sensible young man, without having a wish beyond it. I am perfectly satisfied, from what his manners now are, that he never had any design of engaging my affection. It is only that he is blessed with greater sweetness of address, and a stronger desire of generally pleasing, than any other man.”

“You are very cruel,” said her sister, “you will not let me smile, and are provoking me to it every moment.”

“How hard it is in some cases to be believed!”

“And how impossible in others!”

“But why should you wish to persuade me that I feel more than I acknowledge?”

“That is a question which I hardly know how to answer. We all love to instruct, though we can teach only what is not worth knowing. Forgive me; and if you persist in indifference, do not make me your confidante.”

A few days after this visit, Mr. Bingley called again, and alone. His friend had left him that morning for London, but was to return home in ten days time. He sat with them above an hour, and was in remarkably good spirits. Mrs. Bennet invited him to dine with them; but, with many expressions of concern, he confessed himself engaged elsewhere.

“Next time you call,” said she, “I hope we shall be more lucky.”');

insert into Backend.articles (title, text)
values ('LitIpsum: Test-article3', 'As Bradshaw left, the lawyer looked at his watch. “And now, Poole, let us get to ours,” he said; and taking the poker under his arm, led the way into the yard. The scud had banked over the moon, and it was now quite dark. The wind, which only broke in puffs and draughts into that deep well of building, tossed the light of the candle to and fro about their steps, until they came into the shelter of the theatre, where they sat down silently to wait. London hummed solemnly all around; but nearer at hand, the stillness was only broken by the sounds of a footfall moving to and fro along the cabinet floor.

“So it will walk all day, sir,” whispered Poole; “ay, and the better part of the night. Only when a new sample comes from the chemist, there’s a bit of a break. Ah, it’s an ill conscience that’s such an enemy to rest! Ah, sir, there’s blood foully shed in every step of it! But hark again, a little closer--put your heart in your ears, Mr. Utterson, and tell me, is that the doctor’s foot?”

The steps fell lightly and oddly, with a certain swing, for all they went so slowly; it was different indeed from the heavy creaking tread of Henry Jekyll. Utterson sighed. “Is there never anything else?” he asked.

Poole nodded. “Once,” he said. “Once I heard it weeping!”

“Weeping? how that?” said the lawyer, conscious of a sudden chill of horror.

“Weeping like a woman or a lost soul,” said the butler. “I came away with that upon my heart, that I could have wept too.”

But now the ten minutes drew to an end. Poole disinterred the axe from under a stack of packing straw; the candle was set upon the nearest table to light them to the attack; and they drew near with bated breath to where that patient foot was still going up and down, up and down, in the quiet of the night. “Jekyll,” cried Utterson, with a loud voice, “I demand to see you.” He paused a moment, but there came no reply. “I give you fair warning, our suspicions are aroused, and I must and shall see you,” he resumed; “if not by fair means, then by foul--if not of your consent, then by brute force!”

“Utterson,” said the voice, “for God’s sake, have mercy!”

“Ah, that’s not Jekyll’s voice--it’s Hyde’s!” cried Utterson. “Down with the door, Poole!”

Poole swung the axe over his shoulder; the blow shook the building, and the red baize door leaped against the lock and hinges. A dismal screech, as of mere animal terror, rang from the cabinet. Up went the axe again, and again the panels crashed and the frame bounded; four times the blow fell; but the wood was tough and the fittings were of excellent workmanship; and it was not until the fifth, that the lock burst and the wreck of the door fell inwards on the carpet.

The besiegers, appalled by their own riot and the stillness that had succeeded, stood back a little and peered in. There lay the cabinet before their eyes in the quiet lamplight, a good fire glowing and chattering on the hearth, the kettle singing its thin strain, a drawer or two open, papers neatly set forth on the business table, and nearer the fire, the things laid out for tea; the quietest room, you would have said, and, but for the glazed presses full of chemicals, the most commonplace that night in London.

Right in the middle there lay the body of a man sorely contorted and still twitching. They drew near on tiptoe, turned it on its back and beheld the face of Edward Hyde. He was dressed in clothes far too large for him, clothes of the doctor’s bigness; the cords of his face still moved with a semblance of life, but life was quite gone: and by the crushed phial in the hand and the strong smell of kernels that hung upon the air, Utterson knew that he was looking on the body of a self-destroyer.

“We have come too late,” he said sternly, “whether to save or punish. Hyde is gone to his account; and it only remains for us to find the body of your master.”

The far greater proportion of the building was occupied by the theatre, which filled almost the whole ground storey and was lighted from above, and by the cabinet, which formed an upper story at one end and looked upon the court. A corridor joined the theatre to the door on the by-street; and with this the cabinet communicated separately by a second flight of stairs. There were besides a few dark closets and a spacious cellar. All these they now thoroughly examined. Each closet needed but a glance, for all were empty, and all, by the dust that fell from their doors, had stood long unopened. The cellar, indeed, was filled with crazy lumber, mostly dating from the times of the surgeon who was Jekyll’s predecessor; but even as they opened the door they were advertised of the uselessness of further search, by the fall of a perfect mat of cobweb which had for years sealed up the entrance. No where was there any trace of Henry Jekyll dead or alive.

Poole stamped on the flags of the corridor. “He must be buried here,” he said, hearkening to the sound.

“Or he may have fled,” said Utterson, and he turned to examine the door in the by-street. It was locked; and lying near by on the flags, they found the key, already stained with rust.

“This does not look like use,” observed the lawyer.

“Use!” echoed Poole. “Do you not see, sir, it is broken? much as if a man had stamped on it.”

“Ay,” continued Utterson, “and the fractures, too, are rusty.” The two men looked at each other with a scare. “This is beyond me, Poole,” said the lawyer. “Let us go back to the cabinet.”

They mounted the stair in silence, and still with an occasional awestruck glance at the dead body, proceeded more thoroughly to examine the contents of the cabinet. At one table, there were traces of chemical work, various measured heaps of some white salt being laid on glass saucers, as though for an experiment in which the unhappy man had been prevented.

“That is the same drug that I was always bringing him,” said Poole; and even as he spoke, the kettle with a startling noise boiled over.

This brought them to the fireside, where the easy-chair was drawn cosily up, and the tea things stood ready to the sitter’s elbow, the very sugar in the cup. There were several books on a shelf; one lay beside the tea things open, and Utterson was amazed to find it a copy of a pious work, for which Jekyll had several times expressed a great esteem, annotated, in his own hand with startling blasphemies.

Next, in the course of their review of the chamber, the searchers came to the cheval-glass, into whose depths they looked with an involuntary horror. But it was so turned as to show them nothing but the rosy glow playing on the roof, the fire sparkling in a hundred repetitions along the glazed front of the presses, and their own pale and fearful countenances stooping to look in.

“This glass has seen some strange things, sir,” whispered Poole.');

insert into Backend.articles (title, text)
values ('LitIpsum: Test-article4', '"Whoever she is, I wish she would mind her own affairs: I don''t know what the devil a woman lives for after thirty: she is only in other folk''s way. Shall you be at the assembly?"

"I believe not, my Lord."

"No!-why then, how in the world can you contrive to pass your time?"

"In a manner which your Lordship will think very extraordinary," cried Mrs.  Selwyn, "for the young lady reads."

"Ha, ha, ha! Egad, my Lord," cried the facetious companion, "you are got into bad hands."

"You had better, Ma''am," answered he, "attack Jack Coverley here, for you will make nothing of me."

"Of you, my Lord," cried she, "Heaven forbid I should ever entertain so idle an expectation! I only talk, like a silly woman, for the sake of talking; but I have by no means so low an opinion of your Lordship, as to suppose you vulnerable to censure."

"Do, pray, Ma''am," cried he, "turn to Jack Coverley; he''s the very man for you;-he''d be a wit himself if he was not too modest."

"Prithee, my Lord, be quiet," returned the other; "if the lady is contented to bestow all her favours upon you, why should you make such a point of my going snacks?"

"Don''t be apprehensive, gentlemen," said Mrs. Selwyn, drily, "I am not romantic;-I have not the least design of doing good to either of you."

"Have not you been ill since I saw you?" said his Lordship, again addressing himself to me.

"Yes, my Lord."

"I thought so; you are paler than you was, and I suppose that''s the reason I did not recollect you sooner."

"Has not your Lordship too much gallantry," cried Mrs. Selwyn, "to discover a young lady''s illness by her looks?"

"The devil a word can I speak for that woman," said he, in a low voice; "do, prithee, Jack, take her in hand."

"Excuse me, my Lord," answered Mr. Coverley.

"When shall I see you again?" continued his Lordship; "do you go to the pump-room every morning?"

"No, my Lord."

"Do you ride out?"

"No, my Lord."

Just then we arrived at the pump-room, and an end was put to our conversation, if it is not an abuse of words to give such a term to a string of rude questions and free compliments.

He had not opportunity to say much more to me, as Mrs. Selwyn joined a large party, and I walked home between two ladies. He had, however, the curiosity to see us to the door.

Mrs. Selwyn was very eager to know how I had made acquaintance with this nobleman, whose manners so evidently announced the character of a confirmed libertine. I could give her very little satisfaction, as I was ignorant even of his name: but, in the afternoon, Mr. Ridgeway, the apothecary, gave us very ample information.

As his person was easily described, for he is remarkably tall, Mr. Ridgeway told us he was Lord Merton, a nobleman who is but lately come to his title, though he has already dissipated more than half his fortune; a professed admirer of beauty, but a man of most licentious character; that among men, his companions consisted chiefly of gamblers and jockeys, and among women he was rarely admitted.

"Well, Miss Anville," said Mrs. Selwyn, "I am glad I was not more civil to him. You may depend upon me for keeping him at a distance."

"O, Madam," said Mr. Ridgeway, "he may now be admitted any where, for he is going to reform."

"Has he, under that notion, persuaded any fool to marry him?"

"Not yet, Madam, but a marriage is expected to take place shortly: it has been some time in agitation; but the friends of the lady have obliged her to wait till she is of age: however, her brother, who has chiefly opposed the match, now that she is near being at her own disposal, is tolerably quiet. She is very pretty, and will have a large fortune. We expect her at the Wells every day."

"What is her name?" said Mrs. Selwyn.

"Larpent," answered he: "Lady Louisa Larpent, sister of Lord Orville."

"Lord Orville!" repeated I, all amazement.

"Yes, Ma''am; his Lordship is coming with her. I have had certain information.  They are to be at the Honourable Mrs. Beaumont''s. She is a relation of my Lord''s, and has a very fine house upon Clifton Hill."

His Lordship is coming with her! -Good God, what an emotion did those words give me! How strange, my dear Sir, that, just at this time, he should visit Bristol! It will be impossible for me to avoid seeing him, as Mrs. Selwyn is very well acquainted with Mrs. Beaumont. Indeed, I have had an escape in not being under the same roof with him, for Mrs. Beaumont invited us to her house immediately upon our arrival; but the inconvenience of being so distant from the pump-room made Mrs. Selwyn decline her civility.

Oh that the first meeting were over!-or that I could quit Bristol without seeing him!-inexpressibly do I dread an interview! Should the same impertinent freedom be expressed by his looks, which dictated this cruel letter, I shall not know how to endure either him or myself. Had I but returned it, I should be easier, because my sentiments of it would then be known to him; but now, he can only gather them from my behaviour; and I tremble lest he should mistake my indignation for confusion!-lest he should misconstrue my reserve into embarrassment!-for how, my dearest Sir, how shall I be able totally to divest myself of the respect with which I have been used to think of him?-the pleasure with which I have been used to see him?

Surely he, as well as I, must recollect the  the moment of our meeting; and he will, probably, mean to gather my thoughts of it from my looks;-oh that they could but convey to him my real detestation of impertinence and vanity! then would he see how much he had mistaken my disposition when he imagined them my due.

There was a time when the very idea that such a man as Lord Merton should ever be connected with Lord Orville would have both surprised and shocked me; and even yet I am pleased to hear of his repugnance to the marriage.

But how strange, that a man of so abandoned a character should be the choice of a sister of Lord Orville! and how strange, that, almost at the moment of the union, he should be so importunate in gallantry to another woman! What a world is this we live in! how corrupt! how degenerate! well might I be contented to see no more of it! If I find that the eyes of Lord Orville agree with his pen,-I shall then think, that of all mankind, the only virtuous individual resides at Berry Hill.

.

EVELINA IN CONTINUATION.  Bristol Hotwells, Sept. 16th.');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: An article', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Some other article', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: The dairy of Jenz', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Four Horsemen met at a crossroads', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: If you can get it to work', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Somehow this is just not as fun', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: As getting a script to programmatically', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Create your testdata', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: I would\'ve loved to make it work', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: But it appear that is not going to happen', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Oh well, I\'ll just have to add this', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Manually to the SQL file', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: This is a fucking travesty', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: But since there\'s not much time left', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: We\'ll have to just make it work', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: And that involves me writing all these manually', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: I\'ve now lost count of how many of these I\'m making', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: I guess it works out in the end, maybe', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: There\'s just so many', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: And I should make them unique', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Making them unique is hard work', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: And it also precludes me', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: From making sure that the', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Dates are better created', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Would\'ve loved to use some JS', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Scripting to make it all', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Get added for up to 1000', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Articles, but that just didn\'t happen', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: I\'m coming up on the end of how many I\'ve', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: copied the SQL inserting', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: For. Let\'s hope it all works', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: Otherwise I\'m Fucked', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: And none of us wants that to happen', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: This is the penultimate article I\'m writing', '');

insert into Backend.articles (title, text)
VALUES ('LitIpsum: This is the ultimate article', '');

select @articlenumbers:= LAST_INSERT_ID();

#
#	USERS
#



insert into Backend.users (username, password)
values ('test3', 'test3');

insert into Backend.users (username, password)
values ('test4', 'test4');

insert into Backend.users (username, password)
values ('test5', 'test5');

insert into Backend.users (username, password)
values ('test6', 'test6');

insert into Backend.users (username, password)
values ('test7', 'test7');

insert into Backend.users (username, password)
values ('test8', 'test8');

insert into Backend.users (username, password)
values ('test9', 'test9');


select @usernumbers:= LAST_INSERT_ID();

#
#	AUTHORS
#

insert into Backend.authors (firstName, lastName)
values ('testAuthorFirstName1', 'testAuthorLastName1');

insert into Backend.authors (firstName, lastName)
values ('testAuthorFirstName2', 'testAuthorLastName2');

insert into Backend.authors (firstName, lastName)
values ('testAuthorFirstName3', 'testAuthorLastName3');

insert into Backend.authors (firstName, lastName)
values ('Just', 'Have');

insert into Backend.authors (firstName, lastName)
values ('To', 'Add');

insert into Backend.authors (firstName, lastName)
values ('A', 'Lot');

insert into Backend.authors (firstName, lastName)
values ('Of', 'Authors');

insert into Backend.authors (firstName, lastName)
values ('Whose', 'Names');

insert into Backend.authors (firstName, lastName)
values ('I', 'Don\'t');

insert into Backend.authors (firstName, lastName)
values ('Care', 'About');

insert into Backend.authors (firstName, lastName)
values ('Which', 'Is');

insert into Backend.authors (firstName, lastName)
values ('Fine', 'But');

insert into Backend.authors (firstName, lastName)
values ('It', 'Shouldn\'t');

insert into Backend.authors (firstName, lastName)
values ('Be', 'Such');

insert into Backend.authors (firstName, lastName)
values ('A', 'Fucking');

insert into Backend.authors (firstName, lastName)
values ('Problem', 'With');

insert into Backend.authors (firstName, lastName)
values ('Making', 'Them');

insert into Backend.authors (firstName, lastName)
values ('All', 'That');

insert into Backend.authors (firstName, lastName)
values ('Different', 'If');

insert into Backend.authors (firstName, lastName)
values ('We', 'Couldvejustmadeascriptforit');

select @authornumbers:= LAST_INSERT_ID();

#
#	TAGS
#



insert into Backend.tags (tag)
values ('TestTag3');

insert into Backend.tags (tag)
values ('TestTag4');

insert into Backend.tags (tag)
values ('TestTag5');

insert into Backend.tags (tag)
values ('TestTag6');

insert into Backend.tags (tag)
values ('TestTag7');

insert into Backend.tags (tag)
values ('TestTag8');

select @tagnumbers:= LAST_INSERT_ID();

#
#	COMMENTS
#

# TODO ADD STORED PROCEDURE TO CONNECT COMMENTS TO ARTICLES

insert into Backend.comments (userID, articleID, text)
values ((Select id from Backend.users where username = 'test'),
		(select min(id) from Backend.articles where title = 'LitIpsum: Test-article_1'),
		'Test-comment');

insert into Backend.comments (userID, articleID, text)
values ((Select id from Backend.users where username = 'test2'),
		(select min(id) from Backend.articles where title = 'LitIpsum: Test-article2'),
		'Test-comment2');

insert into Backend.comments (userID, articleID, text)
values ((Select id from Backend.users where username = 'test'),
		(select min(id) from Backend.articles where title = 'LitIpsum: Test-article2'),
		'Test-comment3');

insert into Backend.comments (userID, articleID, text)
values ((Select id from Backend.users where username = 'test'),
		(select min(id) from Backend.articles where title = 'LitIpsum: Test-article_1'),
		'Test-comment4');

insert into Backend.articleTags (articleID, tagID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article_1'),
		(select min(id) from Backend.tags where tag = 'TestTag'));

insert into Backend.articleTags (articleID, tagID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article_1'),
		(select min(id) from Backend.tags where tag = 'TestTag2'));

insert into Backend.articleTags (articleID, tagID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article2'),
		(select min(id) from Backend.tags where tag = 'TestTag'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article_1'),
		(select min(id) from Backend.authors where firstName = 'testAuthorFirstName1'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article2'),
		(select min(id) from Backend.authors where firstName = 'testAuthorFirstName2'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article3'),
		(select min(id) from Backend.authors where firstName = 'testAuthorFirstName3'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article2'),
		(select min(id) from Backend.authors where firstName = 'testAuthorFirstName4'));

insert into Backend.articleAuthors (articleID, authorID)
values ((select min(id) from Backend.articles where title = 'LitIpsum: Test-article4'),
		(select min(id) from Backend.authors where firstName = 'testAuthorFirstName5'));

insert into Backend.savedArticles (userID, articleID)
values ((Select id from Backend.users where username = 'test'),
		(select min(id) from Backend.articles where title = 'LitIpsum: Test-article_1'));

insert into Backend.savedArticles (userID, articleID)
values ((Select id from Backend.users where username = 'test2'),
		(select min(id) from Backend.articles where title = 'LitIpsum: Test-article_1'));

insert into Backend.savedArticles (userID, articleID)
values ((Select id from Backend.users where username = 'test'),
		(select min(id) from Backend.articles where title = 'LitIpsum: Test-article2'));
