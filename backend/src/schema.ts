import {
	GraphQLInt,
	GraphQLObjectType,
	GraphQLSchema,
	GraphQLList, GraphQLString, GraphQLBoolean,
} from 'graphql';
import db from './db';
import {Op} from "sequelize";

const user: GraphQLObjectType = new GraphQLObjectType({
	name: 'user',
	description: 'This is a user',
	fields: () => {
		return {
			id:
				{
					type: GraphQLInt,
					resolve(user) {
						return user.id;
					}
				},
			username:
				{
					type: GraphQLString,
					resolve(user) {
						return user.username;
					}
				},
			comments:
				{
					type: new GraphQLList(comment),
					resolve(user) {
						return db.models.comment.findAll({where: {userID: user.id}})
					}
				},
			savedArticles:
				{
					type: new GraphQLList(savedArticles),
					resolve(user) {
						return db.models.savedArticles.findAll({where: {userID: user.id}});
					}
				}
		}
	}
});

const article: GraphQLObjectType = new GraphQLObjectType({
	name: 'article',
	description: 'This is an article',
	fields: () => {
		return {
			id:
				{
					type: GraphQLInt,
					resolve(article) {
						return article.id;
					}
				},
			timestamp:
				{
					type: GraphQLString,
					resolve(article) {
						return article.timestamp.toISOString();
					}
				},
			title:
				{
					type: GraphQLString,
					resolve(article) {
						return article.title;
					}
				},
			text:
				{
					type: GraphQLString,
					resolve(article) {
						return article.text;
					}
				},
			authorConnection:
				{
					type: GraphQLList(articleAuthor),
					resolve(article) {
						return db.models.articleAuthors.findAll({where: {articleID: article.id}})
					}
				},
			tagConnection:
				{
					type: GraphQLList(articleTags),
					resolve(article) {
						return db.models.articleTags.findAll({where: {articleID: article.id}})
					}
				},
			commentConnection:
				{
					type: GraphQLList(comment),
					resolve(article) {
						return db.models.comment.findAll({where: {articleID: article.id}})
					}
				},
			savedByUserConnection:
				{
					type: GraphQLList(savedArticles),
					resolve(article) {
						return db.models.savedArticles.findAll({where: {articleID: article.id}})
					}
				},
		}
	}
});

const author: GraphQLObjectType = new GraphQLObjectType({
	name: 'author',
	description: 'This is an author, using a first and last name',
	fields: () => {
		return {
			id:
				{
					type: GraphQLInt,
					resolve(author) {
						return author.id;
					}
				},
			firstName:
				{
					type: GraphQLString,
					resolve(author) {
						return author.firstName;
					}
				},
			lastName:
				{
					type: GraphQLString,
					resolve(author) {
						return author.lastName;
					}
				},
			articleConnection:
				{
					type: new GraphQLList(articleAuthor),
					resolve(author) {
						return db.models.articleAuthors.findAll({where: {authorID: author.id}})
					}
				}
		}
	}
});

const tag: GraphQLObjectType = new GraphQLObjectType({
	name: 'tag',
	description: 'This is a tag which will help define what articles are about',
	fields: () => {
		return {
			id:
				{
					type: GraphQLInt,
					resolve(tag) {
						return tag.id;
					}
				},
			tag:
				{
					type: GraphQLString,
					resolve(tag) {
						return tag.tag;
					}
				},
			articleConnection:
				{
					type: GraphQLList(articleTags),
					resolve(tag) {
						return db.models.articleTags.findAll({where: {tagID: tag.id}})
					}
				},
		}
	}
});

const comment: GraphQLObjectType = new GraphQLObjectType({
	name: 'comment',
	description: 'This is a comment which a [user] has made on an [article]',
	fields: () => {
		return {
			id:
				{
					type: GraphQLInt,
					resolve(comment) {
						return comment.id;
					}
				},
			userID:
				{
					type: GraphQLInt,
					resolve(comment) {
						return comment.userID;
					}
				},
			articleID:
				{
					type: GraphQLInt,
					resolve(comment) {
						return comment.articleID;
					}
				},
			timestamp:
				{
					type: GraphQLString,
					resolve(comment) {
						return comment.timestamp.toISOString();
					}
				},
			text:
				{
					type: GraphQLString,
					resolve(comment) {
						return comment.text;
					}
				},
			user:
				{
					type: GraphQLList(user),
					resolve(comment) {
						return db.models.user.findAll({where: {id: comment.userID}})
					}
				},
			article:
				{
					type: GraphQLList(article),
					resolve(comment) {
						return db.models.article.findAll({where: {id: comment.articleID}})
					}
				},
		}
	}
});

const articleTags: GraphQLObjectType = new GraphQLObjectType({
	name: 'articleTags',
	description: 'A binding which connects articles with the tags they fit',
	fields: () => {
		return {
			id:
				{
					type: GraphQLInt,
					resolve(articleTags) {
						return articleTags.id;
					}
				},
			articleID:
				{
					type: GraphQLInt,
					resolve(articleTags) {
						return articleTags.articleID;
					}
				},
			tagID:
				{
					type: GraphQLInt,
					resolve(articleTags) {
						return articleTags.tagID;
					}
				},
			article:
				{
					type: GraphQLList(article),
					args:
							{
								id: {type: GraphQLInt},
								title: {type: GraphQLString},
								alphabetically: {type: GraphQLBoolean},
								limit: {type: GraphQLInt},
								page: {type: GraphQLInt},
							},
					resolve(articleTags, args) {
						return getArticlesFromDatabase(args)
					}
				},
			tag:
				{
					type: GraphQLList(tag),
					resolve(articleTags) {
						return db.models.tag.findAll({where: {id: articleTags.tagID}});
					}
				},
		}
	}
});

const articleAuthor: GraphQLObjectType = new GraphQLObjectType({
	name: 'articleAuthor',
	description: 'A connection which binds authors and articles together',
	fields: () => {
		return {
			id:
				{
					type: GraphQLInt,
					resolve(articleAuthor) {
						return articleAuthor.id;
					}
				},
			articleID:
				{
					type: GraphQLInt,
					resolve(articleAuthor) {
						return articleAuthor.articleID;
					}
				},
			authorID:
				{
					type: GraphQLInt,
					resolve(articleAuthor) {
						return articleAuthor.authorID;
					}
				},
			article:
				{
					type: GraphQLList(article),
					args:
						{
							id: {type: GraphQLInt},
							title: {type: GraphQLString},
							alphabetically: {type: GraphQLBoolean},
							limit: {type: GraphQLInt},
							page: {type: GraphQLInt},
						},
					resolve(articleAuthor, args) {
						return db.models.article.findAll({where: {id: articleAuthor.articleID}})
					}
				},
			author:
				{
					type: GraphQLList(author),
					resolve(articleAuthor) {
						return db.models.author.findAll({where: {id: articleAuthor.authorID}})
					}
				},
		}
	}
});

const savedArticles: GraphQLObjectType = new GraphQLObjectType({
	name: 'savedArticles',
	description: 'A connection which keeps track of what [article]s a [user] has saved',
	fields: () => {
		return {
			id:
				{
					type: GraphQLInt,
					resolve(savedArticles) {
						return savedArticles.id;
					}
				},
			userID:
				{
					type: GraphQLInt,
					resolve(savedArticles) {
						return savedArticles.userID;
					}
				},
			articleID:
				{
					type: GraphQLInt,
					resolve(savedArticles) {
						return savedArticles.articleID;
					}
				},
			article:
				{
					type: new GraphQLList(article),
					resolve(savedArticles) {
						return db.models.article.findAll({where: {id: savedArticles.articleID}})
					}
				},
			user:
				{
					type: new GraphQLList(user),
					resolve(savedArticles) {
						return db.models.user.findAll({where: {id: savedArticles.userID}})
					}
				},
		}
	}
});


/*const article = new GraphQLObjectType({
	name: '',
	description: '',
	fields: () => {
		return {

		}
	}
});*/

/**
 * The Query returns fields which themselves should return JSON objects
 *
 * Fields should be defined like the following example:
 * @example
 * ExampleField:
 *    {
 * 	  type: new GraphQLList(example)
 * 	  args:
 * 	    {
 *	      id: {type: GraphQLInt},
 * 	    }
 * 	    resolve(root, args) {
 * 	      return db.models.example.findAll({where: args})
 * 	    }
 * 	}
 *
 * The resolve function can involve a lot of code for complicated queries.
 * You can even use raw SQL-queries which Sequelize which is known as "db" here.
 *
 * For conditionally adding options, look at the article field in this implementation.
 *
 * The args are passed through the resolve function as a JSON object, which can be
 * accessed by args[<key>], and conditionally extracted with if (args[<key>]){}, see
 * the article field.
 *
 * @type {GraphQLObjectType<any, any>}
 */
const Query = new GraphQLObjectType(
	{
		name: 'Query',
		description: 'This is a root query',
		fields: () => {
			return {
				user:
					{
						type: new GraphQLList(user),
						args:
							{
								id: {type: GraphQLInt},
								username: {type: GraphQLString},
								password: {type: GraphQLString},
							},
						resolve(root, args) {
							return db.models.user.findAll({where: args})
						}
					},
				article:
					{
						type: new GraphQLList(article),
						args:
							{
								id: {type: GraphQLInt},
								title: {type: GraphQLString},
								alphabetically: {type: GraphQLBoolean},
								limit: {type: GraphQLInt},
								page: {type: GraphQLInt},
							},
						resolve(root, args) {
							return getArticlesFromDatabase(args);
						}
					},
				author:
					{
						type: new GraphQLList(author),
						args:
							{
								id: {type: GraphQLInt},
								firstName: {type: GraphQLString},
								lastName: {type: GraphQLString},
							},
						resolve(root, args) {
							return db.models.author.findAll({where: args})
						}
					},
				tag:
					{
						type: new GraphQLList(tag),
						args:
							{
								id: {type: GraphQLInt},
								tag: {type: GraphQLString},
							},
						resolve(root, args) {
							return db.models.tag.findAll({where: args})
						}
					},
				comment:
					{
						type: new GraphQLList(comment),
						args:
							{
								id: {type: GraphQLInt},
								userID: {type: GraphQLInt},
								articleID: {type: GraphQLInt},
							},
						resolve(root, args) {
							return db.models.comment.findAll({where: args})
						}
					},
				articleTags:
					{
						type: new GraphQLList(articleTags),
						args:
							{
								id: {type: GraphQLInt},
								articleID: {type: GraphQLInt},
								tagID: {type: GraphQLInt},
							},
						resolve(root, args) {
							return db.models.articleTags.findAll({where: args})
						}
					},
				articleAuthors:
					{
						type: new GraphQLList(articleAuthor),
						args:
							{
								id: {type: GraphQLInt},
								articleID: {type: GraphQLInt},
								authorID: {type: GraphQLInt},
							},
						resolve(root, args) {
							return db.models.articleAuthors.findAll({where: args})
						}
					},
				savedArticles:
					{
						type: new GraphQLList(savedArticles),
						args:
							{
								id: {type: GraphQLInt},
								userID: {type: GraphQLInt},
								articleID: {type: GraphQLInt},
							},
						resolve(root, args) {
							return db.models.savedArticles.findAll({where: args})
						}
					},
			}
		}
	}
);


const Mutation = new GraphQLObjectType({
	name: "Mutation",
	description: "This is the Root mutation query",
	fields:
		{
			createComment:
				{
					type: comment,
					args:
						{
							userID: {type: GraphQLInt},
							articleID: {type: GraphQLInt},
							text: {type: GraphQLString},
							timestamp: {type: GraphQLInt}
						},
					resolve(root, args) {
						//let result =
						return db.models.comment.create(args);
					}
				},
			createArticle:
				{
					type: article,
					args:
						{
							timestamp: {type: GraphQLInt},
							title: {type: GraphQLString},
							text: {type: GraphQLString},
						},
					resolve(root, args) {
						return db.models.article.create(args)
					}
				},
			createSavedArticles:
				{
					type: savedArticles,
					args:
						{
							userID: {type: GraphQLInt},
							articleID: {type: GraphQLInt},
						},
					resolve(root, args) {
						return db.models.savedArticles.create(args)
					}
				},
			createUser:
				{
					type: user,
					args:
						{
							username: {type: GraphQLString},
							password: {type: GraphQLString},
						},
					resolve(root, args) {
						return db.models.user.create(args)
					}
				},
			createAuthor:
				{
					type: author,
					args:
						{
							firstName: {type: GraphQLString},
							lastName: {type: GraphQLString},
						},
					resolve(root, args) {
						return db.models.author.create(args)
					}
				},
			createTag:
				{
					type: tag,
					args:
						{
							tag: {type: GraphQLString},
						},
					resolve(root, args) {
						return db.models.tag.create(args)
					}
				},
			createArticleTags:
				{
					type: articleTags,
					args:
						{
							articleID: {type: GraphQLInt},
							tagID: {type: GraphQLInt},
						},
					resolve(root, args) {
						return db.models.articleTags.create(args)
					}
				},
			createArticleAuthor:
				{
					type: articleAuthor,
					args:
						{
							articleID: {type: GraphQLInt},
							authorID: {type: GraphQLInt},
						},
					resolve(root, args) {
						return db.models.articleAuthors.create(args)
					}
				},
			createArticleAndAuthorWithConnection:
				{
					type: article,
					args:
						{
							timestamp: {type: GraphQLInt},
							title: {type: GraphQLString},
							text: {type: GraphQLString},
							firstName: {type: GraphQLString},
							lastName: {type: GraphQLString},
						},
					resolve(root, args) {
						return db.models.article.create(args).then(async function (value: any) {
							await db.models.author.create(args).then(async function (result: any) {
								await db.models.articleAuthors.create({
									authorID: result["id"],
									articleID: value["id"]
								}).then()
							});
							return Promise.resolve(value)
						})
					}
				}
		}
});


const Schema = new GraphQLSchema(
	{
		query: Query,
		mutation: Mutation,
	}
);

export default Schema;


/*<>:
					{
						type: new GraphQLList(<>),
						args:
							{
								id:
									{
										type: GraphQLInt
									},
								<>:
									{
										type: GraphQLString
									},
							},
						resolve(root, args) {
							return db.models.<>.findAll({where: args})
						}
					},*/


// Helper functions

/**
 * This wil return the number given to it, or 10 if value is undefined
 *
 * @param {number} value a number, or undefined if not provided by query
 * @returns {number}
 */
function getNumberTenOrArgNumber(value: number): number {
	return getDefaultValueOrArgNumber(10, value);
}

function getNumberZeroOrArgNumber(value: number): number {
	return getDefaultValueOrArgNumber(0, value);
}

function getDefaultValueOrArgNumber(defaultValue: number, value: number): number {
	if (!value) {
		return defaultValue;
	}
	return value;
}

function getArticlesFromDatabase(args: any) {
	let whereStatement = {};
	let orderStatement: String[][] = [];
	let limit: number = getNumberTenOrArgNumber(args.limit);
	let page: number = getNumberZeroOrArgNumber(args.page);
	if (args["alphabetically"]) {
		orderStatement.push(["title", "ASC"]);
	}
	if (args["title"]) {
		// The (whereStatement as any) is a very dirty hack for typed language to be able
		// treat it as a JSON object and add them
		(whereStatement as any).title = {[Op.like]: '%' + args["title"] + '%'};
	}
	if (args["id"]) {
		(whereStatement as any).id = {[Op.eq]: args["id"]};
	}
	return db.models.article.findAll({
		where: whereStatement,
		order: orderStatement,
		limit: limit,
		offset: limit * page,
	})
}
