drop schema if exists Backend;
create schema Backend;
CREATE TABLE `Backend`.`authors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
CREATE TABLE `Backend`.`articles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` VARCHAR(100) NOT NULL,
  `text` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
CREATE TABLE `Backend`.`tags` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tag` VARCHAR(45) NOT NULL UNIQUE,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
CREATE TABLE `Backend`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL UNIQUE,
  `password` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
CREATE TABLE `Backend`.`articleAuthors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `articleID` INT NOT NULL,
  `authorID` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `articleID_idx` (`articleID` ASC),
  INDEX `authorID_idx` (`authorID` ASC),
  UNIQUE INDEX `uniqueArticleAuthorRelationship` (`articleID` ASC, `authorID` ASC),
  CONSTRAINT `articleAuthorsArticleID`
    FOREIGN KEY (`articleID`)
    REFERENCES `Backend`.`articles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `articleAuthorsAuthorID`
    FOREIGN KEY (`authorID`)
    REFERENCES `Backend`.`authors` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE);
CREATE TABLE `Backend`.`savedArticles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userID` INT NOT NULL,
  `articleID` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `userID_idx` (`userID` ASC),
  INDEX `articleID_idx` (`articleID` ASC),
  UNIQUE INDEX `uniqueSavedArticleRelationship` (`userID` ASC, `articleID` ASC),
  CONSTRAINT `savedArticlesUserID`
    FOREIGN KEY (`userID`)
    REFERENCES `Backend`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `savedArticlesArticleID`
    FOREIGN KEY (`articleID`)
    REFERENCES `Backend`.`articles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
CREATE TABLE `Backend`.`articleTags` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `articleID` INT NOT NULL,
  `tagID` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `articleTagTagID_idx` (`tagID` ASC),
  INDEX `articleTagArticleID_idx` (`articleID` ASC),
  UNIQUE INDEX `uniqueArticleTagRelationship` (`articleID` ASC, `tagID` ASC),
  CONSTRAINT `articleTagsTagID`
    FOREIGN KEY (`tagID`)
    REFERENCES `Backend`.`tags` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `articleTagsArticleID`
    FOREIGN KEY (`articleID`)
    REFERENCES `Backend`.`articles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
CREATE TABLE `Backend`.`comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userID` INT NULL,
  `articleID` INT NOT NULL,
  `timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `commentsUserID_idx` (`userID` ASC),
  INDEX `commentsArticleID_idx` (`articleID` ASC),
  CONSTRAINT `commentsUserID`
    FOREIGN KEY (`userID`)
    REFERENCES `Backend`.`users` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `commentsArticleID`
    FOREIGN KEY (`articleID`)
    REFERENCES `Backend`.`articles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
