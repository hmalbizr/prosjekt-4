# Useful links by area

## GraphQL

[Querying Relational Data with GraphQL](https://medium.com/scaphold/querying-relational-data-with-graphql-ddd098f0555d)

[Understanding Pagination](https://www.apollographql.com/blog/understanding-pagination-rest-graphql-and-relay-b10f835549e7#.lor7ia8hk)

[Explaining GraphQL Connections](https://www.apollographql.com/blog/explaining-graphql-connections-c48b7c3d6976/)

[Building a GraphQL Server with Node.js and Express](https://softchris.github.io/pages/graphql-express.html)

[GraphQL Introspection](https://graphql.org/learn/introspection/)  
How to ask the GraphQL endpoint for what it supports

### Videos

[Build GraphQL server for Node.js, using MySQL](https://discordapp.com/channels/764223833019645952/764223833019645955/769909543273758730)

## Sequelize

[For adding to an object for [where:]](https://stackoverflow.com/questions/18083389/ignore-typescript-errors-property-does-not-exist-on-value-of-type)

[Sequelize conditional parameters](https://stackoverflow.com/questions/42236837/how-to-perform-a-search-with-conditional-where-parameters-using-sequelize)

[Sequelize with RAW SQL](https://dev.to/joannatomassoni/a-deeper-dive-into-sequelize-queries-dynamic-field-names-the-op-object-and-raw-sql-insertions-2pkb)

[Sequelize findAll with sort order](https://stackoverflow.com/questions/36259532/sequelize-findall-sort-order-in-nodejs)

## Axios

[Speaking to a GraphQL server with Axios](https://medium.com/@stubailo/how-to-call-a-graphql-server-with-axios-337a94ad6cf9)

[Article on Axios](https://blog.logrocket.com/how-to-make-http-requests-like-a-pro-with-axios/)

[Axios GitHub, with sort of documentation](https://github.com/axios/axios)

[NPM page for Axios](https://www.npmjs.com/package/axios)

## Markdown

[Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
