import React from 'react';
import './App.css';
import Navbar from './components/Navbar/navbar';
import {useSelector} from "react-redux";
import {RootState} from "./reducers/rootReducer";
import Article from "./components/Article/Article";
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {rootReducer} from "./reducers/rootReducer";
import {Provider} from 'react-redux';


declare global {
	interface Window { Cypress: any; store:any;}
}


export const AppWrapper = () => {
	const myStore = createStore(rootReducer, applyMiddleware(thunk));
	console.log(myStore.getState())

	if (window.Cypress) {
		window.store = myStore;
	}

	return (
		<Provider store={myStore}>
			<App />
		</Provider>
	)
}


export function App() {
	const search_results = useSelector((state: RootState) => state.search)
  return (
		<div className={"container"}>
			<div className={"header"}>
				<Navbar/>
			</div>


			<div className={"result-container"}>
				<Article searchQuery={search_results}/>
			</div>
			<div className={"footer"}>
				Articles
			</div>
		</div>
	);
}

export default App;
