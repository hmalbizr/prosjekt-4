import {combineReducers} from 'redux';
import searchReduser from "./SearchReducer";

export const rootReducer = combineReducers({
    search: searchReduser
});

export type RootState = ReturnType<typeof rootReducer>
