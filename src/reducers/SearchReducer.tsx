export interface Action {
	type: string,
	id: number,
	currentState: boolean,
	payload: string,
	page: number,
	page_start: number
}

let sort = false;
let filterTag = false;
let filterAuthor = false;
let searchValue = "";
let filterType = "";
let page = 0;

const checkTypeToChangePage = (page: number, searchValue:string, filterType: string) => {
	let query;
	if(sort)
		query = '{article(alphabetically: true, limit: 4, page: '+page+') {authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}';
	else if(filterAuthor){
		const res = filterType.split(" ");
		query = '{author(firstName:"' + res[0] + '", lastName:"' + res[1] + '") {articleConnection {article (limit: 4, page: '+page+'){authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}}}';

	}
	else if(filterTag)
		query = '{tag(tag:"' + filterType + '") {articleConnection {article (limit: 4, page: '+page+') {authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}}}';

	else
		query = '{article(title: "'+ searchValue +'", limit: 4, page: '+page+'){authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}';
	return query
}

const searchReducer = (state:'', action: Action) => {
	let query = '{article(limit: 4, page: '+page+'){authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}';
	switch (action.type) {
		case "Search":
			sort = false;
			searchValue = action.payload;
			query = '{article(title: "' + action.payload + '", limit: 4, page: '+page+') {authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}';
			page= 0;
			filterTag = false;
			filterAuthor = false;
			return {query: query, page: page};

		case "FilterAuthor":
			sort = false;
			page = 0;
			filterType = action.payload;
			filterAuthor = true;
			filterTag = false;
			const res = action.payload.split(" ");
			query = '{author(firstName:"' + res[0] + '", lastName:"' + res[1] + '") {articleConnection {article (limit: 4, page: '+page+'){authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}}}';
			return {query: query, page: page};

		case "FilterTag":
			sort = false;
			page = 0;
			filterType = action.payload;
			filterTag = true;
			filterAuthor = false;
			query = '{tag(tag:"' + action.payload + '") {articleConnection {article (limit: 4, page: '+page+') {authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}}}';
			return {query: query, page: page};

		case "Sort":
			page = 0;
			sort = true;
			query = '{article(title: "'+ searchValue +'", alphabetically: true, limit: 4, page: '+page+') {authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}';
			return {query: query, page: page};

		case "AddPage":
			page = page + 1;
			query = checkTypeToChangePage(page,searchValue , filterType)
			return {page: page,
				query: query
			};

		case "PrevPage":
			if(page > 0)
				page = page - 1;
			query = checkTypeToChangePage(page, searchValue, filterType)
			return {page: page,
				query: query
			};

		case "Update":
			query = checkTypeToChangePage(page, searchValue, filterType)
			return {query: query, page: page};
		default:
			return {query: query, page: page};
	}
};

export default searchReducer;
