export const Search = (input: string="") => {
	return {
		type: 'Search',
		payload: input,
	};
}


export const FilterTag = (input: string="") => {
	return {
		type: 'FilterTag',
		payload: input,
	};
}

export const FilterAuthor = (input: string="") => {
	return {
		type: 'FilterAuthor',
		payload: input,
	};
}

export const Sort = (input: string="") => {
	return {
		type: 'Sort',
		payload: input,
	};
}


export const AddPage = () => {
	return {
		type: 'AddPage',
	};
}

export const PrevPage = () => {
	return {
		type: 'PrevPage',
	};
}

export const Update = () => {
	return {
		type: 'Update',
	};
}
