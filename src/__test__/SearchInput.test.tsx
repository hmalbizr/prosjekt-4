import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom/extend-expect';
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import {rootReducer} from "../reducers/rootReducer";
import thunk from "redux-thunk";
import {App} from "./../App";

const myStore = createStore(rootReducer, applyMiddleware(thunk));

afterEach(() => {
	jest.runOnlyPendingTimers();
})

beforeEach(() => {
	jest.useFakeTimers();
})

it('Search input', () => {
	const { getByPlaceholderText, getByText, getByTestId, asFragment } = render(<><Provider store={myStore}><App /></Provider></>);
	userEvent.type(getByPlaceholderText("Search on title"), 'love');
	getByText("Search").click();
	//I get an error when calling userEvent.click
	//userEvent.click(getByText("Search"));
	expect(getByText("Loading ..")).toBeInTheDocument();
	expect(asFragment()).toMatchSnapshot();
	expect(getByTestId("search-input")).toHaveValue("love");
	if(myStore.getState().search){
		expect(myStore.getState().search).toEqual({
			query: "{article(title: \"love\", limit: 4, page: 0) {authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}",
				page: 0})
	}
	else
		fail("No Search in store")

});

