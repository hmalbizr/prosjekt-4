import React from 'react';
import { render, cleanup, fireEvent, waitForElement } from '@testing-library/react';
import ReactDOM from 'react-dom';
import '@testing-library/jest-dom/extend-expect';
import Navbar from "./../components/Navbar/navbar";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import {rootReducer} from "../reducers/rootReducer";
import thunk from "redux-thunk";

const myStore = createStore(rootReducer, applyMiddleware(thunk));

afterEach(cleanup)

it('Check the presentation of Header', () => {
	const { asFragment } = render(<><Provider store={myStore}><Navbar /></Provider></>);
	expect(asFragment()).toMatchSnapshot();

});


it('Filter menu', () => {
	const { getByTestId } = render(<><Provider store={myStore}><Navbar /></Provider></>);
	fireEvent.mouseEnter(getByTestId("filter-tag"));
	expect(getByTestId("filter-tag-dropdown")).toBeVisible();
	fireEvent.mouseEnter(getByTestId("filter-author"));
	expect(getByTestId("filter-author-dropdown")).toBeVisible();

});
