import {Search} from "../../actions/Actions";
import {connect} from "react-redux";
import React from "react";

interface SuggestionsProps{
	results: Array<any>,
	Search: any
	click: boolean
}

interface SuggestionsState{
	click: boolean
}

/** Suggestions: take a list of titles from SearchInput as props and use them to be shown on the screen
*
 */
class Suggestions extends React.Component<SuggestionsProps, SuggestionsState> {
	click:boolean;
	constructor(props: SuggestionsProps) {
		super(props);
		this.click = this.props.click
		this.state = {
			click: this.props.click
		};

	}

	render() {
		return(<ul className={this.click ? "hidden" : "recommendations"}>{
			this.click = false}
			{this.props.results.map((article:any, index:number) => (
				<li key={index} className={'suggestion'} onClick={() => {
					this.click = true
					this.props.Search(article.title)
				}}>
					{article.title}
				</li>
			))
		}</ul>)
	}


}

const mapStateToProps = (state:any) => {
	return {query: state.search.query}
};

const mapDispatchToProps = () => {
	return {
		Search
	};
};

export default connect(mapStateToProps, mapDispatchToProps())(Suggestions);
