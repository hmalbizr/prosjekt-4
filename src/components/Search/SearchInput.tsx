import React from "react";
import { connect} from "react-redux";
import { Search } from "./../../actions/Actions";
import Suggestions from "./Suggestions";
import './../Navbar/navbar.css';

const searchIcon = require("./search-icon.png");

interface SearchProps{
	Search: any,
	query: any
}

interface SearchState{
	value: string,
	results: Array<any>
}

/**
 * SearchInput: Takes the search input from user and send it to redux reducers to use it in database searching.
 * Updates the list of suggestions
 * state.value is the input.
 */
class SearchInput extends React.Component<SearchProps , SearchState> {
	clickSearch: boolean;
	constructor(props: SearchProps) {
		super(props);
		this.state = {
			value: '',
			results: [],
		};
		this.clickSearch = true
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);

	}

	getInfo(input: string) {
		fetch('/graphql', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
			},
			body: JSON.stringify({query: '{article(title:"' + input + '" limit: 10, page: 0){title}}'})
		})
			.then(response => {
				if (response.ok) {
					return response.json();
				} else {
					console.log("Invalid response from API server.");
				}
			}).then(data => {
			if (data === undefined || data.data[Object.keys(data.data)[0]].length === 0) {
				this.setState({results: []})
			} else {
				this.setState({results: data.data.article})
			}
		});
	}


	handleChange(event: any) {
		this.setState({value: event.target.value});
		if(event.target.value.length >= 1)
			this.getInfo(event.target.value);
		else this.setState({results: []})
	}

	handleSubmit(event: any) {
		this.props.Search(this.state.value)
		this.clickSearch = true;
		event.preventDefault();
	}


	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<li className={"menu-item menu-item-label"}>
					<label htmlFor={"search"} className={"search-label"} data-testid={"search-label"} onClick={() => {
						this.props.Search(this.state.value)
						this.clickSearch = true;
					}}>Search</label>
					<img src={searchIcon} alt={"search-icon"} className={"search-icon"} onClick={() => {
						this.props.Search(this.state.value);
						this.clickSearch = true;
					}}/>
				</li>
				<li className={"menu-item menu-item-input"}>
					<input type={"text"} id={"search-input"} name={"search"} value={this.state.value} onChange={(event) => {
						this.handleChange(event);
					}}
						   data-testid={"search-input"} placeholder={"Search on title"}/>
				</li>
				{!this.clickSearch? <Suggestions results={this.state.results} click={!this.clickSearch}/>: ""}
				{this.clickSearch = false}
			</form>

		);
	}
}

const mapStateToProps = (state:any) => {
	return {query: state.search.query}
};

const mapDispatchToProps = () => {
	return {
		Search
	};
};

export default connect(mapStateToProps, mapDispatchToProps())(SearchInput);


