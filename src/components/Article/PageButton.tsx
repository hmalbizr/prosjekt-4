import React from "react";
import { connect } from "react-redux";
import { AddPage, PrevPage } from "./../../actions/Actions";
import './Articles.css';

interface PageProps{
	page: number,
	AddPage: any,
	PrevPage: any
}

interface PageState{
	page_num: number
}
/**PageButton: Change pages to print articles dynamically by clicking on this button.
 */
class PageButton extends React.Component<PageProps, PageState> {
	constructor(props: PageProps) {
		super(props);
		this.state = {page_num:0};
	}


	IncrementPage() {
		if (this.props.page) {
			this.props.AddPage()
			this.setState({page_num: this.props.page})
		}
		else {
			this.props.AddPage(2);
			this.setState({page_num: 1})
		}
	}

	DecrementPage() {
		if (this.props.page) {
			this.props.PrevPage()
			this.setState({page_num: this.props.page})
		}
		else {
			this.props.PrevPage();
			this.setState({page_num: 1})
		}
	}


	render() {
		return (<div>
				<button className={"page-number"} onClick={()=>{this.DecrementPage()}}> Previous Page</button>
				<button className={"page-number"} onClick={()=>{this.IncrementPage()}}> Next Page</button>
			</div>)
	}
}

const mapStateToProps = (state:any) => {
	return {page: state.search.page}
};

const mapDispatchToProps = () => {
	return {
		AddPage: AddPage,
		PrevPage: PrevPage
	};
};

export default connect(mapStateToProps, mapDispatchToProps())(PageButton);
