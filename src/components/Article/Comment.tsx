import React from "react";
import './Articles.css'
import {Update} from "../../actions/Actions";
import {connect} from "react-redux";

interface CommentProps{
	Update: any,
	article: any
}

interface CommentState{
	value: string,
}

/**CommentInput: Display the comments of an article and dds comment to it,
 *props: the article to add article to
 * state: the comment value to be added
 */
class CommentInput extends React.Component<CommentProps , CommentState> {
	constructor(props: CommentProps) {
		super(props);
		this.state = {
			value: '',
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	addComment(comment: string, article_id: number) {
		const comment_query = 'mutation createComment{createComment(userID: 1, articleID: ' + article_id + ', text:"' + comment + '"){id userID articleID timestamp text}}';
		if(comment === "")
			return
		fetch('/graphql', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
			},
			body: JSON.stringify({query: comment_query})
		})
			.then(response => {
				if (response.ok) {

					return response.json();
				} else {
					console.log("Invalid response from API server.");
				}
			})
	}

	handleChange(event: any) {
		this.setState({value: event.target.value});
	}

	handleSubmit(event: any) {
		this.addComment(this.state.value, this.props.article.id);
		this.props.Update();
		event.preventDefault();
	}


	render() {
		return (
			<form>
				<div>
					{this.props.article.commentConnection.map((comment: any, i: number) =>
						<p key={i} data-testid={"comments"} className={'comments'}> {comment.text}</p>
					)}
					<input type={"text"} id={"comment-input"} name={"comment"} value={this.state.value}
						   data-testid={"comment-input"}  placeholder={"Add your comment .."} onChange={this.handleChange}/>
					<button type={'submit'} className={'add-comment-btn'} data-testid={"comment-button"}
							onClick={(event) => {
						this.handleSubmit(event);
						this.setState({value: ""})
					}}>Add comment
					</button>
				</div>
			</form>

		);
	}
}
const mapStateToProps = () => ({
});

const mapDispatchToProps = () => {
	return {
		Update
	};
};

export default connect(mapStateToProps, mapDispatchToProps())(CommentInput);

