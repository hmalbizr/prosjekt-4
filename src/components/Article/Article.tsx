import React from "react";
import { connect} from "react-redux";
import { Search } from "./../../actions/Actions";
import './Articles.css';
import CommentInput from "./Comment";
import PageButton from "./PageButton";

interface ArticleProps{
	searchQuery: string,
	query: any
}

interface ArticleState{
	articles: Array<ArticleData>,
	article_connections: Array<any>,
	author_connections: Array<any>,
	isLoading: boolean,
	error: boolean
}

interface ArticleData{
	authorConnection: Array<any>,
	commentConnection: Array<any>,
	id: number,
	title: string,
	timestamp: string,
	text: string,
}

/**Article: Display each articles which are fetched from database.
 * State: A list of articles
 */
class Article extends React.Component<ArticleProps , ArticleState> {
	constructor(props: ArticleProps) {
		super(props);
		this.state = {
			articles: [],
			article_connections: [],
			author_connections: [],
			isLoading: true,
			error: false
		};
	}

	componentDidMount() {
		fetch('/graphql', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
			},
			body: JSON.stringify({query: '{article(limit: 4, page: 0){authorConnection {author{firstName lastName}} commentConnection{text} id text title timestamp}}'})
		})
			.then(response => {
				if (response.ok) {
					return response.json();
				} else {
					console.log("Invalid response from API server.");
				}
			})
			.then(data => {
				if (data === undefined || data.data[Object.keys(data.data)[0]].length === 0) {
					this.setState({articles: [], isLoading: false, error: true})
				} else {
					this.setState({articles: data.data.article, error: false, isLoading: false})
				}
			});
	}

	componentWillReceiveProps(p:any) {
		if(!(p.query.includes("{title}}"))){
			fetch('/graphql', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Accept': 'application/json',
				},
				body: JSON.stringify({query: p.query})
			})
				.then(response => {
					if (response.ok) {
						return response.json();
					} else {
						console.log("Invalid response from API server.");
					}
				})
				.then(data => {
					if (data === undefined || data.data[Object.keys(data.data)[0]].length === 0) {
						this.setState({articles: [], isLoading: false, error: true})
					} else {
						if(data.data[Object.keys(data.data)[0]][0].articleConnection){
							this.setState({articles: [], article_connections: data.data[Object.keys(data.data)[0]][0].articleConnection, error: false, isLoading: false})
						}
						else {
							if(data.data[Object.keys(data.data)[0]][0].authorConnection[0])
								this.setState({articles: data.data[Object.keys(data.data)[0]], author_connections: data.data[Object.keys(data.data)[0]][0].authorConnection[0].author, article_connections:[], error: false, isLoading: false})
							else
								this.setState({articles: data.data[Object.keys(data.data)[0]], author_connections: [], article_connections:[], error: false, isLoading: false})
						}
					}
				});
		}
	}

	render(){
		const {error, isLoading, article_connections, articles} = this.state
		let articles_array: Array<any> = [];
		if(article_connections.length > 0) {
			for(let article in article_connections){
				if(article_connections[article].article[0] !== undefined)
					articles_array.push(article_connections[article].article[0])
			}
		}
		else {
			articles_array = articles
		}

		if (error) {
			return (
				<div>
					<h1>No Data Found</h1>
				</div>
			)
		}
		if (isLoading) {
			return (<div>
				<h1>Loading ..</h1>
			</div>)
		}

		else {
			if(articles_array.length === 0){

				//No articles have been returned
				return(<div>
					<h1>No Data Found</h1>
				</div>)
			}
			//Print the articles from search results
			return (<div>
				{articles_array.map((art, index) =>
					<div key={index} className={"one-article"}>
						<h1 data-testid={"article-title"}>{art.title}</h1>
						{art.authorConnection.length>0? art.authorConnection.map((author_l:any, i:number) =>
								<p key={i} id={'author'}>Author: {author_l.author[0].firstName} {author_l.author[0].lastName}</p>
						): "Unknown Author"}
						<p>{art.text}</p>
						<h2>Comments:</h2>
						<CommentInput article={art}/>
					</div>
				)}
				<PageButton />
			</div>)
		}
	}
}

const mapStateToProps = (state:any) => {
	return {query: state.search.query, page:state.search.page}
};

const mapDispatchToProps = () => {
	return {
		Search
	};
};

export default connect(mapStateToProps, mapDispatchToProps())(Article);
