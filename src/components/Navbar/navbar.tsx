import React, { useState } from "react";
import Dropdown from "./dropdown";
import "./navbar.css";
//import { useDispatch} from "react-redux";
//import {RootState} from "./../../reducers/rootReducer";
import SearchInput from "./../Search/SearchInput";
import Login from "../Users/UserLogin";

/**
 * Creates the header menu and responses to the screen size to fix the menu presentation.
 * @constructor
 */
function Navbar() {
	//useSelector here can be used to get back the results after searching, filtering or sorting, and then display them.
	//const search_value = useSelector((state: RootState) => state.search)
	//const dispatch = useDispatch();

	const [click, setClick] = useState(false);
	const [filterAuthor, setFilterAuthor] = useState(false);
	const [filterTag, setFilterTag] = useState(false);
	const [sort, setSort] = useState(false);
	const [mobil, setMobil] = useState(window.innerWidth < 990);

	// Toggle menu button
	const handleClick = () => setClick(!click);

	const onMouseEnterFilterAuthor = () => {
		setFilterAuthor(true);
	};
	const onMouseEnterFilterTag = () => {
		setFilterTag(true);
	};
	const onMouseEnterSort = () => {
		setSort(true);
	};

	const onMouseLeaveFilterAuthor = () => {
		setFilterAuthor(false);
	};
	const onMouseLeaveFilterTag = () => {
		setFilterTag(false);
	};
	const onMouseLeaveSort = () => {
		setSort(false);
	};

	const onResize = () => {
		setMobil(window.innerWidth < 990);
	};
	window.addEventListener("resize", onResize);

	const burgerMenu = (
		<div className={"menu-icon"}>
			<div className="bar1"></div>
			<div className="bar2"></div>
			<div className="bar3"></div>
		</div>
	);

	const nothing = null;

	return (
		<React.Fragment>
			<nav className="navbar">
				<div className={""} onClick={handleClick}>
					{mobil ? burgerMenu : nothing}
					<i className={""} />
				</div>
				<ul className={"menu"}>
					<li className={"menu-item menu-item-label"}>
						<SearchInput />
					</li>
					<li
						className={
							mobil && !click
								? "hidden"
								: "menu-item menu-item-filter"
						}
						data-testid={"filter-author"}
						onMouseEnter={onMouseEnterFilterAuthor}
						onMouseLeave={onMouseLeaveFilterAuthor}
					>
						<p className="nav-links">Filter all on Authors</p>
						{filterAuthor && <Dropdown dropdown={"filterAuthor"} />}
					</li>
					<li
						className={
							mobil && !click
								? "hidden"
								: "menu-item menu-item-filter"
						}
						data-testid={"filter-tag"}
						onMouseEnter={onMouseEnterFilterTag}
						onMouseLeave={onMouseLeaveFilterTag}
					>
						<p className="nav-links">Filter all on Category</p>
						{filterTag && <Dropdown dropdown={"filterTag"}/>}
					</li>
					<li
						className={
							mobil && !click
								? "hidden"
								: "menu-item menu-item-sort"
						}
						onMouseEnter={onMouseEnterSort}
						onMouseLeave={onMouseLeaveSort}
					>
						<p className="sort_item">Sort articles alphabetically</p>
						{sort && <Dropdown dropdown={"sort"} />}
					</li>
					<li className={"menu-item"}>
						<Login />
					</li>
				</ul>
			</nav>
		</React.Fragment>
	);
}

export default Navbar;
