export async function getAuthors(){
	const response = await fetch('/graphql', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json',
		},
		body: JSON.stringify({query: '{author {firstName lastName}}'})
	})

	const data = await response.json();
	const All_authors_data = data.data[Object.keys(data.data)[0]]
	let All_authors: Array<object> = []
	for(let element in All_authors_data){
		let firstName = All_authors_data[element].firstName
		let lastName = All_authors_data[element].lastName
		let AuthorName = firstName + " " + lastName
		All_authors.push({name: AuthorName})
	}
	//Return the list of all authors
	return All_authors
}

export async function getTags(){
	const response = await fetch('/graphql', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json',
		},
		body: JSON.stringify({query: '{tag{id tag}}'})
	})

	const data = await response.json();
	const All_tags_data = data.data[Object.keys(data.data)[0]]
	let All_tags: Array<object> = []
	for(let element in All_tags_data){
		//let id = All_tags_data[element].id
		let tag = All_tags_data[element].tag
		All_tags.push({name: tag})
	}
	//Return the list of all authors
	return All_tags
}

export let filter_author: any;
getAuthors().then(data => filter_author= data)

export let filter_tag: any;
getTags().then(data => filter_tag= data)

export let filter_type3 = [
	{
		name: "Category: TestTag",
	},
	{
		name: "Category: TestTag2",
	},
	{
		name: "Author: testFirstName testLastName",
	},
	{
		name: "Author: test2FirstName test2LastName",
	},
];

export const sort_type3 = [
	{
		name: "Date",
	},
	{
		name: "Category",
	},
	{
		name: "Length",
	},
];
