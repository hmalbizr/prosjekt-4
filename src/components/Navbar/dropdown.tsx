import React, {useState} from "react";
import './navbar.css'
import { useDispatch} from "react-redux";
import {FilterAuthor, FilterTag, Sort} from "./../../actions/Actions";
import {filter_author, filter_tag} from "./FilterSortTypes";


interface DropdownTypeProps {
	dropdown : string;
}


/**
 * Displays the dropdown menu for filter and sort.
 * @param props
 * @constructor
 */
function Dropdown(props : DropdownTypeProps){

	const dispatch = useDispatch();

	const [click, setClick] = useState(false);

	const handleClick = () => setClick(!click);

	const filterAuthorThemeElement =
		<ul onClick={handleClick} className={"dropdown-menu"} data-testid={"filter-author-dropdown"}>
			{filter_author ? filter_author.map((item: any, index: number) => {
				return (
					<li key={index} className={"dropdown-item"} onClick={() => {dispatch(FilterAuthor(item.name))}}>
						<p className={item.name}>
							{item.name}
						</p>
					</li>
				)
			}) : ""}
		</ul>

	const filterTagThemeElement =
		<ul onClick={handleClick} className={"dropdown-menu"} data-testid={"filter-tag-dropdown"}>
			{filter_tag ? filter_tag.map((item: any, index: number) => {
				return (
					<li key={index} className={"dropdown-item"} onClick={() => {dispatch(FilterTag(item.name))}}>
						<p className={item.name}>
							{item.name}
						</p>
					</li>
				)
			}) : ""}
		</ul>

	/*const sortElement =
		<ul onClick={handleClick} className={"dropdown-menu"}>
			{sort_type.map((item, index) => {
				return (
					<li key={index} className={"dropdown-item"} onClick={() => {dispatch(Sort(item.name))}}>
						<p className={item.name} >
							{item.name}
						</p>
					</li>
				)
			})}
		</ul>*/
	const sortElement = <p className={"dropdown-item"} onClick={() => {dispatch(Sort())}}> Sort articles alphabetically</p>


	if (props.dropdown === "filterAuthor") {
		return (filterAuthorThemeElement);
	}
	else if (props.dropdown === "filterTag"){
		return (filterTagThemeElement);
	}
	else if (props.dropdown === "sort"){
		return (sortElement);
	}
	return(
		<></>
	);
}

export default Dropdown;
