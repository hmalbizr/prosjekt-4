import React from "react";
import Popup from "reactjs-popup";
import "./../Navbar/navbar.css";

interface LoginProps {}

interface LoginState {
	username: string;
	password: string;
	result: Array<UserData>;
	hideloggedin: boolean;
	isloggedin: boolean;
}
interface UserData {
	id: number;
	username: string;
}

class Login extends React.Component<LoginProps, LoginState> {
	constructor(props: any) {
		super(props);
		this.state = {
			username: "",
			password: "",
			result: [],
			hideloggedin: true,
			isloggedin: false,
		};
	}

	handleloggedin() {
		const logdetails = localStorage.getItem("logdetails");
		if (logdetails !== null) {
			this.setState({ hideloggedin: false });
			this.setState({ isloggedin: true });
		}
	}
	componentDidMount() {
		this.handleloggedin();
	}

	handleSubmit = (event: any) => {
		event.preventDefault();
		this.fetchUsers();
	};

	handleChange = (event: any) => {
		event.persist();
		this.setState((values) => ({
			...values,
			[event.target.name]: event.target.value,
		}));
	};

	fetchUsers = async () => {
		fetch("graphql", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Accept: "application/json",
			},
			body: JSON.stringify({ query: "{user{id username}}" }),
		})
			.then((response) => {
				if (response.ok) {
					return response.json();
				} else {
					console.log("error from API server");
				}
			})

			.then((data) => {
				if (data === undefined) {
					this.setState({ result: [] });
				} else {
					console.log(data.data.user);
					for (let user in data.data.user) {
						if (
							this.state.username ===
								data.data.user[user].username &&
							this.state.password === "test"
						) {
							this.setState({ isloggedin: true });
							console.log("suksess");
							alert(
								"You are logged in as: " + this.state.username
							);
						} else {
							console.log("feil");
						}
					}
					this.setState({ result: data.data.user });
				}
			});
	};

	render() {
		return (
			<div>
				<div className="login-container">
					<Popup
						className="nav-button"
						trigger={<button className="login-btn"> Log in</button>}
						position="bottom right"
						contentStyle={{ width: "250px" }}
					>
						<div className="Popup-holder">
							<form onSubmit={this.handleSubmit}>
								<div className="form-row">
									<div className="col">
										Username:
										<input
											type="text"
											className="form-control"
											placeholder="Your username"
											value={this.state.username}
											onChange={this.handleChange}
											name="username"
										/>
									</div>
									<div className="col">
										Password:
										<input
											type="password"
											className="form-control"
											placeholder="Your password"
											value={this.state.password}
											onChange={this.handleChange}
											name="password"
										/>
									</div>
								</div>

								<button
									type="submit"
									className="btn btn-warning"
								>
									{" "}
									Login{" "}
								</button>
							</form>
						</div>
					</Popup>
				</div>
			</div>
		);
	}
}

export default Login;
